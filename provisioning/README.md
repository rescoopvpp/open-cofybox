# CoFyBox Provisioning

The CoFyBox images are built in the pipeline, preloaded with latest versions of software, and then uploaded to AWS S3 version of the CoFy Box software. This supports bulk provisioning. The alternative would be to manually create each device in the Balena Cloud web console.

To add a device, you must:
- Run the scripts in this folder to add a Balena device and configure a copy of the base image with the device metadata.
- Burn the configured image to an SD card

## Requirements

- Install Balena CLI (IMPORTANT: use the latest standalone version from github)
- Install AWS S3 CLI (either package manager or AWS direct DL)
- Install jq (probably package manager)

## Prerequisites

- A Balena Cloud account connected with the appopriate application.
- Access to the S3 bucket containing the preloaded images.

## Instructions

This folder contains the files and scripts used in CoFyBox bulk provisioning.

If an image exists already you can start at 2.

```
#. Login to balena-cli with `balena login` (if required)
#. Run 1_download_sdimg.sh to download latest image (this only needs to be done once in any session).
#. Run (with sudo) 2_prepare_sdimg.sh to create a new device on balena and generate an image for this device. The image will be named using the balena name e.g. happy-lion.img.
#. Run balena local flash/dd/Balena Etcher to flash this image to an SD card (32GB+ recommended). 
#. Repeat 3, 4 if you need to burn more than one device. If you make more than one make sure to label or otherwise note the image that is loaded on a specific SD card. 
#. Device data is appended to devices.log (for making labels etc.).
```
