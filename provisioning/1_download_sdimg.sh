#You need to install:
# aws-cli
# jq

#!/bin/bash

ARCH_NAME="${ARCH_NAME:-raspberrypi3}"
RELEASE="${RELEASE:-staging}"

# Construct IMAGE_NAME and BALENA_OS_VERSION based on above
IMAGE_NAME="cofycompany-"$ARCH_NAME"-cofybox-"$RELEASE"-sdcard.img"
BALENA_OS_VERSION=`aws s3api get-object-tagging --bucket cofybox-sdimg --key cofycompany-${ARCH_NAME}-cofybox-${RELEASE}-sdcard.img.zip|jq -r '.TagSet|.[]|select(.Key=="BALENA_OS_VERSION")|.Value'`

echo "export ARCH_NAME=$ARCH_NAME">create_env.sh
echo "export RELEASE=$RELEASE">>create_env.sh
echo "export IMAGE_NAME=$IMAGE_NAME">>create_env.sh
echo "export BALENA_OS_VERSION=$BALENA_OS_VERSION">>create_env.sh
echo 'UUID0,Name0,Time0,SSID0' >devices.log

# Copy the SD image from S3
aws s3 cp s3://cofybox-sdimg/${IMAGE_NAME}.zip ./
if [[ ! -f "${IMAGE_NAME}.zip" ]] ; then
    echo 'File '$LOCAL_PATH ' is not there, aborting.'
    echo 'Make sure you are logged into aws.'
    echo 'Try running aws configure.'
    exit
fi

unzip ${IMAGE_NAME}.zip
mv $IMAGE_NAME cofybox-sdcard.img
rm ${IMAGE_NAME}.zip
