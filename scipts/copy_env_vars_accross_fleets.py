import subprocess
import json

fleet_from = 'CofyCompany/cofybox-staging'
fleet_to = 'CofyCompany/frenchmeter'

env_vars_list_process = subprocess.run(['balena', 'envs', '--fleet', fleet_from, '--json'], stdout=subprocess.PIPE)
env_vars = json.loads(env_vars_list_process.stdout.decode())

for env_var in env_vars:
    print(f'setting {env_var["name"]} to {env_var["value"]}...')
    cmd = ['balena', 'env', 'add', env_var["name"], env_var["value"], '--fleet', fleet_to ]
    if not env_var["serviceName"] == '*':
        print(f'    for service {env_var["serviceName"]}')
        cmd += ['--service', env_var["serviceName"]]
    set_var_process = subprocess.run(cmd, stdout=subprocess.PIPE)
    state_of_env_vars = set_var_process.stdout.decode()