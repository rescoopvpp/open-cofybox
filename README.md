# Open Cofybox

This is the open source version of REScoopVPP's cofybox repository. We had to remove some private code from the original repo, but we have left as much in here as possible. in this readme, you will find instructions on how to develop with this respository, and we include a button at the bottom for you to deploy to your very own balena fleet. The code is set up to deploy to Raspberry Pis, but you can alter that if you wish.

Some of the services in this repo are coupled to our cofycloud, including `cofybox-config` and `cofycloud-api-pull`. It is unlikely that you will be able to access the cofycloud if you are not a REScoopVPP partner. However, we would encourage you to configure your own cloud connection with any cloud service that you have access to, using inspiration from in here.

If you work with this repo and get something to work, espcially on a real device, let us know! 

## Developing locally on your laptop with docker-compose

The deployment has been setup in such a way that it can easily be run and tested locally on a laptop. This is faciliated by using the standard overriding behaviour present in docker-compose in conjunction with the balena multi-arch/platform images. The only difference will be the application of network settings which is disabled by an environment variable for the config UI in the `local.yml` override file.

To test service(s) locally run:

```bash
docker-compose -f docker-compose.yml -f local.yml up mosquitto <service 1> <service 2> [...]

```

(most services depend on the MQTT broker so this should normally be included).

For example, say you wish to test the glue component, then you need to run:

```bash
docker-compose -f docker-compose.yml -f local.yml up --build mosquitto glue 

```
### Reset service state
Sometimes you may wish to reset the state of services locally. The 'state' of a service is stored in the persistent volumes so these must be removed.

To do this first use `docker-compose down` to remove (stopped) containers:
```bash
docker-compose -f docker-compose.yml -f local.yml down <service 1>

```
Then remove the relevant volumes using:
```bash
docker-compose volume rm <volume>

```
You can also use the standard docker cleanup command (but this will remove all stopped containers and associated volumes):
```bash
docker system prune --volumes
```

### Ubuntu

On Ubuntu, AppArmor complains about the containers accessing dbus. To get around this, you can use the profile provided with:

`sudo apparmor_parser -r docker/ubuntu/cofybox-balena-apparmor-policy`

You can also copy this into `/etc/apparmor.d/` if you want it to be persistent across restarts.

You then need to ln or cp ubuntu.yml from docker/ubuntu to the top level and add the additional -f flag to your docker-compose command (similar to what you need to do for docker/local.yml)

## Developing on a local device

Some aspects of the system can only be tested on an actual device. This can be done using the faster Balena local mode workflow where code is pushed from your laptop to a device directly (not via cloud) usually somewhere on your local network. 

### Local mode tips
* Local push sometimes seems to get stuck or fails. You can manually rescue this by `balena ssh` into the device and restarting the supervisor container and then retrying. 
* If this doesn't work it can help to reset the device. There are many ways to do this but the easiest is pushing a 'blank' compose file using `balena push` e.g.

```
version:2
```
This will kill all running services. You can then `balena ssh` into the device and use the cleanup command:
```
balena system prune --volumes
```
* A local mode device can either be provisioned intially to the fleet (using a development image) or the a balenaOS base image can be used. A balenaOS base image by default never connected to the cloud so some specific things in the environment may not be present such as a 'friendly name' (they do however have a persistent UUID which is generated at startup).
* You can get a container to stay alive / idle using the `balena-idle` command. This can be useful for debugging.


## Balena Cloud Deployment

Please refer to balena-cli documentation and installation instructions which can be found [here](https://www.balena.io/docs/reference/balena-cli/). The deployment uses the documented `balena deploy` workflow i.e. we build the balena images on our own runners before pushing them to balena cloud.

It is possible to test Balena builds locally using the `--emulated` flag to balena build command but the need for this can largely be avoided due to the use of the multi-arch images: `balena build --application cofybox-staging --emulated`

## Deploy with Balena

[![balena deploy button](https://www.balena.io/deploy.svg)](https://dashboard.balena-cloud.com/deploy?repoUrl=https://gitlab.com/rescoopvpp/cofybox-balena.git)
