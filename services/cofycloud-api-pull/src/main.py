import json
import os
import logging
import sched
import time
import paho.mqtt.client as mqtt

from pricing import request_and_publish_prices
from explicit import request_and_publish_signals

first_run_prices = True
first_run_signals = True

MQTT_BROKER = os.getenv("MQTT_BROKER", "localhost")
brokers_out = {"broker1": MQTT_BROKER}

logging_level = logging.INFO
logger = logging.getLogger('cofycloud-api-pull')
LOG_FORMAT2 = "[%(asctime)s %(process)d:%(threadName)s] %(name)s - %(levelname)s - %(message)s | %(filename)s:%(lineno)d"
logging.basicConfig(level=logging_level, format=LOG_FORMAT2)

logger.info("Starting CofyCloud API pull")

scheduler = sched.scheduler(time.time, time.sleep)


def run():
    global first_run_prices
    global first_run_signals

    client = mqtt.Client("cofycloudapi")
    try:
        client.connect(brokers_out['broker1'])
    except:
        logger.exception(
            "Could not connect to MQTT broker, nothing to do anymore"
        )
    else:
        try:
            with open("/cofybox-config/api_config.json", 'r') as f:
                api_config = json.load(f)
        except FileNotFoundError:
            logger.exception("API config file not found")
        else:
            base_url = api_config['baseUrl']
            api_key = api_config['authorizationHeaderValue']

            try:
                request_and_publish_prices(base_url=base_url, api_key=api_key, first_run=first_run_prices, mqtt_client=client)
            except:
                pass
            else:
                first_run_prices = False

            try:
                request_and_publish_signals(base_url=base_url, api_key=api_key, first_run=first_run_signals, mqtt_client=client)
            except:
                pass
            else:
                first_run_signals = False
            client.disconnect()

    scheduler.enter(
        delay=300,  # run every 5 minutes
        priority=1,
        action=run
    )


if __name__ == '__main__':
    logger.info("Starting API pull in 10 seconds")
    scheduler.enter(
        delay=10,  # wait 10 seconds, to make sure the API token is saved by
        # the cofybox-config service
        priority=1,
        action=run,
    )
    scheduler.run()