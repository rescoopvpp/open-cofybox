#!/bin/bash
if test "$COFYBOX_ENABLE_SERVICE" = TRUE ; then
    python main.py
else
    echo "Cofycloud API Pull block not enabled by environment variable"
fi
