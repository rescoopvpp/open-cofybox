import time
from typing import Tuple, Dict
import pandas as pd
import urllib
import requests
import json
import logging
import paho.mqtt.client as mqtt


logging_level = logging.INFO
logger = logging.getLogger('cofycloud-api-pull')
LOG_FORMAT2 = "[%(asctime)s %(process)d:%(threadName)s] %(name)s - %(levelname)s - %(message)s | %(filename)s:%(lineno)d"
logging.basicConfig(level=logging_level, format=LOG_FORMAT2)


def request_current_prices(base_url: str, api_key: str) -> Tuple[pd.Series,
                                                                Dict]:
    """
    Returns a Tuple consisting of:
        A series containing the current prices
        A dictionary containing the units (currency)
    """
    # Grab API URL and KEY from config file
    if not base_url.endswith('/'):
        base_url += '/'

    # Prepare URL, headers and parameters
    url = urllib.parse.urljoin(base_url, 'data/tariff')
    headers = {"Authorization": api_key}
    now = pd.Timestamp.utcnow()
    params = {
        "start": now.floor('30min'), # We need to request the current block
        # of 30 minutes
        "end": now.ceil('30min'),
        "interval": "PT5M"
    }

    # Do the request
    r = requests.get(
        url,
        headers=headers,
        params=params
    )
    r.raise_for_status()

    # Define a few helper methods to parse the payload into a dataframe
    def parse_payload(payload: Dict) -> pd.DataFrame:
        try:
            df = pd.concat([parse_series(s) for s in payload['value']], axis=1)
        except ValueError:
            return pd.DataFrame()
        return df

    def parse_series(series: Dict) -> pd.Series:
        df = pd.DataFrame(series['data'])
        df.set_index('timestamp', inplace=True)
        df.index = pd.to_datetime(df.index, utc=True)
        df.index = pd.DatetimeIndex(df.index)
        df.sort_index(inplace=True)
        ts = df.squeeze(axis=1)
        ts.index.name = None
        ts.name = series['name']
        return ts

    payload = json.loads(r.text)  # make sure we have unicode, because
    # currency symbols
    df = parse_payload(payload)
    if df.empty:
        return None, None
    current = df.loc[now.floor('5min')]  # select current data only
    unit_mapping = {e['name']: e['unit'] for e in r.json()['value']}

    return current, unit_mapping


def request_and_publish_prices(base_url: str, api_key: str, first_run: bool, mqtt_client: mqtt.Client):
    prices, units = request_current_prices(base_url=base_url, api_key=api_key)
    if prices is not None and units is not None:
        prices = prices.round(4)

        logger.info("Fetched prices:")
        logger.info(prices)
        logger.info(units)

        names = {
            'electricityTariff': 'Tariff Indicator Electricity Consumption',
            'electricityInjectionFee': 'Tariff Indicator Electricity Feed-in'
        }
        topics = {
            'electricityTariff': 'consumption_tariff',
            'electricityInjectionFee': 'feedin_tariff'
        }

        for tariff in names.keys():
            state_topic = f"data/devices/tariff_indicator/{topics[tariff]}"
            if first_run:  # On the first run we create sensors
                logger.info("Creating autodiscovered sensors within HA")
                autodiscover_topic = f"homeassistant/sensor/tariff_indicator/{topics[tariff]}/config"

                unit = units[tariff]
                if '£' in unit:
                    unit = '£'
                elif '€' in unit:
                    unit = '€'
                autodiscover_payload = {
                    "name": names[tariff],
                    "device_class": "monetary",
                    "unit_of_measurement": unit,
                    "state_topic": state_topic,
                    "unique_id": f"tariff_indicator_{tariff}",
                    "value_template": "{{ value_json.value }}",
                    "device": {
                        "identifiers": ["COFY_tariff_indicator"],
                        "name": "Tariff indicator",
                        "model": "Tariff indicator for COFY-box",
                        "manufacturer": "cofybox.io"
                    }
                }
                try:
                    # First empty payload to reset
                    mqtt_client.publish(topic=autodiscover_topic, payload="")
                    time.sleep(0.25)
                    logger.info(f"Publishing {autodiscover_payload} to {autodiscover_topic}")
                    mqtt_client.publish(topic=autodiscover_topic, payload=json.dumps(autodiscover_payload, ensure_ascii=False), retain=True)
                except:
                    logger.exception(f"Could not publish to {autodiscover_topic}")
                    break

            state_payload = {
                "value": prices[tariff]
            }
            try:
                logging.info(f"Publishing {state_payload} to {state_topic}")
                mqtt_client.publish(topic=state_topic, payload=json.dumps(state_payload, ensure_ascii=False))
            except:
                logger.exception(f"Could not publish to {state_topic}")
                break
    else: # Api call has returnd nothing
        logging.info("Pricing API has returned nothing.")