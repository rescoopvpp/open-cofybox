import time
from typing import Dict
import urllib
import requests
import json
import logging
import paho.mqtt.client as mqtt


logging_level = logging.INFO
logger = logging.getLogger('cofycloud-api-pull')
LOG_FORMAT2 = "[%(asctime)s %(process)d:%(threadName)s] %(name)s - %(levelname)s - %(message)s | %(filename)s:%(lineno)d"
logging.basicConfig(level=logging_level, format=LOG_FORMAT2)


def request_signals(base_url: str, api_key: str) -> Dict:
    """
    Request the explicit signals from COFYcloud
    """
    # Grab API URL and KEY from config file
    if not base_url.endswith('/'):
        base_url += '/'

    # Prepare URL, headers and parameters
    url = urllib.parse.urljoin(base_url, 'signals')
    headers = {"Authorization": api_key}

    # Do the request
    logger.info(f"Calling {url} with headers {headers}")
    r = requests.get(
        url,
        headers=headers,
    )
    r.raise_for_status()
    return r.json()


def request_and_publish_signals(base_url: str, api_key: str, first_run: bool, mqtt_client: mqtt.Client):
    try:
        signals = request_signals(base_url=base_url, api_key=api_key)
    except requests.HTTPError as e:
        logger.exception(e)
        return
    else:
        logger.info("Fetched Signals!")
        logger.info(signals)

    for signal in signals:
        state_topic = f"data/devices/cloudsignals/{signal}"
        if first_run:  # On the first run we create sensors
            logger.info("Creating autodiscovered sensors within HA")
            autodiscover_topic = f"homeassistant/sensor/cloudsignals/{signal}/config"
            autodiscover_payload = {
                "name": signal,
                "state_topic": state_topic,
                "unique_id": f"cloud_signal_{signal}",
                "value_template": "{{ value_json.value }}",
                "device": {
                    "identifiers": ["COFY_Cloud_Signals"],
                    "name": "Cloud Signals",
                    "model": "Cloud Signals for COFY-box",
                    "manufacturer": "cofybox.io"
                }
            }
            try:
                # First empty payload to reset
                mqtt_client.publish(topic=autodiscover_topic, payload="")
                time.sleep(0.25)
                logger.info(f"Publishing {autodiscover_payload} to {autodiscover_topic}")
                mqtt_client.publish(topic=autodiscover_topic, payload=json.dumps(autodiscover_payload, ensure_ascii=False), retain=True)
            except:
                logger.exception(f"Could not publish to {autodiscover_topic}")
                break

        try:
            value = int(signals[signal])
        except:
            value = signals[signal]

        state_payload = {
            "value": value
        }
        try:
            logging.info(f"Publishing {state_payload} to {state_topic}")
            mqtt_client.publish(topic=state_topic, payload=json.dumps(state_payload, ensure_ascii=False))
        except:
            logger.exception(f"Could not publish to {state_topic}")
            break
