import json
from os import path
import sys

from paho.mqtt.client import Client


def get_commandline_input():
    if len(sys.argv) != 2:
        print("Provide ONE mqtt topic value")
        sys.exit(-1)

    return sys.argv[-1]


def read_config(filepath: str):
    print(f"Opening config file: {filepath}")
    with open(filepath, encoding='utf-8') as configfile:
        return json.load(configfile)


def on_subscribe(client, _userdata, _mid, granted_qos):
    print(f"Succesfully subscribed {client} with QoS {granted_qos}")


def on_message(_client, _userdata, message):
    print(f"Received message {message.topic}:{message.payload}")


def main():
    topic = get_commandline_input()
    config = read_config(filepath=f"{path.dirname(__file__)}/../config/local_testing.json")

    mqtt_client = Client()
    host = config["mqtt_host"]
    port = config["mqtt_port"]
    print(f"Listening on {host}:{port}")
    mqtt_client.connect(host, port)
    mqtt_client.on_subscribe = on_subscribe
    mqtt_client.on_message = on_message
    mqtt_client.subscribe(topic)
    try:
        mqtt_client.loop_forever()
    except KeyboardInterrupt:
        print("\nBye!")
        mqtt_client.loop_stop()


main()
