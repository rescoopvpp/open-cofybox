import json
from os import path
from pprint import pprint
import sys
from d2lparser import D2LParser


def read_commandline_argument():
    if len(sys.argv) != 2:
        print("Provide a hexstring to parse")
        sys.exit(-1)

    return bytes.fromhex(str(sys.argv[-1]))


def read_config(filepath: str):
    print(f"Opening config file: {filepath}")
    with open(filepath, encoding='utf-8') as configfile:
        return json.load(configfile)


def main():
    hexstring = read_commandline_argument()
    config = read_config(filepath=f"{path.dirname(__file__)}/../config/local_testing.json")

    d2l_key = config["d2l_key"]
    d2l_iv = config["d2l_iv"]
    print(f"Making parser from {d2l_key}:{d2l_iv}")
    parser = D2LParser(d2l_key, d2l_iv)

    packet = parser.parse_request(msg=hexstring, encrypted=True)  # Only encrypted is supported
    pprint(packet.header)
    if packet.payload is not None :
        pprint(json.loads(packet.payload))


main()
