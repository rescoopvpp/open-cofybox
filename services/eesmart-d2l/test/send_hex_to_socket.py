
import json
from os import path
import socket
import sys


def read_commandline_argument():
    if len(sys.argv) != 2:
        print("Provide a hexstring to parse")
        sys.exit(-1)

    return sys.argv[-1]


def read_config(filepath: str):
    print(f"Opening config file: {filepath}")
    with open(filepath, encoding='utf-8') as configfile:
        return json.load(configfile)


def main():

    inputstring = read_commandline_argument()

    config = read_config(filepath=f"{path.dirname(__file__)}/../config/local_testing.json")

    host = config["d2l_host"]
    port = config["d2l_port"]
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print(f"Connecting to {host}:{port}")
    sock.connect((host, port))
    print(f"Sending data: {inputstring}")
    sock.sendall(bytes.fromhex(inputstring))


main()
