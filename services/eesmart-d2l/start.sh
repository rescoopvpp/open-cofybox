#!/bin/bash
if test "$COFYBOX_ENABLE_SERVICE" = TRUE ; then
    cp -n config/eesmart-d2l.json /cofybox-config/eesmart-d2l.json
    python3 src/main.py
else
    echo "Eesmart D2L parser disabled"
fi
