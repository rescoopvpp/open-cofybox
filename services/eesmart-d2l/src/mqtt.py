import logging

from paho.mqtt.client import Client

logger = logging.getLogger(__name__)


def mqtt_on_connect(client, userdata, flags, rc_value):
    logger.info(f"Connected with result code {str(rc_value)}")


def mqtt_on_message(client, userdata, msg):
    logger.info(f"{msg.topic} {str(msg.payload)}")


def mqtt_on_subscribe(obj, mid, granted_qos):
    logger.info(f"Subscribed: {mid} {granted_qos}")


class MQTTClient(Client):

    def __init__(self, client_id=""):
        super().__init__(client_id=client_id,
                         clean_session=True,
                         reconnect_on_failure=True)
        self.on_connect = mqtt_on_connect
        self.on_message = mqtt_on_message
        self.on_subscribe = mqtt_on_subscribe

    def connect(self, host, port):
        logger.info(f"Connecting to {host}:{port}")
        super().connect(host, port)

    def publish(self, topic: str, payload: str):
        logger.info(f"[PUB] {topic} - {payload}")
        return super().publish(topic, payload, qos=1, retain=False, properties=None)
