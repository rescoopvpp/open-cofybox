import json
import logging
from os import environ
from sys import stdout
from json import load, JSONDecodeError
import time

from d2lparser.d2lparser import D2LParser, PayloadException, HeaderException
from d2lparser.packet import PacketType

from mqtt import MQTTClient
from server import D2LServer
from mappings import ETIQUETTE_MAPPINGS



logging.basicConfig(stream=stdout,
                    format="%(asctime)s\t[%(filename)s]\t[%(levelname)s]\t- %(message)s",
                    level=logging.DEBUG)
logger = logging.getLogger(__name__)

###########################
#   STATIC DEFINITIONS    #
###########################

CONFIG_FILE     = "/cofybox-config/eesmart-d2l.json"
DEVICE_NAME     = "d2l_smart_meter"
FRAMETYPE       = "_TYPE_TRAME"
FIRST_RUN       = True

#########################
#   STATIC FUNCTIONS    #
#########################


def _load_configuration(config_file):
    logger.info("Loading configuration file")
    with open(config_file, encoding="utf-8") as config_json:
        config = load(config_json)
        for key, value in config.items():
            logger.debug("[ %s - %s ]", key, value)
    return config

#############
#   CODE    #
#############


def handle_get_firmware_packet(packet) -> bytearray:
    logging.error(packet)


def handle_get_horloge_packet(packet, parser: D2LParser) -> bytearray:
    response = parser.generate_response(packet, encrypted=True)
    return response


def handle_push_json_packet(packet, mqtt_client) -> bytearray:
    global FIRST_RUN
    # Send MQTT Message based on sensors
    sensor_list = {k: v for k, v in packet.payload.items() if k in ETIQUETTE_MAPPINGS}
    logger.info(sensor_list)
    values = []
    for k, v in sensor_list.items():
        etiquette = ETIQUETTE_MAPPINGS[k]
        try:
            value = etiquette.type(v)
        except:
            value = v
        values.append((etiquette, value))

    unique_device = f"{DEVICE_NAME}_{packet.id_d2l}"
    if FIRST_RUN:
        # Quick-Fix in case HA has not booted yet. Wait 5 minutes
        time.sleep(300)
        for etiquette_name, etiquette in ETIQUETTE_MAPPINGS.items():
            unique_sensor = f"{etiquette_name.replace('+', 'p')}_{packet.id_d2l}"

            config_topic = f"homeassistant/sensor/{unique_device}/{unique_sensor}/config"
            state_topic = f"data/devices/{unique_device}/{unique_sensor}"

            mqtt_client.publish(topic=config_topic, payload="")
            time.sleep(0.25)
            payload = {
                "name": etiquette_name,
                "device_class": etiquette.hass_device_class,  # list of classes: https://www.home-assistant.io/integrations/sensor/
                "unit_of_measurement": etiquette.unite,
                "state_topic": state_topic,
                "unique_id": unique_sensor,
                "value_template": "{{ value_json.value }}",
                "device": {
                    "identifiers": ["EESmart", "D2L", "Linky"],
                    "name": f"D2L Dongle {packet.id_d2l}",
                    "manufacturer": "EESmart"
                }
            }
            payload = {k: v for k, v in payload.items() if v is not None}
            mqtt_client.publish(topic=config_topic, payload=json.dumps(payload, ensure_ascii=False).encode("utf-8"))
        FIRST_RUN = False

    for etiquette, value in values:
        unique_sensor = f"{etiquette.etiquette.replace('+', 'p')}_{packet.id_d2l}"
        state_topic = f"data/devices/{unique_device}/{unique_sensor}"
        state_payload = {"value": value}
        mqtt_client.publish(topic=state_topic, payload=json.dumps(state_payload, ensure_ascii=False))


def main():
    if "DEBUG" in environ:
        config = _load_configuration("config/local_testing.json")
    else:
        config = _load_configuration(CONFIG_FILE)

    parser = D2LParser(d2l_key=config["d2l_key"],
                       d2l_iv=config["d2l_iv"])

    logger.info("Booting MQTT client")
    mqtt_client = MQTTClient("d2l_parser")
    mqtt_client.connect(host=config["mqtt_host"],
                        port=config["mqtt_port"])
    mqtt_client.loop_start()

    def on_receive(addr, data):
        host = addr[0]
        port = addr[1]
        logger.info(f"Received {data.hex()} on {host}:{port}")
        global FIRST_RUN

        # Parse packet
        try:
            packet = parser.parse_request(data)
        except PayloadException as exception:
            logger.error(f"[PayloadException] {exception}")
            return
        except HeaderException as exception:
            logger.error(f"[HeaderException] {exception}")
            logger.error("Did you configure the correct D2LParser KEY and IV values?")
            return

        packet.payload = packet.payload if packet.payload else "{}"
        try:
            packet.payload = json.loads(packet.payload)
        except (UnicodeDecodeError, JSONDecodeError):
            logger.error("Unable to parse payload!")
            return
        logger.info(f"Header  = {packet.header}")
        logger.info(f"Payload = {packet.payload}")

        if packet.packet_type == PacketType.GET_FIRMWARE:
            logger.info("GET_FIRMWARE Packet")
            return handle_get_firmware_packet(packet)
        elif packet.packet_type == PacketType.GET_HORLOGE:
            logger.info("GET_HORLOGE Packet")
            return handle_get_horloge_packet(packet, parser)
        elif packet.packet_type == PacketType.PUSH_JSON:
            logger.info("PUSH_JSON Packet")
            return handle_push_json_packet(packet, mqtt_client)
        else:
            logger.info(f"Packet type {packet.packet_type} unsupported")
            return

    logger.info("Booting eesmart d2l-server")
    server = D2LServer(host=config["d2l_host"],
                       port=config["d2l_port"])
    server.on_receive = on_receive
    server.run()


if __name__ == '__main__':
    main()
