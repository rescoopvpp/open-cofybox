''' TCP Server for accepting D2L packets '''

import logging
from socketserver import StreamRequestHandler, TCPServer

logger = logging.getLogger(__name__)


class D2LHandler(StreamRequestHandler):
    """
    The request handler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    def handle(self):
        # self.request is the TCP socket connected to the client
        self.data = self.rfile.read()

        if self.server.on_receive:
            response = self.server.on_receive(self.client_address, self.data)
            if response is not None:
                logging.info(f"Sending response: {response}")
                self.request.sendall(response)


class D2LServer(TCPServer):
    """
    A server which accepts the packets being sent from the D2L server
    """

    def __init__(self, host: str = "0.0.0.0", port: int = 7845) -> None:
        super().__init__(server_address=(host, port),
                         RequestHandlerClass=D2LHandler,
                         bind_and_activate=True)

    @property
    def on_receive(self):
        return self._on_receive

    @on_receive.setter
    def on_receive(self, callback):
        self._on_receive = callback

    def run(self):
        self.serve_forever()
