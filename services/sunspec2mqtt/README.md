# sunspec2mqtt

Interface with Sunspec compatible solar inverters and publish their sensor values to MQTT

Will run continuously every minute. Optionally takes an IP address for the inverter, as a command line argument.
