#!/bin/bash
if test "$COFYBOX_ENABLE_SERVICE" = TRUE ; then
    cp -n sunspec.json /cofybox-config/sunspec.json
    python main.py
else
    echo "Sunspec block not enabled by environment variable"
fi
