# -*- coding: utf-8 -*-
"""
Created on Mon May 11 22:54:37 2020
Interface with any Sunspec compatible inverter, read essential registers and
publish the values to the MQTT bus
Configuration with JSON file
Support for multiple inverters
Currently only TCP connected inverters are supported
To do:
    *Functionise sunspec register publish loops, using additional config file
    *Check why S/N from SolarEdge inverters isn't read
@author: Epyon
"""
import argparse
import json
import logging
import os
import sched
import time

import paho.mqtt.client as mqtt
import sunspec.core.client as sunspecclient
from sunspec.core.modbus.client import ModbusClientError

firstRun = True

MQTT_BROKER = os.getenv("MQTT_BROKER", "localhost")
brokers_out = {"broker1": MQTT_BROKER}

_LOGGER = logging.getLogger(__name__)
_LOGGER.info("Starting Sunspec parser")

parser = argparse.ArgumentParser()

parser.add_argument(
    "ip", type=str, help="specify the IP address of the inverter", nargs="?"
)
args = parser.parse_args()
scheduler = sched.scheduler(time.time, time.sleep)

# For debugging purposes, it's possible to give one inverter ip through the
# command line
if args.ip:
    ipaddr = args.ip
    data = list(
        [
            dict(
                {
                    "name": "debuginverter",
                    "type": "TCP",
                    "device": "solar",
                    "ipaddr": ipaddr,
                    "ipport": 502,
                    "id": 1,
                    "registers": ["PPVphAB", "A", "W", "WH"],
                    "publish": False,
                }
            )
        ]
    )
else:
    try:
        with open("/cofybox-config/sunspec.json") as f:
            data = json.load(f)
    except OSError:
        logging.error("JSON load failed- falling back to default config")
        data = list(
            [
                dict(
                    {
                        "name": "debuginverter",
                        "type": "TCP",
                        "device": "solar",
                        "ipaddr": "192.168.0.99",
                        "ipport": 502,
                        "id": 1,
                        "registers": ["PPVphAB", "A", "W", "WH"],
                        "publish": False,
                    }
                )
            ]
        )  # a default fallback register id

uid = "sunspec_inverter"
devname = "Sunspec inverter"
model = "Sunspec inverter"
manufacturer = "CofyCo"
topic = "homeassistant/sensor/" + uid + "/"
state = "data/devices/" + uid +"/"

def haAutoDiscover(channel, devname, unit_of_measurement):
    """Generate Home Assistant autodiscovery MQTT sensor creation payload"""
    msg = {}
    msg['name'] = channel
    msg['unit_of_measurement'] = unit_of_measurement
    msg['device_class'] = "energy"
    msg['state_topic'] = "data/devices/" + devname + "/" + channel
    msg['value_template'] = "{{ value_json.value }}"
    msg['unique_id'] = devname + '_' + channel
    if(unit_of_measurement == "Wh" or unit_of_measurement == "kWh"):
        msg["state_class"] = "total_increasing";
    else:    
        msg["state_class"] = "measurement";
        msg["last_reset"] = "1970-01-01T00:00:00+00:00";
        msg["last_reset_value_template"] = "1970-01-01T00:00:00+00:00";
    dev = {}
    dev['identifiers'] = [devname.replace(" ", "_")]
    dev['name'] = "PV inverter"
    dev['model'] = devname
    dev['manufacturer'] = "CofyCo"
    msg['device'] = dev
    return json.dumps(msg, ensure_ascii=False)

def fetch_inverter_updates(parsed_data):
    """Loop through inverters and check for updates."""
    global firstRun
    # Connect to the MQTT bus
    client = mqtt.Client("sunspec")
    try:
        client.connect(brokers_out["broker1"])
        for inverter in parsed_data:
            ip = inverter["ipaddr"]
            if(ip != ""):
                port = inverter.get("ipport", 502)
                slaveid = int(inverter["id"])
                name = inverter["name"].lower().replace(" ", "_")
                conattempts = 0
                while conattempts < 10:  # try each inverter ten times before moving on
                    try:
                        #print("Connecting to device " + name + " at " + ip + " id " + str(slaveid))
                        try:                      
                            device = sunspecclient.SunSpecClientDevice(
                                sunspecclient.TCP, slaveid, ipaddr=ip, ipport=port, timeout=10
                            )  # connect to the inverter
                        except Exception:
                            _LOGGER.exception(
                                "Connection error for inverter " + ip + ", skipping"
                            )
                            break
                        else:
                            time.sleep(0.5)
                            readattempts = 0
                            # try each register ten times before moving on
                            while readattempts < 10:
                                try:
                                    if(inverter["device"] == "battery"):
                                        device.inverter.read()
                                        device.storage.read()
                                        timestamp = int(time.time())
                                        if "PPVphAB" in inverter["registers"]:
                                            resp = {
                                                "entity": name,
                                                "sensorId": "battery" + "." + "voltage_phase_l1",
                                                "value": device.inverter.PPVphAB,
                                                "unit": "V",
                                                "metric": "BatteryElectricityVoltage",
                                                "metricKind" : "gauge",
                                                "timestamp": timestamp,
                                            }
                                            resp = json.dumps(resp, ensure_ascii=False)
                                            topic = "data/devices/" + "battery" + "/voltage_phase_l1"
                                            if firstRun:
                                                """Create Home Assistant sensors through MQTT once, delete any pre-existing sensors 
                                                (e.g. due to script reboot) by sending empty message first"""
                                                ctpc = "homeassistant/sensor/" + "battery" + "/voltage_phase_l1/config"
                                                crsp = ""
                                                try:
                                                    client.publish(ctpc, crsp)
                                                    time.sleep(0.25)
                                                    crsp = haAutoDiscover("voltage_phase_l1", "battery", "V")
                                                    client.publish(ctpc, crsp, retain=True)
                                                except:
                                                    _LOGGER.exception(
                                                        "Could not publish to " + ctpc
                                                    )
                                            try:
                                                client.publish(topic, resp)
                                            except:
                                                _LOGGER.exception(
                                                    "Could not publish to " + topic
                                                )
                                        if "PhVphA" in inverter["registers"]:
                                            resp = {
                                                "entity": name,
                                                "sensorId": "battery" + "." + "voltage_phase_l1",
                                                "value": device.inverter.PhVphA,
                                                "unit": "V",
                                                "metric": "BatteryElectricityVoltage",
                                                "metricKind" : "gauge",
                                                "timestamp": timestamp,
                                            }
                                            resp = json.dumps(resp, ensure_ascii=False)
                                            topic = "data/devices/" + "battery" + "/voltage_phase_l1"
                                            if firstRun:
                                                ctpc = "homeassistant/sensor/" + "battery" + "/voltage_phase_l1/config"
                                                crsp = ""
                                                try:
                                                    client.publish(ctpc, crsp)
                                                    time.sleep(0.25)
                                                    crsp = haAutoDiscover("voltage_phase_l1", "battery", "V")
                                                    client.publish(ctpc, crsp, retain=True)
                                                except:
                                                    _LOGGER.exception(
                                                        "Could not publish to " + ctpc
                                                    )
                                            try:
                                                client.publish(topic, resp)
                                            except:
                                                _LOGGER.exception(
                                                    "Could not publish to " + topic
                                                )
                                        if "A" in inverter["registers"]:
                                            resp = {
                                                "entity": name,
                                                "sensorId": "battery" + "." + "current_l1",
                                                "value": device.inverter.A,
                                                "unit": "A",
                                                "metric": "BatteryElectricityCurrent",
                                                "metricKind" : "gauge",
                                                "timestamp": timestamp,
                                            }
                                            resp = json.dumps(resp, ensure_ascii=False)
                                            topic = "data/devices/" + "battery" + "/current_l1"
                                            if firstRun:
                                                ctpc = "homeassistant/sensor/" + "battery" + "/current_l1/config"
                                                crsp = ""
                                                try:
                                                    client.publish(ctpc, crsp)
                                                    time.sleep(0.25)
                                                    crsp = haAutoDiscover("current_l1", "battery", "A")
                                                    client.publish(ctpc, crsp, retain=True)
                                                except:
                                                    _LOGGER.exception(
                                                        "Could not publish to " + ctpc
                                                    )
                                            try:
                                                client.publish(topic, resp)
                                            except:
                                                _LOGGER.exception(
                                                    "Could not publish to " + topic
                                                )
                                        if "W" in inverter["registers"]:
                                            resp = {
                                                "entity": name,
                                                "sensorId": "battery" + "." + "total_active_power",
                                                "value": device.inverter.W,
                                                "unit": "W",
                                                "metric": "BatteryElectricityPower",
                                                "metricKind" : "gauge",
                                                "timestamp": timestamp,
                                            }
                                            resp = json.dumps(resp, ensure_ascii=False)
                                            topic = "data/devices/" + "battery" + "/total_active_power"
                                            if firstRun:
                                                ctpc = "homeassistant/sensor/" + "battery" + "/total_active_power/config"
                                                crsp = ""
                                                try:
                                                    client.publish(ctpc, crsp)
                                                    time.sleep(0.25)
                                                    crsp = haAutoDiscover("total_active_power", "battery", "W")
                                                    client.publish(ctpc, crsp, retain=True)
                                                except:
                                                    _LOGGER.exception(
                                                        "Could not publish to " + ctpc
                                                    )
                                            try:
                                                client.publish(topic, resp)
                                            except:
                                                _LOGGER.exception(
                                                    "Could not publish to " + topic
                                                )
                                        if "WH" in inverter["registers"]:
                                            resp = {
                                                "entity": name,
                                                "sensorId": "battery" + "." + "total_energy_injected",
                                                "value": device.inverter.WH,
                                                "unit": "Wh",
                                                "metric": "BatteryElectricityExport",
                                                "metricKind" : "cumulative",
                                                "timestamp": timestamp,
                                            }
                                            resp = json.dumps(resp, ensure_ascii=False)
                                            topic = "data/devices/" + "battery" + "/total_energy_injected"
                                            if firstRun:
                                                ctpc = "homeassistant/sensor/" + "battery" + "/total_energy_injected/config"
                                                crsp = ""
                                                try:
                                                    client.publish(ctpc, crsp)
                                                    time.sleep(0.25)
                                                    crsp = haAutoDiscover("total_energy_injected", "battery", "Wh")
                                                    client.publish(ctpc, crsp, retain=True)
                                                except:
                                                    _LOGGER.exception(
                                                        "Could not publish to " + ctpc
                                                    )
                                            try:
                                                client.publish(topic, resp)
                                            except:
                                                _LOGGER.exception(
                                                    "Could not publish to " + topic
                                                )
                                        if "ChaState" in inverter["registers"]:
                                            resp = {
                                                "entity": name,
                                                "sensorId": name + "." + "state_of_charge_proc",
                                                "value": device.storage.ChaState,
                                                "unit": "%",
                                                "metric": "BatteryElectricitySoc", 
                                                "metricKind" : "gauge",
                                                "timestamp": timestamp,
                                            }
                                            resp = json.dumps(resp, ensure_ascii=False)
                                            topic = "data/devices/" + "battery" + "/state_of_charge_proc"
                                            if firstRun:
                                                ctpc = "homeassistant/sensor/" + "battery" + "/state_of_charge_proc/config"
                                                crsp = ""
                                                try:
                                                    client.publish(ctpc, crsp)
                                                    time.sleep(0.25)
                                                    crsp = haAutoDiscover("state_of_charge_proc", "battery", "%")
                                                    client.publish(ctpc, crsp, retain=True)
                                                except:
                                                    _LOGGER.exception(
                                                        "Could not publish to " + ctpc
                                                    )
                                            try:
                                                client.publish(topic, resp)
                                            except:
                                                _LOGGER.exception(
                                                    "Could not publish to " + topic
                                                )
                                        break
                                    else:
                                        device.inverter.read()
                                        timestamp = int(time.time())
                                        if "PPVphAB" in inverter["registers"]:
                                            resp = {
                                                "entity": name,
                                                "sensorId": "solar" + "." + "voltage_phase_l1",
                                                "value": device.inverter.PPVphAB,
                                                "unit": "V",
                                                "metric": "PvElectricityVoltage",
                                                "metricKind" : "gauge",
                                                "timestamp": timestamp,
                                            }
                                            resp = json.dumps(resp, ensure_ascii=False)
                                            topic = "data/devices/" + "solar" + "/voltage_phase_l1"
                                            if firstRun:
                                                """Create Home Assistant sensors through MQTT once, delete any pre-existing sensors 
                                                (e.g. due to script reboot) by sending empty message first"""
                                                ctpc = "homeassistant/sensor/" + "solar" + "/voltage_phase_l1/config"
                                                crsp = ""
                                                try:
                                                    client.publish(ctpc, crsp)
                                                    time.sleep(0.25)
                                                    crsp = haAutoDiscover("voltage_phase_l1", "solar", "V")
                                                    client.publish(ctpc, crsp, retain=True)
                                                except:
                                                    _LOGGER.exception(
                                                        "Could not publish to " + ctpc
                                                    )
                                            try:
                                                client.publish(topic, resp)
                                            except:
                                                _LOGGER.exception(
                                                    "Could not publish to " + topic
                                                )
                                        if "PhVphA" in inverter["registers"]:
                                            resp = {
                                                "entity": name,
                                                "sensorId": "solar" + "." + "voltage_phase_l1",
                                                "value": device.inverter.PhVphA,
                                                "unit": "V",
                                                "metric": "PvElectricityVoltage",
                                                "metricKind" : "gauge",
                                                "timestamp": timestamp,
                                            }
                                            resp = json.dumps(resp, ensure_ascii=False)
                                            topic = "data/devices/" + "solar" + "/voltage_phase_l1"
                                            if firstRun:
                                                ctpc = "homeassistant/sensor/" + "solar" + "/voltage_phase_l1/config"
                                                crsp = ""
                                                try:
                                                    client.publish(ctpc, crsp)
                                                    time.sleep(0.25)
                                                    crsp = haAutoDiscover("voltage_phase_l1", "solar", "V")
                                                    client.publish(ctpc, crsp, retain=True)
                                                except:
                                                    _LOGGER.exception(
                                                        "Could not publish to " + ctpc
                                                    )
                                            try:
                                                client.publish(topic, resp)
                                            except:
                                                _LOGGER.exception(
                                                    "Could not publish to " + topic
                                                )
                                        if "A" in inverter["registers"]:
                                            resp = {
                                                "entity": name,
                                                "sensorId": "solar" + "." + "current_l1",
                                                "value": device.inverter.A,
                                                "unit": "A",
                                                "metric": "PvElectricityCurrent",
                                                "metricKind" : "gauge",
                                                "timestamp": timestamp,
                                            }
                                            resp = json.dumps(resp, ensure_ascii=False)
                                            topic = "data/devices/" + "solar" + "/current_l1"
                                            if firstRun:
                                                ctpc = "homeassistant/sensor/" + "solar" + "/current_l1/config"
                                                crsp = ""
                                                try:
                                                    client.publish(ctpc, crsp)
                                                    time.sleep(0.25)
                                                    crsp = haAutoDiscover("current_l1", "solar", "A")
                                                    client.publish(ctpc, crsp, retain=True)
                                                except:
                                                    _LOGGER.exception(
                                                        "Could not publish to " + ctpc
                                                    )
                                            try:
                                                client.publish(topic, resp)
                                            except:
                                                _LOGGER.exception(
                                                    "Could not publish to " + topic
                                                )
                                        if "W" in inverter["registers"]:
                                            resp = {
                                                "entity": name,
                                                "sensorId": "solar" + "." + "total_active_power",
                                                "value": device.inverter.W,
                                                "unit": "W",
                                                "metric": "PvElectricityPower",
                                                "metricKind" : "gauge",
                                                "timestamp": timestamp,
                                            }
                                            resp = json.dumps(resp, ensure_ascii=False)
                                            topic = "data/devices/" + "solar" + "/total_active_power"
                                            if firstRun:
                                                ctpc = "homeassistant/sensor/" + "solar" + "/total_active_power/config"
                                                crsp = ""
                                                try:
                                                    client.publish(ctpc, crsp)
                                                    time.sleep(0.25)
                                                    crsp = haAutoDiscover("total_active_power", "solar", "W")
                                                    client.publish(ctpc, crsp, retain=True)
                                                except:
                                                    _LOGGER.exception(
                                                        "Could not publish to " + ctpc
                                                    )
                                            try:
                                                client.publish(topic, resp)
                                            except:
                                                _LOGGER.exception(
                                                    "Could not publish to " + topic
                                                )
                                        if "WH" in inverter["registers"]:
                                            resp = {
                                                "entity": name,
                                                "sensorId": "solar" + "." + "total_energy_injected",
                                                "value": device.inverter.WH,
                                                "unit": "Wh",
                                                "metric": "PvElectricityProduction",
                                                "metricKind" : "cumulative",
                                                "timestamp": timestamp,
                                            }
                                            resp = json.dumps(resp, ensure_ascii=False)
                                            topic = "data/devices/" + "solar" + "/total_energy_injected"
                                            if firstRun:
                                                ctpc = "homeassistant/sensor/" + "solar" + "/total_energy_injected/config"
                                                crsp = ""
                                                try:
                                                    client.publish(ctpc, crsp)
                                                    time.sleep(0.25)
                                                    crsp = haAutoDiscover("total_energy_injected", "solar", "Wh")
                                                    client.publish(ctpc, crsp, retain=True)
                                                except:
                                                    _LOGGER.exception(
                                                        "Could not publish to " + ctpc
                                                    )
                                            try:
                                                client.publish(topic, resp)
                                            except:
                                                _LOGGER.exception(
                                                    "Could not publish to " + topic
                                                )
                                        if "Hz" in inverter["registers"]:
                                            resp = {
                                                "entity": name,
                                                "sensorId": "solar" + "." + "frequency",
                                                "value": device.inverter.Hz,
                                                "unit": "Hz",
                                                "metricKind" : "gauge",
                                                "timestamp": timestamp,
                                            }
                                            resp = json.dumps(resp, ensure_ascii=False)
                                            topic = "data/devices/" + "solar" + "/frequency"
                                            if firstRun:
                                                ctpc = "homeassistant/sensor/" + "solar" + "/frequency/config"
                                                crsp = ""
                                                try:
                                                    client.publish(ctpc, crsp)
                                                    time.sleep(0.25)
                                                    crsp = haAutoDiscover("frequency", "solar", "Hz")
                                                    client.publish(ctpc, crsp, retain=True)
                                                except:
                                                    _LOGGER.exception(
                                                        "Could not publish to " + ctpc
                                                    )
                                            try:
                                                client.publish(topic, resp)
                                            except:
                                                _LOGGER.exception(
                                                    "Could not publish to " + topic
                                                )
                                        break
                                except:
                                    readattempts += 1
                                    time.sleep(0.5)
                                    if readattempts == 10:
                                        _LOGGER.exception(
                                            "Read error for inverter " + ip + ", skipping"
                                        )
                            device.close()
                            break
                    except Exception as ex:
                        if not isinstance(ex, ModbusClientError):
                            # Can't close d if d never defined
                            device.close()
                        conattempts += 1
                        time.sleep(0.5)
                        if conattempts == 10:
                            _LOGGER.exception(
                                "Connection error for inverter " + ip + ", skipping"
                            )
                            break
        client.disconnect()
        firstRun = False
    except:
        _LOGGER.exception(
            "Could not connect to MQTT broker, nothing to do anymore"
        )
    scheduler.enter(60, 1, fetch_inverter_updates, (parsed_data,))

if __name__ == "__main__":
    scheduler.enter(0, 1, fetch_inverter_updates, (data,))  # Zero delay, run immediately
    scheduler.run()
