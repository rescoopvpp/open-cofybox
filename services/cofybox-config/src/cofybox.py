import ipaddress
import datetime
import os
import uuid
from enum import Enum
from typing import List, Tuple, Optional
import pathlib
from zoneinfo import ZoneInfo, ZoneInfoNotFoundError

from pydantic import BaseModel
from pydantic import BaseSettings
from pydantic import Field
from pydantic import validator
from pydantic import AnyHttpUrl, SecretStr

from utils import as_form
from network import get_network_info


class Lang(str, Enum):
    english = "en"
    french = "fr"
    spanish = "es"
    german = "de"
    dutch = "nl"


class EnergyCommunity(str, Enum):
    carboncoop = "Carbon Co-op"
    energent = "Energent"
    enercoop = "Enercoop"
    burgerwerke = "Burgerwerke"
    somenergia = "Som Energia"


class CofyBoxConfigControl(BaseModel):
    control_selfcon_option: bool = Field(
        False, description="A flag intended to turn on/off self-consumption control."
    )
    control_tariff_option: bool = Field(
        None,
        description="A flag intended to turn on/off tariff / price optimal control.",
    )
    control_comm_option: bool = Field(
        None, description="A flag intended to turn on/off community control."
    )
    control_flex_option: bool = Field(
        None,
        description="A flag intended to turn on/off explicit DSR control (requires additional OpenADR configuration).",
    )
    control_tariff: str = Field(
        None, description="The tariff name/code used.", example=""
    )
    available_tariffs: List[str] = Field(
        None, description="A list of available tariffs synced from server."
    )
    community: EnergyCommunity = Field(
        None,
        description="The selected energy community (can be none but this may restrict other control options).",
        example="Enercoop",
    )
    enabled: bool = Field(
        None,
        description="Enables appliances and installations to be controlled by cofybox"
    )
    re_enable_at: Optional[str] = Field(
        None,
        description="Time at which the cloud will reenable control."
    )
    control_types: Optional[list ] = Field(
        None,
        description="The types of control enabled by the user. These can be explicit or automatic."
    )


class CofyBoxConfigUserLocation(BaseModel):
    location_latitude: float = Field(
        None,
        gt=-180.0,
        lt=180.0,
        description="Latitude of cofy-box install location.",
        example=112.0,
    )
    location_longitude: float = Field(
        None,
        gt=-180.0,
        lt=180.0,
        description="Longitude of cofy-box install location.",
        example=90.0,
    )


class CofyBoxConfigUserTimezone(BaseModel):
    timezone: str = Field(
        None, description="The IANA time zone.", example=str(ZoneInfo("Europe/London"))
    )


class CofyBoxConfigUserLanguage(BaseModel):
    lang: Lang = Field(
        None,
        description="A two letter language code controlling the language used in config UI (and potentially other things).",  # noqa: E501
        example="fr",
    )


class CofyBoxConfigUser(
    CofyBoxConfigUserLocation,
    CofyBoxConfigUserTimezone,
    CofyBoxConfigUserLanguage,
):
    @validator("timezone")
    def is_valid_iana_timezone(cls, v):
        if v:
            try:
                ZoneInfo(v)
            except ZoneInfoNotFoundError:
                ValueError("Invalid time zone!")
        return v

    # @validator('network_hotspot_name')
    # def is_valid_network_hotspot_name(cls, v):
    #     if v is not None:

    #         if not valid_ap_regex.match(v):
    #             raise ValueError('Invalid wifi WPA hotspot name!')
    #         return v


class CofyBoxConfigSolarHasSolar(BaseModel):
    has_solar: bool = Field(
        None,
        description="Whether the user has solar",  # noqa: E501
    )


class CofyBoxConfigSolarConfig(BaseModel):
    max_power_output: Optional[float] = Field(
        None,
        description="Maximum power output of solar array in kilowatts peak (kWp)",  # noqa: E501
        example="5.3",
    )
    declination: Optional[int] = Field(
        None,
        description="Angle between 0 (horizontal) and 90 (vertical)",  # noqa: E501
        example="35",
    )
    azimuth: Optional[int] = Field(
        None,
        description="Angle from 0(North) where 90 is East",  # noqa: E501
        example="190",
    )


class CofyBoxConfigSolar(
    CofyBoxConfigSolarHasSolar,
    CofyBoxConfigSolarConfig,
):
    pass


class CofyBoxInfoNetwork(BaseModel):
    wired_ip: ipaddress.IPv4Address = Field(
        None,
        description="The IP address of the wired network connection.",
        example=ipaddress.IPv4Address("192.168.1.21"),
    )
    wifi_client_ip: ipaddress.IPv4Address = Field(
        None,
        description="The IP address of the wireless network connection.",
        example=ipaddress.IPv4Address("192.168.1.121"),
    )
    wifi_client_ap: str = Field(
        None, description="The wifi client AP SSID name.", example="MyHomeWIFI"
    )
    wifi_hotspot_ap: str = Field(
        None, description="The wifi hotspot AP SSID name.", example="cofybox-0000"
    )
    wifi_hotspot_ip: ipaddress.IPv4Address = Field(
        None,
        description="The IP address of the wifi hotspot gateway.",
        example="10.42.0.1",
    )
    wifi_hotspot_pass: str = Field(
        None,
        description="The schema version of the config file (by date in reverse order with extra place on end).",
        example="shuishekabam213123",
        min_length=8,
    )


class CofyBoxInfoCloudStatus(str, Enum):
    connected = "connected"  # cofy-client has received authorization token and has recently connected to cofy-cloud
    disconnected = "disconnected"  # cofy-client has authorization token but cannot connect to cofy-cloud
    unassigned = "unassigned"  # cofy-client was able to reach the DPS endpoint but has received a non-200 status code
    failed = "failed"  # cofy-client is not authorized and has failed when attempting to connect to the DPS endpoint
    disabled = "disabled"  # undefined
    assigning = "assigning"  # undefined
    assigned = "assigned"  # undefined


class CofyBoxInfoCloud(BaseModel):
    status: CofyBoxInfoCloudStatus = Field(
        None, example=CofyBoxInfoCloudStatus.connected
    )
    assigned_queue: AnyHttpUrl = Field(
        None, example="x"
    )  # some validation on this may be possible - is this a URL?
    authorization_header_value: str = Field(
        None, example=""
    )  # some validation on this should be possible
    created_at: datetime.datetime = Field(None, example="")
    cloud_space_url: AnyHttpUrl = Field(None, example="")
    # New config sync stuff:
    base_url: AnyHttpUrl = Field(None, example="")
    api_key: str = Field(None, example="")


class CofyBoxInfo(BaseModel):
    """
    This models the dynamic state of the CoFyBox which is determined from the environment or communication with external interfaces.
    """

    uuid: uuid.UUID
    friendlyname: str
    cofycloud: CofyBoxInfoCloud = Field(...)
    network: CofyBoxInfoNetwork = Field(...)


@as_form
class CofyBoxConfigForm(BaseModel):
    lang: Lang = Field(None, description="User selected language for cofy-box.")
    cofycloud_code: str = Field(None, description="Pair code to cofy-cloud.")
    control_selfcon_option: bool = Field(None, description="Self-consumption on/off.")
    control_comm_option: bool = Field(None, description="Community control on/off.")
    control_flex_option: bool = Field(None, description="Explicit DSR control on/off.")
    control_tariff_option: bool = Field(None, description="Dynamic TOUT on/off.")
    location_latitude: float = Field(None, description="Latitude.")
    location_longitude: float = Field(None, description="Longitude.")
    timezone: str = Field(None, description="Timezone.")

    @validator(
        "control_selfcon_option",
        "control_comm_option",
        "control_flex_option",
        "control_tariff_option",
        pre=True,
    )
    def convert_none_radio_inputs_to_false(cls, v):
        if v is None:
            return False
        else:
            return True


class CofyConfigSettings(BaseSettings):
    """
    Setting variables automagically loaded from environment variables of capitalised same name.
    """

    balena_device_uuid: uuid.UUID
    balena_device_name_at_init: str  # TODO: validate
    cofybox_config_abs_path: pathlib.Path = "/cofybox-config/config.json"
    cofycloud_api_url: AnyHttpUrl
    cofycloud_registration_url: AnyHttpUrl
    cofybox_config_wifi_hotspot_enabled: bool = True
    cofybox_config_wifi_hotspot_ssid_base: str = "cofybox"

    @validator("balena_device_name_at_init")
    def device_must_have_a_name(v):
        if len(v) < 8:
            v = "fake-name"
        return v


class CofyBoxConfig(BaseModel):
    modifiedAt: datetime.datetime = Field(
        ...,
        description="Should be the last time this config was modified. If never modified should be null/None. Relies on clients updating.",  # noqa: E501
    )
    version: int = Field(
        202112011,
        gt=202112010,
        const=True,
        description="The schema version of the config file (by date in reverse order with extra place on end).",
    )
    control: CofyBoxConfigControl = Field(...)
    user: CofyBoxConfigUser = Field(...)
    solar: CofyBoxConfigSolar = Field(...)

    def update_from_form_input(self, form: CofyBoxConfigForm):
        "Update the object based on a cofy config form submission."

        if form.lang is not None:
            self.user.lang = form.lang
        if form.control_selfcon_option is not None:
            self.control.control_selfcon_option = form.control_selfcon_option
        if form.control_comm_option is not None:
            self.control.control_comm_option = form.control_comm_option
        if form.control_flex_option is not None:
            self.control.control_flex_option = form.control_flex_option
        if form.control_tariff_option is not None:
            self.control.control_tariff_option = form.control_tariff_option
        if form.location_latitude is not None:
            self.user.location_latitude = form.location_latitude
        if form.location_longitude is not None:
            self.user.location_longitude = form.location_longitude
        if form.timezone is not None:
            self.user.timezone = form.timezone

    def sync_to_file(self, file_path):
        """
        Sync the state of the CoFyBox runtime configuration to a file.

        Currently this will just overwrite the file.
        """
        # Warn on diff
        with open(file_path, "w") as text_file:
            text_file.write(self.json())


class unitEnum(str, Enum):
    """
    Enum for use in GlueRecipeConfig Pydantic model.
    """

    # W, kW, kWh, Wh, V, A, l, m³, kg, km, °C, %, count, none
    W = "W"
    kW = "kW"
    kWh = "kWh"
    Wh = "Wh"
    V = "V"
    A = "A"
    l = "l"  # noqa: E741
    m3 = "m3"
    kg = "kg"
    km = "km"
    C = "C"
    percent = "%"
    count = "count"
    none = "none"


class HADiscoveryPayloadDeviceClassEnum(str, Enum):
    """
    We only support a subset of the HA device classes.

    See: https://developers.home-assistant.io/docs/core/entity/sensor/#available-device-classes
    """

    battery = "battery"
    current = "current"
    energy = "energy"
    frequency = "frequency"
    gas = "gas"


class HADiscoveryPayloadStateClassEnum(str, Enum):
    """
    See: https://developers.home-assistant.io/docs/core/entity/sensor/#available-state-classes
    """

    measurement = "measurement"
    total = "total"
    total_increasing = "total_increasing"


class HADiscoveryPayloadDevice(BaseModel):
    identifiers: List[str]
    name: str
    model: str
    manufacturer: str


class HADiscoveryPayload(BaseModel):
    name: str
    unit_of_measurement: Optional[unitEnum]
    device_class: Optional[HADiscoveryPayloadDeviceClassEnum]
    state_class: Optional[HADiscoveryPayloadStateClassEnum]
    state_topic: str
    value_template: Optional[str]
    unique_id: Optional[str]
    last_reset: Optional[datetime.datetime]
    last_reset_value_template: Optional[datetime.datetime]
    device: Optional[HADiscoveryPayloadDevice]


def generate_fake_cofyboxinfo(settings: CofyConfigSettings) -> CofyBoxInfo:
    """
    A utility function used to generate some fake cofy-box info.
    """

    return CofyBoxInfo(
        uuid=settings.balena_device_uuid,
        friendlyname=settings.balena_device_name_at_init,
        cofycloud=CofyBoxInfoCloud(status="connected"),
        network=CofyBoxInfoNetwork(
            wifi_hotspot_ip=ipaddress.IPv4Address("192.168.1.121"),
            wifi_hotspot_ap="cofybox-0000",
            wired_ip=ipaddress.IPv4Address("10.42.0.1"),
        ),
    )


def generate_cofyboxinfo(settings: CofyConfigSettings) -> CofyBoxInfo:
    """
    A utility function used to generate a new CofyBoxInfo object.
    """

    try:
        wired_ip, wifi_client_ip, wifi_client_ap = get_network_info()
    except Exception as e:
        if os.environ.get("RUNNING_LOCALLY") == 'True':
            # If running locally we don't want to fail
            wired_ip = None
            wifi_client_ip = None
            wifi_client_ap = None
        else:
            raise e

    return CofyBoxInfo(
        uuid=settings.balena_device_uuid,
        friendlyname=settings.balena_device_name_at_init,
        cofycloud=CofyBoxInfoCloud(
            status="unassigned",
        ),
        network=CofyBoxInfoNetwork(
            wifi_client_ip=wifi_client_ip,
            wifi_client_ap=wifi_client_ap,
            wired_ip=wired_ip,
        ),
    )


def create_cofyboxconfig() -> CofyBoxConfig:
    """
    Generate an 'empty' CofyBox configuration file.
    """

    return CofyBoxConfig(
        modifiedAt=datetime.datetime.now(),
        control=CofyBoxConfigControl(),
        user=CofyBoxConfigUser(lang="en"),
        solar=CofyBoxConfigSolar(),
    )
