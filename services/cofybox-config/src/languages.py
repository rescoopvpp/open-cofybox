from pathlib import Path
import glob
import json
from typing import Dict, Callable

from babel.plural import PluralRule

plural_rule = PluralRule({"one": "n in 0..1"})


def generate_languages(languages_folder_path: Path) -> dict:
    """
    Localisation and translation support as per https://phrase.com/blog/posts/fastapi-i18n/
    """
    default_fallback = "en"
    languages = {}
    language_list = glob.glob(str(languages_folder_path))
    for lang in language_list:
        filename = Path(lang).name
        lang_code = filename.split(".")[0]
        with open(lang, "r", encoding="utf8") as file:
            languages[lang_code] = json.load(file)
    return languages


def plural_formatting_closure(languages: Dict) -> Callable:
    def plural_formatting(key_value, input, locale):
        """
        Function to support pluralisation
        """
        key = ""
        for i in languages[locale]:
            if key_value == languages[locale][i]:
                key = i
                break
        if not key:
            return key_value
        plural_key = f"{key}_plural"
        if plural_rule(input) != "one" and plural_key in languages[locale]:
            key = plural_key
        return languages[locale][key]

    return plural_formatting
