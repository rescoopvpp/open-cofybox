import logging
import json

from cofycloud import post_data_to_cofycloud_data_queue, CofyCloudDQPayload

logger = logging.getLogger("cofybox-config")

""" 
Ripped shamelessly from paho.mqtt because gmqtt lacks this functionality. See:
https://github.com/eclipse/paho.mqtt.python
"""


class MQTTMatcher(object):
    """Intended to manage topic filters including wildcards.
    Internally, MQTTMatcher use a prefix tree (trie) to store
    values associated with filters, and has an iter_match()
    method to iterate efficiently over all filters that match
    some topic name."""

    class Node(object):
        __slots__ = "_children", "_content"

        def __init__(self):
            self._children = {}
            self._content = None

    def __init__(self):
        self._root = self.Node()

    def __setitem__(self, key, value):
        """Add a topic filter :key to the prefix tree
        and associate it to :value"""
        node = self._root
        for sym in key.split("/"):
            node = node._children.setdefault(sym, self.Node())
        node._content = value

    def __getitem__(self, key):
        """Retrieve the value associated with some topic filter :key"""
        try:
            node = self._root
            for sym in key.split("/"):
                node = node._children[sym]
            if node._content is None:
                raise KeyError(key)
            return node._content
        except KeyError:
            raise KeyError(key)

    def __delitem__(self, key):
        """Delete the value associated with some topic filter :key"""
        lst = []
        try:
            parent, node = None, self._root
            for k in key.split("/"):
                parent, node = node, node._children[k]
                lst.append((parent, k, node))
            # TODO
            node._content = None
        except KeyError:
            raise KeyError(key)
        else:  # cleanup
            for parent, k, node in reversed(lst):
                if node._children or node._content is not None:
                    break
                del parent._children[k]

    def iter_match(self, topic):
        """Return an iterator on all values associated with filters
        that match the :topic"""
        lst = topic.split("/")
        normal = not topic.startswith("$")

        def rec(node, i=0):
            if i == len(lst):
                if node._content is not None:
                    yield node._content
            else:
                part = lst[i]
                if part in node._children:
                    for content in rec(node._children[part], i + 1):
                        yield content
                if "+" in node._children and (normal or i > 0):
                    for content in rec(node._children["+"], i + 1):
                        yield content
            if "#" in node._children and (normal or i > 0):
                content = node._children["#"]._content
                if content is not None:
                    yield content

        return rec(self._root)


def topic_matches_sub(sub, topic):
    """Check whether a topic matches a subscription.
    For example:
    foo/bar would match the subscription foo/# or +/bar
    non/matching would not match the subscription non/+/+
    """
    matcher = MQTTMatcher()
    matcher[sub] = True
    try:
        next(matcher.iter_match(topic))
        return True
    except StopIteration:
        return False


def on_connect(client, flags, rc, properties):
    client.subscribe("aggregated_data/#")
    client.subscribe("TEST/#")


async def on_message(client, topic: str, payload: str, qos: int, properties) -> int:
    # TODO: currently this just processes every MQTT message received on the
    # subscribed topics as if they are in a single format .
    # Note also that gmqtt does not support per-sub callbacks (unlike paho) so
    # need to match out messages here...

    json_payload = json.loads(payload)
    if topic_matches_sub("aggregated_data/#", topic):
        logger.debug(
            f"MQTT message received on aggregated_data: topic: {str(topic)},payload: {str(payload)}"
        )
        payload = CofyCloudDQPayload(
            deviceId=json_payload["tags"].get("cofybox_id"),
            sensorId=json_payload["tags"].get("sensor_id"),
            sensorName=json_payload["tags"].get("friendly_name"),
            metric=json_payload["tags"].get("metric"),
            metricKind=json_payload["tags"].get("metricKind"),
            unit=json_payload["tags"].get("unit"),
            timestamp=json_payload["timestamp"],
            value=json_payload["fields"].get("value_mean"),
        )
        status = post_data_to_cofycloud_data_queue(
            client.app.cofyboxinfo,
            client.app.cofyboxconfig,
            payload.json(exclude_unset=True),
        )
        client.app.cofyboxinfo.cofycloud.status = status
    elif topic_matches_sub("TEST/#", topic):
        logger.debug(
            f"MQTT message received on TEST: topic: {str(topic)},payload: {str(payload)}"
        )

    return 0  # must return zero


def on_disconnect(client, packet, exc=None):
    pass


def on_subscribe(client, mid, qos, properties):
    pass
