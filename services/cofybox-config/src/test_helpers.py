import os
import uuid
import pytest

from faker import Faker

cofybox_uuid = uuid.uuid4()

fake = Faker()
Faker.seed(0)
cofybox_fake_name = f"{fake.word()}-{fake.word()}"


@pytest.fixture
def ensure_env_vars():
    # Inject some fake config into environment
    env = {
        "BALENA_DEVICE_UUID": str(cofybox_uuid),
        "BALENA_DEVICE_NAME_AT_INIT": cofybox_fake_name,
        "COFYCLOUD_API_URL": "https://api.sandbox.cofybox.io/api/",
        "COFYCLOUD_REGISTRATION_URL": "https://dps.sandbox.cofybox.io/api/v1/devices",
        "COFYBOX_CONFIG_ABS_PATH": "./config.json",
    }

    for key, val in env.items():
        os.putenv(key, val)
        os.environ[key] = val
