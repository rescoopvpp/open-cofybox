import json

from fastapi.testclient import TestClient
from httpx import Response
import pytest

import main
from cofybox import generate_fake_cofyboxinfo, CofyConfigSettings

from test_helpers import ensure_env_vars


# Fixture to run all tests in src dir
@pytest.fixture(autouse=True)
def change_test_dir(request, monkeypatch):
    monkeypatch.chdir(request.fspath.dirname)


bridge_5_mill_latitude = 53.483719126469744
bridge_5_mill_longitude = -2.2157660574961513


class TestApi:
    def test_set_location(self, mocker, ensure_env_vars):
        # mocker.patch("main.MQTTClient.on_connect")
        mocker.patch("main.MQTTClient.connect")
        mocker.patch("main.MQTTClient.publish")
        mocker.patch("main.set_wifi_hotspot_ap")
        mock_generate_cofyboxinfo = mocker.patch("main.generate_cofyboxinfo")
        mock_generate_cofyboxinfo.return_value = generate_fake_cofyboxinfo(
            CofyConfigSettings()
        )

        with TestClient(main.app) as client:  # with closure to trigger startup event
            try:
                initial_get_response = client.get("/api/location")
                initial_location_json = initial_get_response.json()
                assert initial_location_json == {
                    "location_latitude": None,
                    "location_longitude": None,
                }

                post_response = client.post(
                    "/api/location",
                    headers={"Content-type": "application/json"},
                    data=json.dumps(
                        {
                            "location_latitude": bridge_5_mill_latitude,
                            "location_longitude": bridge_5_mill_longitude,
                        }
                    ),
                )
                assert post_response.status_code == 200

                new_location_response = client.get("/api/location")
                assert new_location_response.status_code == 200
                new_location_json = new_location_response.json()

                assert new_location_json == {
                    "location_latitude": bridge_5_mill_latitude,
                    "location_longitude": bridge_5_mill_longitude,
                }

                with open("config.json", "r") as f:
                    file_contents_json = json.loads(f.read())
                    assert (
                        file_contents_json["user"]["location_latitude"]
                        == bridge_5_mill_latitude
                    )
                    assert (
                        file_contents_json["user"]["location_longitude"]
                        == bridge_5_mill_longitude
                    )

            finally:
                client.post(
                    "/api/location",
                    headers={"Content-type": "application/json"},
                    data=json.dumps(
                        {"location_latitude": None, "location_longitude": None}
                    ),
                )

    def test_set_timezone(self, mocker, ensure_env_vars):
        # mocker.patch("main.MQTTClient.on_connect")
        mocker.patch("main.MQTTClient.connect")
        mocker.patch("main.MQTTClient.publish")
        mocker.patch("main.set_wifi_hotspot_ap")
        mock_generate_cofyboxinfo = mocker.patch("main.generate_cofyboxinfo")
        mock_generate_cofyboxinfo.return_value = generate_fake_cofyboxinfo(
            CofyConfigSettings()
        )

        with TestClient(main.app) as client:  # with closure to trigger startup event
            try:
                initial_get_response = client.get("/api/timezone")
                initial_timezone_json = initial_get_response.json()
                assert initial_get_response.status_code == 200
                assert initial_timezone_json == {"timezone": None}

                import pytz
                import random

                test_timezone = random.choice(pytz.all_timezones)

                post_response = client.post(
                    "/api/timezone",
                    headers={"Content-type": "application/json"},
                    data=json.dumps(
                        {
                            "timezone": test_timezone,
                        }
                    ),
                )
                assert post_response.status_code == 200

                new_timezone_response = client.get("/api/timezone")
                assert new_timezone_response.status_code == 200
                new_timezone_json = new_timezone_response.json()

                assert new_timezone_json == {"timezone": test_timezone}

                with open("config.json", "r") as f:
                    file_contents_json = json.loads(f.read())
                    assert file_contents_json["user"]["timezone"] == test_timezone

            finally:
                client.post(
                    "/api/timezone",
                    headers={"Content-type": "application/json"},
                    data=json.dumps({"timezone": None}),
                )
    
    @pytest.mark.skip(reason="tzwhere is broken so let's avoid guessing")
    def test_guess_timezone(self, mocker, ensure_env_vars):
        # mocker.patch("main.MQTTClient.on_connect")
        mocker.patch("main.MQTTClient.connect")
        mocker.patch("main.MQTTClient.publish")
        mocker.patch("main.set_wifi_hotspot_ap")
        mock_generate_cofyboxinfo = mocker.patch("main.generate_cofyboxinfo")
        mock_generate_cofyboxinfo.return_value = generate_fake_cofyboxinfo(
            CofyConfigSettings()
        )

        with TestClient(main.app) as client:  # with closure to trigger startup event
            try:
                initial_get_response = client.get("/api/timezone")
                initial_timezone_json = initial_get_response.json()
                assert initial_get_response.status_code == 200
                assert initial_timezone_json == {"timezone": None}

                post_response = client.post(
                    "/api/location",
                    headers={"Content-type": "application/json"},
                    data=json.dumps(
                        {
                            "location_latitude": bridge_5_mill_latitude,
                            "location_longitude": bridge_5_mill_longitude,
                        }
                    ),
                )
                assert post_response.status_code == 200

                new_timezone_response = client.get("/api/timezone")
                assert new_timezone_response.status_code == 200
                new_timezone_json = new_timezone_response.json()

                assert new_timezone_json == {"timezone": "Europe/London"}

                with open("config.json", "r") as f:
                    file_contents_json = json.loads(f.read())
                    assert file_contents_json["user"]["timezone"] == None

            finally:
                client.post(
                    "/api/location",
                    headers={"Content-type": "application/json"},
                    data=json.dumps(
                        {"location_latitude": None, "location_longitude": None}
                    ),
                )

    def test_set_language(self, mocker, ensure_env_vars):
        # mocker.patch("main.MQTTClient.on_connect")
        mocker.patch("main.MQTTClient.connect")
        mocker.patch("main.MQTTClient.publish")
        mocker.patch("main.set_wifi_hotspot_ap")
        mock_generate_cofyboxinfo = mocker.patch("main.generate_cofyboxinfo")
        mock_generate_cofyboxinfo.return_value = generate_fake_cofyboxinfo(
            CofyConfigSettings()
        )

        with TestClient(main.app) as client:  # with closure to trigger startup event
            try:
                initial_get_response = client.get("/api/language")
                initial_language_json = initial_get_response.json()
                assert initial_get_response.status_code == 200
                assert initial_language_json == {"language": "en"}

                test_language = "de"

                post_response = client.post(
                    "/api/language",
                    headers={"Content-type": "application/json"},
                    data=json.dumps(
                        {
                            "lang": test_language,
                        }
                    ),
                )
                assert post_response.status_code == 200

                new_language_response = client.get("/api/language")
                assert new_language_response.status_code == 200
                new_language_json = new_language_response.json()

                assert new_language_json == {"language": test_language}

                with open("config.json", "r") as f:
                    file_contents_json = json.loads(f.read())
                    assert file_contents_json["user"]["lang"] == test_language

            finally:
                client.post(
                    "/api/language",
                    headers={"Content-type": "application/json"},
                    data=json.dumps({"lang": "en"}),
                )

    def test_get_location_name(self, mocker):
        # mocker.patch("main.MQTTClient.on_connect")
        mocker.patch("main.MQTTClient.connect")
        mocker.patch("main.MQTTClient.publish")
        mocker.patch("main.set_wifi_hotspot_ap")
        mock_generate_cofyboxinfo = mocker.patch("main.generate_cofyboxinfo")
        mock_generate_cofyboxinfo.return_value = generate_fake_cofyboxinfo(
            CofyConfigSettings()
        )

        geocode_earth_return_value = {
            "features": [
                {"properties": {"confidence": 0.6, "label": "high confidence"}},
                {"properties": {"confidence": 0.4, "label": "low confidence"}},
            ]
        }

        mocker.patch(
            "rest_api.httpx.get",
            return_value=Response(200, text=json.dumps(geocode_earth_return_value)),
        )

        with TestClient(main.app) as client:  # with closure to trigger startup event
            try:
                initial_get_response = client.get("/api/location-name")
                assert initial_get_response.status_code == 200
                assert initial_get_response.json() == None

                post_response = client.post(
                    "/api/location",
                    headers={"Content-type": "application/json"},
                    data=json.dumps(
                        {
                            "location_latitude": bridge_5_mill_latitude,
                            "location_longitude": bridge_5_mill_longitude,
                        }
                    ),
                )
                assert post_response.status_code == 200

                new_location_name_get_response = client.get("/api/location-name")
                assert new_location_name_get_response.status_code == 200
                assert new_location_name_get_response.json() == "high confidence"

            finally:
                client.post(
                    "/api/location",
                    headers={"Content-type": "application/json"},
                    data=json.dumps(
                        {"location_latitude": None, "location_longitude": None}
                    ),
                )

    def test_set_has_solar(self, mocker, ensure_env_vars):
        # mocker.patch("main.MQTTClient.on_connect")
        mocker.patch("main.MQTTClient.connect")
        mocker.patch("main.MQTTClient.publish")
        mocker.patch("main.set_wifi_hotspot_ap")
        mock_generate_cofyboxinfo = mocker.patch("main.generate_cofyboxinfo")
        mock_generate_cofyboxinfo.return_value = generate_fake_cofyboxinfo(
            CofyConfigSettings()
        )

        with TestClient(main.app) as client:  # with closure to trigger startup event
            try:
                initial_get_response = client.get("/api/has-solar")
                initial_has_solar_json = initial_get_response.json()
                assert initial_get_response.status_code == 200
                assert initial_has_solar_json == {"has_solar": None}

                post_response = client.post(
                    "/api/has-solar",
                    headers={"Content-type": "application/json"},
                    data=json.dumps(
                        {
                            "has_solar": True,
                        }
                    ),
                )
                assert post_response.status_code == 200

                new_has_solar_response = client.get("/api/has-solar")
                assert new_has_solar_response.status_code == 200
                new_has_solar_json = new_has_solar_response.json()

                assert new_has_solar_json == {"has_solar": True}

                with open("config.json", "r") as f:
                    file_contents_json = json.loads(f.read())
                    assert file_contents_json["solar"]["has_solar"] == True

            finally:
                client.post(
                    "/api/has-solar",
                    headers={"Content-type": "application/json"},
                    data=json.dumps({"has_solar": None}),
                )

    def test_set_solar_config(self, mocker, ensure_env_vars):
        # mocker.patch("main.MQTTClient.on_connect")
        mocker.patch("main.MQTTClient.connect")
        mocker.patch("main.MQTTClient.publish")
        mocker.patch("main.set_wifi_hotspot_ap")
        mock_generate_cofyboxinfo = mocker.patch("main.generate_cofyboxinfo")
        mock_generate_cofyboxinfo.return_value = generate_fake_cofyboxinfo(
            CofyConfigSettings()
        )

        with TestClient(main.app) as client:  # with closure to trigger startup event
            try:
                initial_get_response = client.get("/api/solar-config")
                initial_solar_json = initial_get_response.json()
                empty_solar_config = {
                    "azimuth": None,
                    "declination": None,
                    "max_power_output": None,
                }
                assert initial_solar_json == empty_solar_config

                test_azimuth, test_declination, test_max_power_output = 190, 35, 3500

                post_response = client.post(
                    "/api/solar-config",
                    headers={"Content-type": "application/json"},
                    data=json.dumps(
                        {
                            "azimuth": test_azimuth,
                            "declination": test_declination,
                            "max_power_output": test_max_power_output,
                        }
                    ),
                )
                assert post_response.status_code == 200

                new_solar_config_response = client.get("/api/solar-config")
                assert new_solar_config_response.status_code == 200
                new_solar_json = new_solar_config_response.json()

                assert new_solar_json == {
                    "azimuth": test_azimuth,
                    "declination": test_declination,
                    "max_power_output": test_max_power_output,
                }

                with open("config.json", "r") as f:
                    file_contents_json = json.loads(f.read())
                    assert file_contents_json["solar"]["azimuth"] == test_azimuth
                    assert (
                        file_contents_json["solar"]["declination"] == test_declination
                    )
                    assert (
                        file_contents_json["solar"]["max_power_output"]
                        == test_max_power_output
                    )

            finally:
                client.post(
                    "/api/solar-config",
                    headers={"Content-type": "application/json"},
                    data=json.dumps(empty_solar_config),
                )
