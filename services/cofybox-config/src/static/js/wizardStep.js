const setupSlimSelectWithValue = (input, value) => {
    input.classList.remove('hidden');
    const slimSelectObject = new SlimSelect({
      select: input
    });
    slimSelectObject.set(value);
    slimSelectObject.enable();
    return slimSelectObject;
};
