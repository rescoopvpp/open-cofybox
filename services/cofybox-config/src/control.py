import collections
import json
import logging
import os
from dataclasses import dataclass
from dataclasses import field
from datetime import date
from datetime import datetime
from datetime import timedelta
from datetime import timezone
from typing import Union

import pytz
import httpx
import schedule
import voluptuous as vol
import yaml
from dateutil.parser import isoparse
from OctopusAgile import Agile

RATE_INTERVAL = timedelta(minutes=30)

config = None


class Dynamic2MQTT(mqtt.Client, Agile):
    HA_CURRENT_PRICE_BASE_TOPIC = "homeassistant/sensor/dynamic_price/current_price/"
    HA_NEXT_PRICE_BASE_TOPIC = "homeassistant/sensor/dynamic_price/next_price/"
    HA_PREVIOUS_PRICE_BASE_TOPIC = "homeassistant/sensor/dynamic_price/previous_price/"
    HA_RATES_BASE_TOPIC = "homeassistant/sensor/dynamic_price/upcoming_prices/"
    MQTT_BROKER = os.getenv("MQTT_BROKER", "localhost")

    BASE_URL = os.environ["COFYCLOUD_API_URL"]

    def __init__(self, client_id):

        Agile.__init__(self, area_code=config["agile_region"])

        self.schedule = DynamicSchedule()

    def get_rates(self, date_from: str, date_to: str = None) -> dict:
        if date_to is None:
            date_to = ""
        headers = {"content-type": "application/json"}

        # TODO: make this call async somehow
        r = httpx.get(
            f"{self.BASE_URL}/"
            f"v1/datasets/tariff?id=Octopus_{self.area_code}"
            f"&start={ date_from }&end={ date_to }",
            headers=headers,
        )
        results = r.json()["data"]
        logging.debug(r.url)

        date_rates = collections.OrderedDict()

        rate_list = []
        low_rate_list = []

        for result in results:
            price = result["value"]
            valid_from = result["timestamp"]
            date_rates[valid_from] = price
            rate_list.append(price)
            if price < 15:
                low_rate_list.append(price)

        return {
            "date_rates": date_rates,
            "rate_list": rate_list,
            "low_rate_list": low_rate_list,
        }

    def configure_homeassistant(self):
        self.publish(
            self.HA_CURRENT_PRICE_BASE_TOPIC + "config",
            payload=json.dumps(
                {
                    "name": "dynamic_price_current_price",
                    "state_topic": "data/sensor/dynamic_price/current_price",
                    "unit_of_measurement": config["currency_unit"],
                }
            ),
            retain=True,
        )
        self.publish(
            self.HA_NEXT_PRICE_BASE_TOPIC + "config",
            payload=json.dumps(
                {
                    "name": "dynamic_price_next_price",
                    "state_topic": "data/sensor/dynamic_price/next_price",
                    "unit_of_measurement": config["currency_unit"],
                }
            ),
            retain=True,
        )
        self.publish(
            self.HA_PREVIOUS_PRICE_BASE_TOPIC + "config",
            payload=json.dumps(
                {
                    "name": "dynamic_price_previous_price",
                    "state_topic": "data/sensor/dynamic_price/previous_price",
                    "unit_of_measurement": config["currency_unit"],
                }
            ),
            retain=True,
        )
        self.publish(
            self.HA_RATES_BASE_TOPIC + "config",
            payload=json.dumps(
                {
                    "state_topic": self.HA_RATES_BASE_TOPIC + "state",
                    "name": "dynamic_price_upcoming_prices",
                    "json_attributes_topic": "data/sensor/dynamic_price/upcoming_prices",
                }
            ),
            retain=True,
        )

    def configure_devices_for_homeassistant(self):
        for device_name, device_schedule in self.schedule.device_schedule.items():

            self.publish(
                f"homeassistant/binary_sensor/dynamic_price/{device_name}/config",
                payload=json.dumps(
                    {
                        "state_topic": f"set/dynamic_price/{device_name}/state",
                        "name": f"dynamic_price_{device_name}",
                        "json_attributes_topic": f"set/dynamic_price/{device_name}/attributes",
                    }
                ),
                retain=True,
            )

    def publish_prices(self):
        self.publish(
            "data/sensor/dynamic_price/upcoming_prices",
            payload=json.dumps(self.get_new_rates()["date_rates"]),
            retain=True,
        )
        self.publish(
            "data/sensor/dynamic_price/current_price",
            payload=self.get_current_rate(),
            retain=True,
        )
        self.publish(
            "data/sensor/dynamic_price/next_price",
            payload=self.get_next_rate(),
            retain=True,
        )
        self.publish(
            "data/sensor/dynamic_price/previous_price",
            payload=self.get_previous_rate(),
            retain=True,
        )

    def publish_device_updates(self):
        for device_name, device_schedule in self.schedule.device_schedule.items():

            self.publish_device_state(device_name, device_schedule)
            self.publish_device_attributes(device_name, device_schedule)

    def publish_device_state(self, device_name, device_schedule):
        time = self.round_time(datetime.utcnow())
        time = pytz.utc.localize(time)
        state = "on" if time in device_schedule else "off"

        self.publish(
            f"set/dynamic_price/{device_name}/state",
            payload=state.upper(),
        )

    def publish_device_attributes(self, device_name, device_schedule):
        attributes_payload = {
            "on_slots": json.dumps(device_schedule, default=str),
            "control_strategy": "cheapest_slots",
        }

        self.publish(
            f"set/dynamic_price/{device_name}/attributes",
            payload=json.dumps(attributes_payload),
            retain=True,
        )

    def run(self):

        self.publish_prices()
        self.configure_homeassistant()
        self.configure_devices_for_homeassistant()
        self.publish_device_updates()

        schedule.every().hour.do(self.publish_prices)
        schedule.every().hour.at(":30").do(self.publish_prices)
        schedule.every().hour.do(self.publish_device_updates)
        schedule.every().hour.at(":30").do(self.publish_device_updates)

        schedule.every().day.at("16:30").do(self.make_schedule)

        rc = 0
        while rc == 0:
            schedule.run_pending()
            rc = self.loop()
        return rc

    def make_schedule(self) -> None:
        """
        To be run once a day. Works out the optimum times to run appliances
        and stores it for the rest of the day.
        """
        if "devices" not in config or len(config["devices"]) == 0:
            return

        today = date.today()
        tomorrow = today + timedelta(days=1)
        seven_today = datetime(
            today.year, today.month, today.day, 19, 0, 0, tzinfo=timezone.utc
        )
        seven_tomorrow = seven_today + timedelta(days=1)

        # pass requirements into
        rates_in_period = self.get_rates(
            seven_today.strftime("%Y-%m-%dT%H:%M:%SZ"),
            seven_tomorrow.strftime("%Y-%m-%dT%H:%M:%SZ"),
        )["date_rates"]

        if len(rates_in_period) < 48:
            logging.warning(
                "Cannot create schedule: Rates not yet published for this day."
            )
            return

        self.schedule.device_schedule = {}
        for device_name, device in config["devices"].items():
            try:
                try:
                    num_hours: int = device["hours_required"]
                except KeyError:
                    logging.warning(
                        "Device " + device_name + " had no required hours defined"
                    )
                    continue

                # parse requirements
                requirements = device.get("requirements", [])
                parsed_requirements = []
                for requirement in requirements:
                    parsed_requirement = {
                        "slots": int(requirement["numHrs"]) * 2,
                        "numHrs": requirement["numHrs"],
                    }
                    if requirement["day_from"] == "today":
                        parsed_requirement["time_from"] = today.strftime(
                            f"%Y-%m-%dT{requirement['time_from']}Z"
                        )
                    elif requirement["day_from"] == "tomorrow":
                        parsed_requirement["time_from"] = tomorrow.strftime(
                            f"%Y-%m-%dT{requirement['time_from']}Z"
                        )

                    if requirement["day_to"] == "today":
                        parsed_requirement["time_to"] = today.strftime(
                            f"%Y-%m-%dT{requirement['time_to']}Z"
                        )
                    elif requirement["day_to"] == "tomorrow":
                        parsed_requirement["time_to"] = tomorrow.strftime(
                            f"%Y-%m-%dT{requirement['time_to']}Z"
                        )
                    parsed_requirements.append(parsed_requirement)

                # get cheapest times which match requirements
                num_slots = timedelta(hours=num_hours) // RATE_INTERVAL

                results = self.get_min_times(
                    num_slots, rates_in_period, parsed_requirements
                )
                self.schedule.device_schedule[device_name] = sorted(
                    list(map(lambda date_str: isoparse(date_str), list(results.keys())))
                )

            except Exception:
                logging.exception("Error creating schedule for " + device_name)
                continue

        self.schedule.scheduled_date = date.today()

        with open("schedule.json", "w") as output_schedule_file:
            json.dump(self.schedule, output_schedule_file, cls=ISODateJsonEncoder)


@dataclass
class DynamicSchedule:
    scheduled_date: date = None
    device_schedule: dict = field(default_factory=dict)


class ISODateJsonEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.isoformat()
        elif isinstance(obj, date):
            return obj.isoformat()
        elif isinstance(obj, DynamicSchedule):
            return obj.__dict__
        else:
            return obj


def datetime_parser(parsed_dict: dict) -> dict:
    for key, value in parsed_dict.items():
        if isinstance(value, str):
            parsed_dict[key] = _convert_to_datetime(value)
        elif isinstance(value, list):
            parsed_dict[key] = list(map(_convert_to_datetime, value))
    return parsed_dict


def _convert_to_datetime(value: str) -> Union[date, datetime, str]:
    """Converts a string to a date or datetime if possible.
    Date objects prioritised over datetime objects."""
    try:
        return date.fromisoformat(value)
    except ValueError:
        try:
            return datetime.fromisoformat(value)
        except ValueError:
            return value


def valid_config(config_object):
    """Validates the settings in the configuration yaml file. This is a work in progress
    and doesn't validate all settings yet"""
    agile_regions = [
        "A",
        "B",
        "C",
        "D",
        "E",
        "F",
        "G",
        "H",
        "J",
        "K",
        "L",
        "M",
        "N",
        "P",
    ]
    schema = vol.Schema(
        {
            "agile_region": vol.In(agile_regions),
            "currency_unit": vol.In("pence", "cent"),
            "devices": vol.Optional(dict),
        }
    )
    try:
        schema(config_object)
        return config_object
    except vol.Invalid as e:
        logging.exception(f"Invalid config schema {e}")
        restart_dynamic2mqtt_service(
            message="Check yaml file - configuration incorrect"
        )


# if __name__ == "__main__":
#     logging.info("Launching dynamic2MQTT")
#     config_file_path = "/cofybox-config/dynamic2mqtt_configuration.yaml"
#     with open(config_file_path, "r") as yamlfile:
#         config = yaml.safe_load(yamlfile)

#     config = valid_config(config)
#     dynamic2mqtt_client = Dynamic2MQTT("dynamic2mqtt")

#     try:
#         with open("schedule.json", "r") as schedule_file:
#             dynamic2mqtt_client.schedule = DynamicSchedule(
#                 **json.load(schedule_file, object_hook=datetime_parser)
#             )

#     except FileNotFoundError:
#         logging.info("No saved schedule found, creating new one.")
#         dynamic2mqtt_client.make_schedule()
#     except json.JSONDecodeError:
#         logging.error("Could not parse stored schedule- creating new one.")
#         dynamic2mqtt_client.make_schedule()

#     dynamic2mqtt_client.run()
