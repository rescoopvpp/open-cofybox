import os
import uuid

from faker import Faker
import httpx
from fastapi.testclient import TestClient
import pytest
from pytest_httpx import HTTPXMock

import main
from cofycloud import update_cofycloud_status
from cofybox import generate_fake_cofyboxinfo, create_cofyboxconfig, CofyConfigSettings

# Inject some fake config into environment

fake = Faker()
Faker.seed(0)
cofybox_fake_name = f"{fake.word()}-{fake.word()}"

cofybox_uuid = uuid.uuid4()
env = {
    "BALENA_DEVICE_UUID": str(cofybox_uuid),
    "BALENA_DEVICE_NAME_AT_INIT": cofybox_fake_name,
    "COFYCLOUD_API_URL": "https://api.sandbox.cofybox.io/api/",
    "COFYCLOUD_REGISTRATION_URL": "https://dps.sandbox.cofybox.io/api/v1/devices",
    "COFYBOX_CONFIG_ABS_PATH": "./config.json",
}

for key, val in env.items():
    os.putenv(key, val)
    os.environ[key] = val


def test_cofycloud_client_basic(httpx_mock: HTTPXMock, mocker):
    httpx_mock.add_response("{}")

    mocker.patch("main.MQTTClient.on_connect")
    mocker.patch("main.MQTTClient.connect")
    mocker.patch("main.MQTTClient.publish")
    mocker.patch("main.set_wifi_hotspot_ap")
    settings = CofyConfigSettings()
    cofyboxinfo = generate_fake_cofyboxinfo(settings)
    cofyboxconfig = create_cofyboxconfig()
    update_cofycloud_status(cofyboxinfo, cofyboxconfig, settings)
