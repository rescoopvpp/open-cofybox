import logging
import json
import uuid
import datetime
from typing import Optional

import httpx
from pydantic import BaseModel, Field, validator, AnyHttpUrl, SecretStr

from cofybox import (
    CofyBoxInfo,
    CofyBoxInfoCloudStatus,
    CofyConfigSettings,
    CofyBoxConfig,
)
from utils import actions_after_updating_config

logger = logging.getLogger("cofybox-config")

def to_camel(string: str) -> str:
    string_split = string.split('_')
    return string_split[0]+''.join(word.capitalize() for word in string_split[1:])

class FixedModel(BaseModel):
    class Config:
        alias_generator = to_camel

class CofyCloudDPSRequestPayload(BaseModel):
    """
    See: https://dps.sandbox.cofybox.io/index.html
    """

    registrationToken: uuid.UUID
    timeZone: str = Field(None, alias="properties.TimeZone")

    @validator("timeZone")
    def is_valid_iana_timezone(cls, v):
        if v is not None:
            try:
                ZoneInfo(v)
            except ZoneInfoNotFoundError:
                ValueError("Invalid time zone!")
        return v


class CofyCloudDPSResponseServiceBusPayload(BaseModel):
    """
    See: https://dps.sandbox.cofybox.io/index.html
    """

    assignedQueue: str  # some validation may be possible
    authorizationHeaderValue: str  # some validation should be possible

class CofyCloudDPSResponseApiPayload(BaseModel):
    """
    See: https://dps.sandbox.cofybox.io/index.html
    """

    baseUrl: str  # some validation may be possible
    authorizationHeaderValue: str  # some validation should be possible

class CofyCloudDPSResponsePayload(BaseModel):
    """
    See: https://dps.sandbox.cofybox.io/index.html
    """

    deviceId: uuid.UUID
    cloudSpaceUrl: AnyHttpUrl
    createdDateTimeUtc: datetime.datetime
    serviceBus: CofyCloudDPSResponseServiceBusPayload
    api: CofyCloudDPSResponseApiPayload


class CofyCloudConfigSyncPV(FixedModel):
    """
    See: https://dps.sandbox.cofybox.io/index.html
    """

    enabled: bool
    peak_power: Optional[float] = Field(
        None,
        description="Maximum power output of solar array in kilowatts peak (kWp)",  # noqa: E501
        example="5.3",
    )
    declination: Optional[int] = Field(
        None,
        description="Angle between 0 (horizontal) and 90 (vertical)",  # noqa: E501
        example="35",
    )
    azimuth: Optional[int] = Field(
        None,
        description="Angle from 0(North) where 90 is East",  # noqa: E501
        example="190",
    )

class CofyCloudConfigSyncAddress(FixedModel):
    """
    See: https://dps.sandbox.cofybox.io/index.html
    """

    latitude: float = Field(
        None,
        gt=-180.0,
        lt=180.0,
        description="Latitude of cofy-box install location.",
        example=112.0,
    )
    longitude: float = Field(
        None,
        gt=-180.0,
        lt=180.0,
        description="Longitude of cofy-box install location.",
        example=90.0,
    )
    time_zone: str
    postal_code: Optional[str]
    city: str
    country: str
    country_code: str


class CofyCloudConfigSyncControl(FixedModel):
    """
    See: https://docs.cofybox.io/cofycloud/config-sync
    """

    enabled: bool
    re_enable_at: Optional[str]
    control_types: Optional[list]


class CofyCloudConfigSync(FixedModel):
    address: CofyCloudConfigSyncAddress
    pv_forecasting: CofyCloudConfigSyncPV
    control: CofyCloudConfigSyncControl


class CofyCloudConfigSyncDesiredParser(BaseModel):
    desired: CofyCloudConfigSync


class CofyCloudConfigSyncReportedParser(BaseModel):
    reported: CofyCloudConfigSync


class CofyCloudDQPayload(BaseModel):
    # TODO: import same models from glue for better validation
    deviceId: uuid.UUID
    sensorId: str
    sensorName: Optional[str]
    metric: str
    metricKind: str
    unit: str
    timestamp: str
    value: str


def update_cofycloud_status(
    cofyboxinfo: CofyBoxInfo, cofyboxconfig: CofyBoxConfig, settings: CofyConfigSettings
):
    """
    If previously authorized to cofycloud poll the data API.

    If not, try to get an authorization header token from the cofycloud DPS API.

    Also infer cofycloud connection status and update CofyBoxInfo state accordingly.
    """
    if cofyboxinfo.cofycloud.authorization_header_value is None:
        logger.info(f"Preparing request using cofyboxconfig: {cofyboxconfig}")
        request_params = {
            "registrationToken": settings.balena_device_uuid,
            "properties.displayName": cofyboxinfo.friendlyname,
        }
        try:
            r = httpx.post(settings.cofycloud_registration_url, params=request_params)
            logger.info(f"A request was sent: {request_params}")
        except httpx.RequestError as exc:
            logger.error(
                f"An error occurred while requesting from the DPS endpoint: {exc.request.url!r}."
            )
            logger.error(f"Request will be retried at a later time.")
        else:
            match r.status_code:  # type: ignore
                case 200:
                    # Parse response and extract cloud connection parameters
                    payload = CofyCloudDPSResponsePayload.parse_obj(r.json())

                    # Test consistency of returned device ID with internal ID (under what circumstances would this be different?)
                    if payload.deviceId != settings.balena_device_uuid:
                        logger.error(
                            f"Hardware UUID does not match cloud ID!: {payload.deviceId} / {settings.balena_device_uuid}"
                        )
                        logger.error(
                            "Execution will continue but this could indicate a problem!"
                        )

                    cofyboxinfo.cofycloud.status = CofyBoxInfoCloudStatus.connected

                    cofyboxinfo.cofycloud.assigned_queue = payload.serviceBus.assignedQueue
                    cofyboxinfo.cofycloud.authorization_header_value = (
                        payload.serviceBus.authorizationHeaderValue
                    )
                    cofyboxinfo.cofycloud.base_url = payload.api.baseUrl
                    cofyboxinfo.cofycloud.api_key = payload.api.authorizationHeaderValue

                    cofyboxinfo.cofycloud.cloud_space_url = payload.cloudSpaceUrl
                    cofyboxinfo.cofycloud.created_at = payload.createdDateTimeUtc

                    logger.info(
                        f"CoFyCloud info has been initalised with: {str(cofyboxinfo)}"
                    )

                    logger.info(f"Writing api information to file")
                    with open("/cofybox-config/api_config.json", "w") as f:
                        json.dump(r.json()["api"], f)

                case 400:
                    cofyboxinfo.cofycloud.status = CofyBoxInfoCloudStatus.unassigned

                    # TODO: improve this error message once DPS API error messages have been rationalised
                    logger.error(
                        f"Problem registering device id: {settings.balena_device_uuid}\n"
                        "The problem is probably one of...\n"
                        "* The device has not been enrolled on cofy-cloud yet.\n"
                        "* API has changed.\n"
                        "* Time zone is not set / wrong.\n"
                        f"The server says: {r.text}\n"
                        "Registration will be retried later...\n"
                    )

                case _:
                    logger.error(
                        f"Server has returned HTTP status code which is not 200 or 400. Check API spec has not changed!"
                    )

    else:
        if cofyboxinfo.cofycloud.assigned_queue is not None:
            # post_data_to_cofycloud_data_queue(cofyboxinfo, cofyboxconfig, {}) # <- this generates lots of dead letter queue messages
            pass
        else:
            logger.error(
                "Cloud connection has been initialised but CoFyCloud data queue endpoint is not defined so no data will be sent!"
            )
            logger.error("Cloud connection may need to be re-initialised!")


def post_data_to_cofycloud_data_queue(
    cofyboxinfo: CofyBoxInfo, cofyboxconfig: CofyBoxConfig, payload: str
) -> CofyBoxInfoCloudStatus:
    """
    Attempt to send data payloads passed to this function upstream to the cofycloud.

    This data will likely have been received from either the MQTT bus or retrieved from InfluxDB.

    This assumes that the device has already been provisioned to the cofycloud but this may not be the case.
    If this is not the case it should log an informative error message.
    """

    status = CofyBoxInfoCloudStatus.failed  # TODO: a better status may be possible...

    logger.info(f"Polling the CoFyCloud data queue endpoint.")
    try:
        headers = {
            "Authorization": cofyboxinfo.cofycloud.authorization_header_value,
            "Content-Type": "application/json",
        }

        r = httpx.post(
            cofyboxinfo.cofycloud.assigned_queue, headers=headers, data=payload
        )
    except httpx.RequestError as exc:
        logger.error(
            f"An error occurred while polling the CoFyCloud data queue endpoint: {exc.request.url!r} {headers}."
        )
        logger.error(f"Request will be retried at a later time.")
        status = CofyBoxInfoCloudStatus.disconnected

    match r.status_code:  # type: ignore
        case 201:
            logger.info(
                f"A response was received from the CoFyCloud data queue endpoint: {r.request} / {r.request.headers['Authorization']} / {r.text} / {r.status_code}"
            )
            status = CofyBoxInfoCloudStatus.connected
        case 400:
            logger.error(
                f"A bad status code was received from the CoFyCloud data queue endpoint: {r.request} / {r.request.headers['Authorization']} / {r.text} / {r.status_code}"
            )
            status = CofyBoxInfoCloudStatus.failed
        case _:
            logger.info(
                f"An undefined status code was received from the CoFyCloud data queue endpoint: {r.request} / {r.request.headers['Authorization']} / {r.text} / {r.status_code}"
            )
            logger.info(
                "This may indicate some sort of problem requiring further investigation."
            )
            # TODO: status = CofyBoxInfoCloudStatus.unknown?

    logger.info(
        f"A response was received from the CoFyCloud data queue endpoint: {r.request} / {r.request.headers['Authorization']} / {r.text} / {r.status_code}"
    )

    return status

def cofycloud_config_sync(app):

    if app.cofyboxinfo.cofycloud.api_key is None:
        logger.info("The device has not been set up with an API key to access the cloud configuration sync service. Sync will not commence.")
        return

    logger.info("Sync commencing...")

    request_headers = {
            "Authorization": app.cofyboxinfo.cofycloud.api_key,
        }

    try:    
        r = httpx.get(f'{app.cofyboxinfo.cofycloud.base_url}/properties', headers=request_headers)
    except httpx.RequestError as exc:
        logger.error(
            f"An error occurred while requesting from the device twin endpoint: {exc.request.url!r}."
        )
        logger.error("Request will be retried at a later time.")

    else:
        logger.info(f"A GET request was sent to device configuration endpoint.")
        unparsed_payload_json = r.json()
        logger.info(f"The cloud digital twin possesses these properties: {r.content.decode()}")
        match r.status_code:
            case 200:
                if "desired" in unparsed_payload_json:
                    # Parse response and extract config sync parameters
                    logger.info("Parsing desired properties..")
                    logger.info(unparsed_payload_json)
                    desired_payload = CofyCloudConfigSyncDesiredParser.parse_obj(unparsed_payload_json)
                    address = desired_payload.desired.address
                    pv = desired_payload.desired.pv_forecasting
                    control = desired_payload.desired.control

                    cofyboxconfig = app.cofyboxconfig

                    cofyboxconfig.user.location_latitude = address.latitude
                    cofyboxconfig.user.location_longitude = address.longitude

                    cofyboxconfig.solar.has_solar = pv.enabled
                    cofyboxconfig.solar.max_power_output = pv.peak_power
                    cofyboxconfig.solar.declination = pv.declination
                    cofyboxconfig.solar.azimuth = pv.azimuth

                    cofyboxconfig.control.enabled = control.enabled
                    cofyboxconfig.control.re_enable_at = control.re_enable_at
                    cofyboxconfig.control.control_types = control.control_types

                    actions_after_updating_config(app)
                    
                    data = {
                        'address.latitude': cofyboxconfig.user.location_latitude,
                        'address.longitude': cofyboxconfig.user.location_longitude,
                        'pvForecasting.enabled': cofyboxconfig.solar.has_solar,
                        'pvForecasting.peakPower': cofyboxconfig.solar.max_power_output,
                        'pvForecasting.azimuth': cofyboxconfig.solar.azimuth,
                        'pvForecasting.declination': cofyboxconfig.solar.declination,
                        'control.enabled': cofyboxconfig.control.enabled,
                        'control.reEnableAt': cofyboxconfig.control.re_enable_at,
                        'control.controlTypes': cofyboxconfig.control.control_types,
                    }

                    try:
                        r_2 = httpx.post(f'{app.cofyboxinfo.cofycloud.base_url}/properties', headers=request_headers, data=data)
                        logger.info(f"A POST request was sent to device configuration endpoint.")
                    except httpx.RequestError as exc:
                        logger.error(
                            f"An error occurred while requesting from the device twin endpoint: {exc.request.url!r}."
                        )
                        logger.error("Request will be retried at a later time.")
                    else:
                        if r_2.status_code == 200:
                            logger.info(f'Device has reported the properties. These are now: {r_2.content.decode()}.')
                        else:
                            logger.info(r_2.status_code)
                            logger.info(r_2.content.decode())
                            logger.error("There was a problem reporting the properties.")

                elif "reported" in unparsed_payload_json:
                    logger.info("Cloud backend has the reported properties. Checking if they are on the device..")
                    reported_payload = CofyCloudConfigSyncReportedParser.parse_obj(unparsed_payload_json)

                    address = reported_payload.reported.address
                    pv = reported_payload.reported.pv_forecasting
                    control = reported_payload.reported.control

                    cofyboxconfig = app.cofyboxconfig

                    if not any([cofyboxconfig.user.location_latitude,
                            cofyboxconfig.user.location_longitude,
                            cofyboxconfig.solar.has_solar,
                            cofyboxconfig.solar.max_power_output,
                            cofyboxconfig.solar.declination,
                            cofyboxconfig.solar.azimuth,
                            cofyboxconfig.control.enabled ]):
                        logger.info("Null settings found on device. Applying reported properties from the cloud.")

                        cofyboxconfig.user.location_latitude = address.latitude
                        cofyboxconfig.user.location_longitude = address.longitude

                        cofyboxconfig.solar.has_solar = pv.enabled
                        cofyboxconfig.solar.max_power_output = pv.peak_power
                        cofyboxconfig.solar.declination = pv.declination
                        cofyboxconfig.solar.azimuth = pv.azimuth

                        cofyboxconfig.control.enabled = control.enabled
                        cofyboxconfig.control.re_enable_at = control.re_enable_at
                        cofyboxconfig.control.control_types = control.control_types

                        actions_after_updating_config(app)

                    if any([cofyboxconfig.user.location_latitude != address.latitude,
                            cofyboxconfig.user.location_longitude != address.longitude,
                            cofyboxconfig.solar.has_solar != pv.enabled,
                            cofyboxconfig.solar.max_power_output != pv.peak_power,
                            cofyboxconfig.solar.declination != pv.declination,
                            cofyboxconfig.solar.azimuth != pv.azimuth,
                            cofyboxconfig.control.enabled != control.enabled,]):
                        logger.info("Reported settings are different to local settings. Making a request to change them on the cloud.")
                        
                        data = {
                            'address.latitude': cofyboxconfig.user.location_latitude,
                            'address.longitude': cofyboxconfig.user.location_longitude,
                            'pvForecasting.enabled': cofyboxconfig.solar.has_solar,
                            'pvForecasting.peakPower': cofyboxconfig.solar.max_power_output,
                            'pvForecasting.azimuth': cofyboxconfig.solar.azimuth,
                            'pvForecasting.declination': cofyboxconfig.solar.declination,
                            'control.enabled': cofyboxconfig.control.enabled,
                            'control.reEnableAt': cofyboxconfig.control.re_enable_at,
                            'control.controlTypes': cofyboxconfig.control.control_types,
                        }

                        try:
                            r_2 = httpx.post(f'{app.cofyboxinfo.cofycloud.base_url}/properties', headers=request_headers, data=data)
                            logger.info(f"A POST request was sent to device configuration endpoint.")
                            logger.info(r_2.json())
                        except httpx.RequestError as exc:
                            logger.error(
                                "An error occurred while requesting from the device twin endpoint: {exc.request.url!r}."
                            )
                            logger.error("Request will be retried at a later time.")
                        else:
                            if r_2.status_code == 200:
                                logger.info(f'Device has reported the properties. These are now: {r_2.content.decode()}.')
                            else:
                                logger.error("There was a problem reporting the properties.")
                    else:
                        logger.info("Latitude, longitude, PV and control settings are synchronised between cloud and device.")
            case 400:
                logger.error(
                    f"A bad status code was received from the CoFyCloud data queue endpoint: {r.request} / {r.request.headers['Authorization']} / {r.text} / {r.status_code}"
                )
            
            case 401:
                logger.error(
                    f"Device is not authorized to access device configuration sync endpoint: {r.request} / {r.request.headers['Authorization']} / {r.text} / {r.status_code}"
                )

