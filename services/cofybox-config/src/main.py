import datetime
import glob
import json
import logging
import os
import re
import uuid
from pathlib import Path
import urllib.parse

from fastapi import Depends
from fastapi import FastAPI
from fastapi import Request
from fastapi.responses import HTMLResponse, JSONResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from fastapi_utils.tasks import repeat_every
from gmqtt import Client as MQTTClient
from pydantic import ValidationError
import httpx


from cofybox import (
    generate_cofyboxinfo,
    create_cofyboxconfig,
    CofyConfigSettings,
    CofyBoxConfig,
    HADiscoveryPayloadDevice,
    HADiscoveryPayload,
    CofyBoxConfigForm,
)
from network import get_network_info, set_wifi_hotspot_ap
from cofycloud import update_cofycloud_status, post_data_to_cofycloud_data_queue, cofycloud_config_sync
from mqtt import on_connect, on_message, on_disconnect, on_subscribe
from languages import generate_languages, plural_formatting_closure

# LOGGING SETUP FOR UVICORN
# The below is required because uvicorn / gunicorn will override whatever is set normally, even at this level.
# This is a known issue with uvicorn / gunicorn which may be addressed in future.
# TODO: switch logging to loguru
logging_level = logging.INFO
logger = logging.getLogger("cofybox-config")

if "gunicorn" in os.environ.get("SERVER_SOFTWARE", ""):
    gunicorn_error_logger = logging.getLogger("gunicorn.error")
    gunicorn_logger = logging.getLogger("gunicorn")

    fastapi_logger.setLevel(gunicorn_logger.level)
    fastapi_logger.handlers = gunicorn_error_logger.handlers
    logger.setLevel(gunicorn_logger.level)

    uvicorn_logger = logging.getLogger("uvicorn.access")
    uvicorn_logger.handlers = gunicorn_error_logger.handlers
else:
    # https://github.com/tiangolo/fastapi/issues/2019
    LOG_FORMAT2 = "[%(asctime)s %(process)d:%(threadName)s] %(name)s - %(levelname)s - %(message)s | %(filename)s:%(lineno)d"
    logging.basicConfig(level=logging_level, format=LOG_FORMAT2)

# Generate templates from templates folder
templates = Jinja2Templates(directory="templates/")
# assign filter to Jinja2
languages = generate_languages(
    (Path(__file__).parent / Path("languages/*.json")).resolve()
)
templates.env.filters["plural_formatting"] = plural_formatting_closure(languages)

GEOCODE_EARTH_API_KEY = os.environ.get("GEOCODE_EARTH_API_KEY", "")
USE_GEOCODE_EARTH_API_DIRECTLY = (
    os.environ.get("USE_GEOCODE_EARTH_API_DIRECTLY", "True") == "True"
)


def generate_timezones_by_group():
    import pytz

    groups = list(set([x.split("/")[0] for x in pytz.all_timezones if "/" in x]))
    timezone_options_by_group = {
        g: [t for t in pytz.all_timezones if t.startswith(g)] for g in groups
    }
    timezone_options_by_group["Other"] = [x for x in pytz.all_timezones if not "/" in x]
    return timezone_options_by_group


def generate_languages():
    return [
        {"display_name": "English", "code": "en"},
        {"display_name": "Español", "code": "es"},
        {"display_name": "Français", "code": "fr"},
        {"display_name": "Deutsch", "code": "de"},
        {"display_name": "Dutch", "code": "nl"},
    ]


def generate_timezones_by_group():
    import pytz

    groups = list(set([x.split("/")[0] for x in pytz.all_timezones if "/" in x]))
    timezone_options_by_group = {
        g: [t for t in pytz.all_timezones if t.startswith(g)] for g in groups
    }
    timezone_options_by_group["Other"] = [x for x in pytz.all_timezones if not "/" in x]
    return timezone_options_by_group


def generate_languages():
    return [
        {"display_name": "English", "code": "en"},
        {"display_name": "Español", "code": "es"},
        {"display_name": "Français", "code": "fr"},
        {"display_name": "Deutsch", "code": "de"},
        {"display_name": "Nederlands", "code": "nl"},
    ]


def render_home(request: Request):
    """
    This renders the HTML for the main page using the Jinja2 templates.
    """

    app = request.app
    cofyboxconfig = app.cofyboxconfig
    cofyboxinfo = app.cofyboxinfo

    # Result must include request object
    result = {"request": request}

    # Localisation
    result.update(languages[cofyboxconfig.user.lang])
    result.update({"locale": cofyboxconfig.user.lang})

    # Add variables to template
    result.update({"cofyboxconfig": cofyboxconfig, "cofyboxinfo": cofyboxinfo})

    return templates.TemplateResponse("index.html", result)


def render_wizard(request: Request, step_name: None | str = None):
    """
    This renders the HTML for the wizard page using the Jinja2 templates.
    """
    app = request.app
    cofyboxconfig = app.cofyboxconfig
    cofyboxinfo = app.cofyboxinfo

    # Result must include request object
    result = {"request": request}
    result.update({"step_name": step_name})

    # Localisation
    result.update(languages[cofyboxconfig.user.lang])
    result.update({"locale": cofyboxconfig.user.lang})

    # Add variables to template
    result.update({"cofyboxconfig": cofyboxconfig, "cofyboxinfo": cofyboxinfo})

    if step_name is not None:
        if step_name.lower() == "timezone":
            result.update({"timezone_options_by_group": generate_timezones_by_group()})

        if step_name.lower() == "language":
            result.update({"languages": generate_languages()})

        if step_name.lower() == "address":
            result.update(
                {
                    "geocode_earth_api_key": GEOCODE_EARTH_API_KEY,
                    "use_geocode_earth_api_directly": USE_GEOCODE_EARTH_API_DIRECTLY,
                }
            )

    return templates.TemplateResponse("wizard.html", result)


app = FastAPI()
app.mount(
    "/cofybox-config-ui/static",
    StaticFiles(directory=Path(__file__).parent.resolve() / "static"),
    name="static",
)


@app.on_event("startup")
async def startup_event():
    """
    This is run on startup. Stateful information which can change at runtime should
    be initialised here (instead of in the FastAPI app factory).
    """

    # Initialise the DBUS event loop and connection
    # This isnt used just attached to app to prevent cleanup.
    # (this is a workaround which may not be required in future versions of python-dbus and python-networkmanager)
    from dbus.mainloop.glib import DBusGMainLoop

    app.__dbus_loop = DBusGMainLoop(set_as_default=True)

    app.cofyboxconfig = None
    app.settings = CofyConfigSettings()
    app.cofyboxinfo = generate_cofyboxinfo(app.settings)

    # Setup MQTT client
    client = MQTTClient(app.settings.balena_device_name_at_init, "cofybox-config")
    client.on_connect = on_connect
    client.on_message = on_message
    client.on_disconnect = on_disconnect
    client.on_subscribe = on_subscribe

    app.mqtt_client = client
    app.mqtt_client.app = app

    settings = app.settings
    client = app.mqtt_client

    # Initialise the MQTT connection. If this doesnt work we want things to fail!
    client.set_auth_credentials(settings.balena_device_name_at_init, None)
    await client.connect("mosquitto")

    # If a config file exists try to read it,
    # if file does not validate then overwrite it
    # if file does not exist then create it
    # TODO: merge this logic into the sync_to_file_method
    try:
        logger.info(f"Trying to open config file: {settings.cofybox_config_abs_path}")
        app.cofyboxconfig = CofyBoxConfig.parse_file(settings.cofybox_config_abs_path)
    except (ValidationError, FileNotFoundError) as exc:
        if exc is ValidationError:
            logger.info("Config file invalid! Deleting!")
        elif exc is FileNotFoundError:
            logger.info("Config file doesn't exist, creating!")

        with open(settings.cofybox_config_abs_path, "w") as text_file:
            app.cofyboxconfig = create_cofyboxconfig()
            text_file.write(app.cofyboxconfig.json())

    # Generate cofybox state info
    app.cofyboxinfo = generate_cofyboxinfo(app.settings)

    cofyboxinfo = app.cofyboxinfo
    cofyboxconfig = app.cofyboxconfig

    # Try to communicate with cofy cloud DPS and data API's.
    # If this is first time should get 400 but application should continue but with status change.
    update_cofycloud_status(cofyboxinfo, cofyboxconfig, settings)

    # Setup the cofy-box device in HA using MQTT Discovery. See: https://www.home-assistant.io/docs/mqtt/discovery/
    # We want to do this before the first status message below
    uuid_short = str(cofyboxinfo.uuid)[:4]
    cofybox_hostname = f"cofybox-{uuid_short}"
    discovery_map = [
        ("uuid", "CoFyBox UUID", "{{value_json.uuid}}"),
        ("friendlyname", "CoFyBox Friendly Name", "{{value_json.friendlyname}}"),
        ("status", "CoFyCloud status", "{{value_json.cofycloud.status}}"),
        (
            "url",
            "CoFyCloud Device Dashboard URL",
            "{{value_json.cofycloud.cloud_space_url}}",
        ),
    ]
    device = HADiscoveryPayloadDevice(
        identifiers=[uuid_short],
        name="CoFyBox",
        model="v1",
        manufacturer="REScoopVPP",
    )
    state_topic = f"cofybox/{uuid_short}"

    for name, friendly_name, value_template in discovery_map:
        client.publish(
            f"homeassistant/sensor/cofybox-{uuid_short}-{name}/config",
            HADiscoveryPayload(
                name=friendly_name,
                state_topic=state_topic,
                value_template=value_template,
                device=device,
                unique_id=f"cofybox-{uuid_short}-{name}",
            ).json(exclude_unset=True),
            qos=1,
            retain=True,
        )

    # Set up HA Discovery for control enabled sensor separately from the above
    # We need a different state topic for it to update properly
    client.publish(
         f"homeassistant/sensor/cofybox-{uuid_short}-controlenabled-control/config",
         HADiscoveryPayload(
            name="CoFyBox Control Enabled",
            state_topic=f"cofybox-control/{uuid_short}",
            value_template="{{value_json.enabled}}",
            device=device,
            unique_id=f"cofybox-{uuid_short}-{name}-control"
         ).json(exclude_unset=True),
         qos=1,
         retain=True
    )

    # Pub a status and config update to MQTT bus
    client.publish(f"{state_topic}/config", cofyboxconfig.json(), qos=1)
    client.publish(state_topic, cofyboxinfo.json(), qos=1)

    # Try to setup the wifi hotspot if enabled
    if settings.cofybox_config_wifi_hotspot_enabled:
        set_wifi_hotspot_ap(
            settings.cofybox_config_wifi_hotspot_ssid_base + "-" + uuid_short,
            cofyboxinfo.friendlyname,
        )


from rest_api import router as rest_api_router

app.include_router(rest_api_router)


@app.on_event("shutdown")
async def shutdown_event():
    await app.mqtt_client.disconnect()


@app.get("/", response_class=HTMLResponse)
async def home(request: Request):

    return render_home(request)


@app.get("/wizard", response_class=HTMLResponse)
async def wizard(request: Request):

    return render_wizard(request)


@app.get("/wizard/{step_name}", response_class=HTMLResponse)
async def wizard(request: Request, step_name: str):

    return render_wizard(request, step_name)


@app.get("/v1/autocomplete")  # , response_class=JSONResponse
async def location_autocomplete(request: Request, text: str):
    r = httpx.get(
        f"https://api.geocode.earth/v1/autocomplete?api_key={GEOCODE_EARTH_API_KEY}&text={text}"
    )
    return r.json()


@app.post("/", response_class=HTMLResponse)
async def home_form_input(
    request: Request, form_data: CofyBoxConfigForm = Depends(CofyBoxConfigForm.as_form)
):
    app = request.app
    cofyboxconfig = app.cofyboxconfig
    cofyboxinfo = app.cofyboxinfo
    settings = app.settings

    # Update the global cofyboxconfig object with form/API input and sync this to file
    logger.info(f"Received config update: {str(form_data)}")
    cofyboxconfig.update_from_form_input(form_data)
    cofyboxconfig.sync_to_file(settings.cofybox_config_abs_path)

    logger.info(f"cofyboxconfig is now: {cofyboxconfig}")

    # Publish config changes to MQTT (async)
    client = app.mqtt_client
    client.publish(
        f"cofybox/{str(cofyboxinfo.uuid)[:4]}/config", cofyboxconfig.json(), qos=1
    )
    client.publish(f"cofybox/{str(cofyboxinfo.uuid)[:4]}", cofyboxinfo.json(), qos=1)

    return render_home(request)


@app.on_event("startup")
@repeat_every(seconds=10, logger=logger)
async def update_cofycloud_status_task():
    logger.info("Updating CofyCloud status...")
    update_cofycloud_status(app.cofyboxinfo, app.cofyboxconfig, app.settings)
    app.mqtt_client.publish(
        f"cofybox/{str(app.cofyboxinfo.uuid)[:4]}", app.cofyboxinfo.json(), qos=1
    )

@app.on_event("startup")
@repeat_every(seconds=300, wait_first=True, logger=logger)
async def cofybox_config_sync():
    logger.info("Attempting cofycloud config sync...")
    cofycloud_config_sync(app)
    
    cofyboxinfo = app.cofyboxinfo
    cofyboxconfig = app.cofyboxconfig

    uuid_short = str(cofyboxinfo.uuid)[:4]

    app.mqtt_client.publish(
        f"cofybox-control/{uuid_short}",
        cofyboxconfig.control.json(),
        qos=1
    )