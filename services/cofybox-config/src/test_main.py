import pytest
from fastapi.testclient import TestClient

import main
from cofybox import generate_fake_cofyboxinfo, CofyConfigSettings

from test_helpers import ensure_env_vars, cofybox_uuid, cofybox_fake_name

# Fixture to run all tests in src dir
@pytest.fixture(autouse=True)
def change_test_dir(request, monkeypatch):
    monkeypatch.chdir(request.fspath.dirname)


class TestApp:
    def test_basic_app_start_and_response(self, mocker, ensure_env_vars):

        mocker.patch("main.MQTTClient.on_connect")
        mocker.patch("main.MQTTClient.connect")
        mocker.patch("main.MQTTClient.publish")
        mocker.patch("main.set_wifi_hotspot_ap")
        mock_generate_cofyboxinfo = mocker.patch("main.generate_cofyboxinfo")
        mock_generate_cofyboxinfo.return_value = generate_fake_cofyboxinfo(
            CofyConfigSettings()
        )
        with TestClient(main.app) as client:  # with closure to trigger startup event
            response = client.get("/")

            assert response.status_code == 200

    def test_startup_and_first_get_mqtt_messages(self, mocker, ensure_env_vars):

        mocker.patch("main.MQTTClient.on_connect")
        mocker.patch("main.MQTTClient.connect")
        mqtt_pubs = mocker.patch("main.MQTTClient.publish")
        mocker.patch("main.set_wifi_hotspot_ap")
        mock_generate_cofyboxinfo = mocker.patch("main.generate_cofyboxinfo")
        mock_generate_cofyboxinfo.return_value = generate_fake_cofyboxinfo(
            CofyConfigSettings()
        )
        with TestClient(main.app) as client:  # with closure to trigger startup event

            assert len(mqtt_pubs.mock_calls) == 8

    def test_initial_valid_wifi_config(self, mocker, ensure_env_vars):

        mocker.patch("main.MQTTClient.on_connect")
        mocker.patch("main.MQTTClient.connect")
        mocker.patch("main.MQTTClient.publish")
        wifi_call = mocker.patch("main.set_wifi_hotspot_ap")
        mock_generate_cofyboxinfo = mocker.patch("main.generate_cofyboxinfo")
        mock_generate_cofyboxinfo.return_value = generate_fake_cofyboxinfo(
            CofyConfigSettings()
        )
        with TestClient(main.app) as client:  # with closure to trigger startup event
            wifi_call.assert_called_with(
                f"cofybox-{str(cofybox_uuid)[:4]}", cofybox_fake_name
            )
