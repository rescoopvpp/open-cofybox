import inspect
import logging
from typing import Type

from fastapi import Form
from pydantic import BaseModel
from pydantic.fields import ModelField

# Decorator for generating form inputs from pydantic model https://newbedev.com/fastapi-form-data-with-pydantic-model

logger = logging.getLogger("cofybox-config")

def as_form(cls: Type[BaseModel]):
    new_parameters = []

    for field_name, model_field in cls.__fields__.items():
        model_field: ModelField  # type: ignore

        if not model_field.required:
            new_parameters.append(
                inspect.Parameter(
                    model_field.alias,
                    inspect.Parameter.POSITIONAL_ONLY,
                    default=Form(model_field.default),
                    annotation=model_field.outer_type_,
                )
            )
        else:
            new_parameters.append(
                inspect.Parameter(
                    model_field.alias,
                    inspect.Parameter.POSITIONAL_ONLY,
                    default=Form(...),
                    annotation=model_field.outer_type_,
                )
            )

    async def as_form_func(**data):
        return cls(**data)

    sig = inspect.signature(as_form_func)
    sig = sig.replace(parameters=new_parameters)
    as_form_func.__signature__ = sig  # type: ignore
    setattr(cls, "as_form", as_form_func)
    return cls

def actions_after_updating_config(app):
    cofyboxconfig, cofyboxinfo, client, settings = (
        app.cofyboxconfig,
        app.cofyboxinfo,
        app.mqtt_client,
        app.settings,
    )
    cofyboxconfig.sync_to_file(settings.cofybox_config_abs_path)

    logger.info(f"cofyboxconfig is now: {cofyboxconfig}")

    # Publish config changes to MQTT (async)
    client.publish(
        f"cofybox/{str(cofyboxinfo.uuid)[:4]}/config", cofyboxconfig.json(), qos=1
    )
    client.publish(f"cofybox/{str(cofyboxinfo.uuid)[:4]}", cofyboxinfo.json(), qos=1)
