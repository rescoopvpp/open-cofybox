import logging

import os

from fastapi import APIRouter, Depends, HTTPException
from fastapi import Request
import httpx
from utils import actions_after_updating_config


from cofybox import (
    CofyBoxConfigUserLocation,
    CofyBoxConfigUserTimezone,
    CofyBoxConfigUserLanguage,
    CofyBoxConfigSolarHasSolar,
    CofyBoxConfigSolarConfig,
)

GEOCODE_EARTH_API_KEY = os.environ.get("GEOCODE_EARTH_API_KEY", "")

router = APIRouter(
    prefix="/api",
)

logger = logging.getLogger("cofybox-config")

@router.post("/location")
async def update_location(request: Request, user: CofyBoxConfigUserLocation):
    request.app.cofyboxconfig.user.location_latitude = user.location_latitude
    request.app.cofyboxconfig.user.location_longitude = user.location_longitude

    actions_after_updating_config(request.app)


@router.get("/location")
async def get_location(request: Request):
    return {
        "location_latitude": request.app.cofyboxconfig.user.location_latitude,
        "location_longitude": request.app.cofyboxconfig.user.location_longitude,
    }


@router.get("/location-name")
async def get_location_name(request: Request):
    latitude = request.app.cofyboxconfig.user.location_latitude
    longitude = request.app.cofyboxconfig.user.location_longitude
    if latitude is None or longitude is None:
        return
    r = httpx.get(
        f"https://api.geocode.earth/v1/reverse?api_key={GEOCODE_EARTH_API_KEY}&point.lat={latitude}&point.lon={longitude}"
    )
    best_match = sorted(
        r.json()["features"], key=lambda x: -x["properties"]["confidence"]
    )[0]
    return best_match["properties"]["label"]


@router.post("/timezone")
async def update_timezone(request: Request, user: CofyBoxConfigUserTimezone):
    request.app.cofyboxconfig.user.timezone = user.timezone

    actions_after_updating_config(request.app)


@router.get("/timezone")
async def get_timezone(request: Request):
    current_timezone = request.app.cofyboxconfig.user.timezone
    # if current_timezone is None:
    #     user = request.app.cofyboxconfig.user
    #     if user.location_longitude is not None and user.location_latitude is not None:
    #         guessed_timezone = tz.tzNameAt(user.location_latitude, user.location_longitude)
    #         return {"timezone": guessed_timezone}
    return {"timezone": current_timezone}


@router.post("/language")
async def update_language(request: Request, user: CofyBoxConfigUserLanguage):
    request.app.cofyboxconfig.user.lang = user.lang

    actions_after_updating_config(request.app)


@router.get("/language")
async def get_language(request: Request):
    return {"language": request.app.cofyboxconfig.user.lang}


@router.post("/has-solar")
async def update_has_solar(request: Request, solar: CofyBoxConfigSolarHasSolar):
    request.app.cofyboxconfig.solar.has_solar = solar.has_solar

    actions_after_updating_config(request.app)


@router.get("/has-solar")
async def get_has_solar(request: Request):
    data: CofyBoxConfigSolarHasSolar = {
        "has_solar": request.app.cofyboxconfig.solar.has_solar
    }
    return data


@router.post("/solar-config")
async def update_has_solar(request: Request, solar: CofyBoxConfigSolarConfig):
    request.app.cofyboxconfig.solar.max_power_output = solar.max_power_output
    request.app.cofyboxconfig.solar.declination = solar.declination
    request.app.cofyboxconfig.solar.azimuth = solar.azimuth

    actions_after_updating_config(request.app)


@router.get("/solar-config")
async def get_has_solar(request: Request):
    data: CofyBoxConfigSolarConfig = {
        "max_power_output": request.app.cofyboxconfig.solar.max_power_output,
        "declination": request.app.cofyboxconfig.solar.declination,
        "azimuth": request.app.cofyboxconfig.solar.azimuth,
    }
    return data
