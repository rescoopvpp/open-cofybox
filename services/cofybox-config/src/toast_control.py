import os
from collections import OrderedDict
import json
from datetime import date, datetime, timezone, timedelta
import unittest
from unittest.mock import patch

from dateutil.parser import isoparse
from dateutil.tz import tzutc
from freezegun import freeze_time
import responses

# Environment variable needs to be set before dynamic2mqtt can be imported.
MOCK_API_ROOT = "https://api.sandbox.cofybox.io/api"
os.environ["COFYCLOUD_API_URL"] = MOCK_API_ROOT
import dynamic2mqtt  # noqa: E402
from control import Dynamic2MQTT  # noqa: E402


class GetRatesTestCase(unittest.TestCase):
    def setUp(self):
        dynamic2mqtt.config = {"agile_region": "N"}

        self.instance = Dynamic2MQTT("N")

        self.expected_url = (
            MOCK_API_ROOT
            + "/v1/datasets/tariff?id=Octopus_N&start=2021-07-15T16:00:00.000Z&end=2021-07-15T18:00:00.000Z"
        )

    @patch("dynamic2mqtt.requests.get")
    def test_calls_api(self, fake_requests_get):
        self.instance.get_rates("2021-07-15T16:00:00.000Z", "2021-07-15T18:00:00.000Z")

        fake_requests_get.assert_called_once_with(
            self.expected_url, headers={"content-type": "application/json"}
        )

    @responses.activate
    def test_formats_rates_correctly(self):
        response_payload = {
            "metric": "Tariff",
            "id": "Octopus_G",
            "timespan": "2021-07-15T16:00:00+00:00/2021-07-15T18:00:00+00:00",
            "data": [
                {"timestamp": "2021-07-15T19:00:00+01:00", "value": 22.7535},
                {"timestamp": "2021-07-15T18:30:00+01:00", "value": 34.4295},
                {"timestamp": "2021-07-15T18:00:00+01:00", "value": 34.65},
                {"timestamp": "2021-07-15T17:30:00+01:00", "value": 34.65},
                {"timestamp": "2021-07-15T17:00:00+01:00", "value": 31.563},
            ],
        }
        responses.add(
            responses.GET, self.expected_url, json=response_payload, status=200
        )

        rates = self.instance.get_rates(
            "2021-07-15T16:00:00.000Z", "2021-07-15T18:00:00.000Z"
        )
        expected_dict = {
            "2021-07-15T19:00:00+01:00": 22.7535,
            "2021-07-15T18:30:00+01:00": 34.4295,
            "2021-07-15T18:00:00+01:00": 34.65,
            "2021-07-15T17:30:00+01:00": 34.65,
            "2021-07-15T17:00:00+01:00": 31.563,
        }
        self.assertDictEqual(expected_dict, rates["date_rates"])


class MakeScheduleTestCase(unittest.TestCase):
    """
    Test the daily task of planning times for the following day.
    """

    def setUp(self):
        dynamic2mqtt.config = {"agile_region": "N"}

        self.instance = Dynamic2MQTT("N")

    @freeze_time("2012-01-14")
    def test_handles_no_devices(self):

        self.instance.make_schedule()

        self.assertIsNone(self.instance.schedule.scheduled_date)
        self.assertDictEqual(self.instance.schedule.device_schedule, {})

    @freeze_time("2012-01-14")
    @patch("dynamic2mqtt.json.dump")
    @patch("dynamic2mqtt.Dynamic2MQTT.get_rates")
    def test_sets_scheduled_date_on_first_run(self, mock_get_rates, _mock_json_dump):

        dynamic2mqtt.config["devices"] = {"shelly": {"required_hours": 3}}
        mock_get_rates.return_value = {"date_rates": dummy_rates}
        self.instance.make_schedule()

        self.assertEqual(self.instance.schedule.scheduled_date, date(2012, 1, 14))

    @freeze_time("2012-01-14")
    @patch("builtins.open", create=True)
    def test_does_nothing_if_up_to_date(self, _mock_open):

        self.instance.scheduled_date = date(2012, 1, 14)

        dummy_device_schedule = {"shelly": "blah"}
        self.instance.device_schedule = dummy_device_schedule
        self.instance.make_schedule()

        self.assertDictEqual(self.instance.device_schedule, dummy_device_schedule)

    @freeze_time("2021-01-14")
    @patch("dynamic2mqtt.Dynamic2MQTT.get_min_times")
    @patch("dynamic2mqtt.Dynamic2MQTT.get_rates")
    @patch("dynamic2mqtt.json.dump")
    def test_update_if_out_of_date(
        self, _mock_json_dump, mock_get_rates, mock_min_times
    ):
        min_times = [
            datetime(2021, 1, 14, 23, 0, tzinfo=tzutc()),
            datetime(2021, 1, 14, 23, 30, tzinfo=tzutc()),
            datetime(2021, 1, 15, 0, 0, tzinfo=tzutc()),
            datetime(2021, 1, 15, 2, 30, tzinfo=tzutc()),
            datetime(2021, 1, 15, 3, 30, tzinfo=tzutc()),
            datetime(2021, 1, 15, 4, 0, tzinfo=tzutc()),
            datetime(2021, 1, 15, 4, 30, tzinfo=tzutc()),
            datetime(2021, 1, 15, 13, 30, tzinfo=tzutc()),
            datetime(2021, 1, 15, 14, 0, tzinfo=tzutc()),
            datetime(2021, 1, 15, 14, 30, tzinfo=tzutc()),
        ]
        mock_min_times.return_value = {key.isoformat(): 4 for key in min_times}

        dynamic2mqtt.config["devices"] = {"shelly": {"hours_required": 5}}
        self.instance.schedule.scheduled_date = date(2021, 1, 13)
        previous_device_schedule = {"shelly": ["blah"]}
        self.instance.schedule.device_schedule = previous_device_schedule

        # Set up fake rates.
        mock_get_rates.return_value = {"date_rates": dummy_rates}

        self.instance.make_schedule()

        self.assertListEqual(
            self.instance.schedule.device_schedule["shelly"], min_times
        )


class ScheduleWithRequirementsTestCase(unittest.TestCase):
    """
    Test the daily task of planning times for the following day, when
    advanced requirements are specified by the configuration file.
    """

    def setUp(self):
        dynamic2mqtt.config = {
            "agile_region": "N",
            "devices": {
                "shelly": {
                    "hours_required": 5,
                    "requirements": [
                        {
                            "day_from": "today",
                            "day_to": "today",
                            "numHrs": 3,
                            "time_from": "19:00:00",
                            "time_to": "23:00:00",
                        },
                        {
                            "day_from": "tomorrow",
                            "day_to": "tomorrow",
                            "numHrs": 1,
                            "time_from": "10:00:00",
                            "time_to": "16:00:00",
                        },
                    ],
                }
            },
        }

        self.instance = Dynamic2MQTT("N")

    @freeze_time("2021-01-14")
    @patch("dynamic2mqtt.Dynamic2MQTT.get_min_times")
    @patch("dynamic2mqtt.Dynamic2MQTT.get_rates")
    @patch("dynamic2mqtt.json.dump")
    def test_update_if_out_of_date(
        self, _mock_json_dump, mock_get_rates, mock_min_times
    ):

        self.instance.schedule.scheduled_date = date(2021, 1, 13)
        previous_device_schedule = {"shelly": []}
        self.instance.schedule.device_schedule = previous_device_schedule

        # Set up fake rates.
        seven_today = datetime(2021, 1, 14, 19, 0, 0, tzinfo=timezone.utc)
        seven_tomorrow = seven_today + timedelta(days=1)
        rates_in_period = {
            key: value
            for (key, value) in dummy_rates.items()
            if seven_today <= isoparse(key) < seven_tomorrow
        }
        self.instance.get_rates.return_value = {"date_rates": rates_in_period}
        mock_min_times.return_value = {
            "2021-01-14T19:00:00Z": 4,
            "2021-01-14T20:00:00Z": 4,
        }

        self.instance.make_schedule()

        mock_get_rates.assert_any_call("2021-01-14T19:00:00Z", "2021-01-15T19:00:00Z")
        mock_min_times.assert_called_once_with(
            10,
            {
                "2021-01-15T18:30:00Z": 15.813,
                "2021-01-15T18:00:00Z": 17.4195,
                "2021-01-15T17:30:00Z": 30.849,
                "2021-01-15T17:00:00Z": 30.408,
                "2021-01-15T16:30:00Z": 30.366,
                "2021-01-15T16:00:00Z": 28.2135,
                "2021-01-15T15:30:00Z": 25.998,
                "2021-01-15T15:00:00Z": 24.8115,
                "2021-01-15T14:30:00Z": 11.7495,
                "2021-01-15T14:00:00Z": 11.802,
                "2021-01-15T13:30:00Z": 12.012,
                "2021-01-15T13:00:00Z": 12.348,
                "2021-01-15T12:30:00Z": 13.146,
                "2021-01-15T12:00:00Z": 14.1645,
                "2021-01-15T11:30:00Z": 16.716,
                "2021-01-15T11:00:00Z": 16.9365,
                "2021-01-15T10:30:00Z": 16.758,
                "2021-01-15T10:00:00Z": 16.317,
                "2021-01-15T09:30:00Z": 15.876,
                "2021-01-15T09:00:00Z": 15.6135,
                "2021-01-15T08:30:00Z": 16.359,
                "2021-01-15T08:00:00Z": 16.1385,
                "2021-01-15T07:30:00Z": 14.8155,
                "2021-01-15T07:00:00Z": 14.3325,
                "2021-01-15T06:30:00Z": 14.112,
                "2021-01-15T06:00:00Z": 13.629,
                "2021-01-15T05:30:00Z": 12.9255,
                "2021-01-15T05:00:00Z": 12.789,
                "2021-01-15T04:30:00Z": 11.907,
                "2021-01-15T04:00:00Z": 11.907,
                "2021-01-15T03:30:00Z": 12.012,
                "2021-01-15T03:00:00Z": 12.348,
                "2021-01-15T02:30:00Z": 12.012,
                "2021-01-15T02:00:00Z": 12.5475,
                "2021-01-15T01:30:00Z": 12.6315,
                "2021-01-15T01:00:00Z": 13.4925,
                "2021-01-15T00:30:00Z": 13.0725,
                "2021-01-15T00:00:00Z": 11.907,
                "2021-01-14T23:30:00Z": 11.445,
                "2021-01-14T23:00:00Z": 11.907,
                "2021-01-14T22:30:00Z": 12.327,
                "2021-01-14T22:00:00Z": 13.8915,
                "2021-01-14T21:30:00Z": 12.411,
                "2021-01-14T21:00:00Z": 14.994,
                "2021-01-14T20:30:00Z": 14.994,
                "2021-01-14T20:00:00Z": 16.5375,
                "2021-01-14T19:30:00Z": 17.178,
                "2021-01-14T19:00:00Z": 18.6585,
            },
            [
                {
                    "slots": 6,
                    "numHrs": 3,
                    "time_from": "2021-01-14T19:00:00Z",
                    "time_to": "2021-01-14T23:00:00Z",
                },
                {
                    "slots": 2,
                    "numHrs": 1,
                    "time_from": "2021-01-15T10:00:00Z",
                    "time_to": "2021-01-15T16:00:00Z",
                },
            ],
        )

        self.assertListEqual(
            self.instance.schedule.device_schedule["shelly"],
            [
                datetime(2021, 1, 14, 19, 0, tzinfo=tzutc()),
                datetime(2021, 1, 14, 20, 0, tzinfo=tzutc()),
            ],
        )


class PublishDeviceStateTestCase(unittest.TestCase):
    def setUp(self):
        dynamic2mqtt.config = {"agile_region": "N"}

        self.instance = Dynamic2MQTT("N")
        self.instance.schedule.device_schedule["shelly"] = [
            datetime(2021, 1, 14, 0, 0, tzinfo=tzutc()),
            datetime(2021, 1, 14, 13, 30, tzinfo=tzutc()),
            datetime(2021, 1, 14, 14, 0, tzinfo=tzutc()),
            datetime(2021, 1, 14, 23, 0, tzinfo=tzutc()),
            datetime(2021, 1, 14, 23, 30, tzinfo=tzutc()),
            datetime(2021, 1, 15, 0, 0, tzinfo=tzutc()),
            datetime(2021, 1, 15, 4, 0, tzinfo=tzutc()),
            datetime(2021, 1, 15, 4, 30, tzinfo=tzutc()),
            datetime(2021, 1, 15, 14, 0, tzinfo=tzutc()),
            datetime(2021, 1, 15, 14, 30, tzinfo=tzutc()),
        ]
        dynamic2mqtt.config["currency_unit"] = "pence"

    @freeze_time("2021-01-14T13:30:00Z")
    @patch("dynamic2mqtt.Dynamic2MQTT.publish")
    @patch("dynamic2mqtt.Dynamic2MQTT.get_new_rates")
    @patch("dynamic2mqtt.Dynamic2MQTT.get_current_rate")
    @patch("dynamic2mqtt.Dynamic2MQTT.get_next_rate")
    @patch("dynamic2mqtt.Dynamic2MQTT.get_previous_rate")
    def test_publishes_prices(
        self,
        mock_get_previous_rate,
        mock_get_next_rate,
        mock_get_current_rate,
        mock_get_rates,
        mock_publish,
    ):
        mock_get_rates.return_value = {"date_rates": dummy_rates}
        mock_get_current_rate.return_value = 16.5375
        mock_get_previous_rate.return_value = 13.000
        mock_get_next_rate.return_value = 14.000

        self.instance.publish_prices()

        mock_publish.assert_any_call(
            "data/sensor/dynamic_price/upcoming_prices",
            payload=json.dumps(dummy_rates),
            retain=True,
        )

        mock_publish.assert_any_call(
            "data/sensor/dynamic_price/current_price",
            payload=16.5375,
            retain=True,
        )

        mock_publish.assert_any_call(
            "data/sensor/dynamic_price/next_price",
            payload=14,
            retain=True,
        )
        mock_publish.assert_any_call(
            "data/sensor/dynamic_price/previous_price",
            payload=13,
            retain=True,
        )

    @freeze_time("2021-01-14T13:30:00Z")
    @patch("dynamic2mqtt.Dynamic2MQTT.publish")
    def test_publishes_device_states_if_currently_on(
        self,
        mock_publish,
    ):

        self.instance.publish_device_updates()

        shelly_call = list(
            filter(
                lambda call: call[0][0] == "set/dynamic_price/shelly/state",
                mock_publish.call_args_list,
            )
        )[0]

        self.assertEqual(shelly_call.kwargs["payload"], "ON")


if __name__ == "__main__":
    unittest.main()

dummy_rates = OrderedDict(
    [
        ("2021-01-15T21:30:00Z", 13.23),
        ("2021-01-15T21:00:00Z", 13.566),
        ("2021-01-15T20:30:00Z", 12.9885),
        ("2021-01-15T20:00:00Z", 14.7735),
        ("2021-01-15T19:30:00Z", 15.435),
        ("2021-01-15T19:00:00Z", 17.115),
        ("2021-01-15T18:30:00Z", 15.813),
        ("2021-01-15T18:00:00Z", 17.4195),
        ("2021-01-15T17:30:00Z", 30.849),
        ("2021-01-15T17:00:00Z", 30.408),
        ("2021-01-15T16:30:00Z", 30.366),
        ("2021-01-15T16:00:00Z", 28.2135),
        ("2021-01-15T15:30:00Z", 25.998),
        ("2021-01-15T15:00:00Z", 24.8115),
        ("2021-01-15T14:30:00Z", 11.7495),
        ("2021-01-15T14:00:00Z", 11.802),
        ("2021-01-15T13:30:00Z", 12.012),
        ("2021-01-15T13:00:00Z", 12.348),
        ("2021-01-15T12:30:00Z", 13.146),
        ("2021-01-15T12:00:00Z", 14.1645),
        ("2021-01-15T11:30:00Z", 16.716),
        ("2021-01-15T11:00:00Z", 16.9365),
        ("2021-01-15T10:30:00Z", 16.758),
        ("2021-01-15T10:00:00Z", 16.317),
        ("2021-01-15T09:30:00Z", 15.876),
        ("2021-01-15T09:00:00Z", 15.6135),
        ("2021-01-15T08:30:00Z", 16.359),
        ("2021-01-15T08:00:00Z", 16.1385),
        ("2021-01-15T07:30:00Z", 14.8155),
        ("2021-01-15T07:00:00Z", 14.3325),
        ("2021-01-15T06:30:00Z", 14.112),
        ("2021-01-15T06:00:00Z", 13.629),
        ("2021-01-15T05:30:00Z", 12.9255),
        ("2021-01-15T05:00:00Z", 12.789),
        ("2021-01-15T04:30:00Z", 11.907),
        ("2021-01-15T04:00:00Z", 11.907),
        ("2021-01-15T03:30:00Z", 12.012),
        ("2021-01-15T03:00:00Z", 12.348),
        ("2021-01-15T02:30:00Z", 12.012),
        ("2021-01-15T02:00:00Z", 12.5475),
        ("2021-01-15T01:30:00Z", 12.6315),
        ("2021-01-15T01:00:00Z", 13.4925),
        ("2021-01-15T00:30:00Z", 13.0725),
        ("2021-01-15T00:00:00Z", 11.907),
        ("2021-01-14T23:30:00Z", 11.445),
        ("2021-01-14T23:00:00Z", 11.907),
        ("2021-01-14T22:30:00Z", 12.327),
        ("2021-01-14T22:00:00Z", 13.8915),
        ("2021-01-14T21:30:00Z", 12.411),
        ("2021-01-14T21:00:00Z", 14.994),
        ("2021-01-14T20:30:00Z", 14.994),
        ("2021-01-14T20:00:00Z", 16.5375),
        ("2021-01-14T19:30:00Z", 17.178),
        ("2021-01-14T19:00:00Z", 18.6585),
        ("2021-01-14T18:30:00Z", 17.9445),
        ("2021-01-14T18:00:00Z", 19.047),
        ("2021-01-14T17:30:00Z", 32.949),
        ("2021-01-14T17:00:00Z", 32.949),
        ("2021-01-14T16:30:00Z", 32.172),
        ("2021-01-14T16:00:00Z", 30.1875),
        ("2021-01-14T15:30:00Z", 30.1875),
        ("2021-01-14T15:00:00Z", 26.523),
        ("2021-01-14T14:30:00Z", 12.411),
        ("2021-01-14T14:00:00Z", 11.907),
        ("2021-01-14T13:30:00Z", 11.025),
        ("2021-01-14T13:00:00Z", 12.348),
        ("2021-01-14T12:30:00Z", 12.789),
        ("2021-01-14T12:00:00Z", 15.435),
        ("2021-01-14T11:30:00Z", 16.5375),
        ("2021-01-14T11:00:00Z", 17.199),
        ("2021-01-14T10:30:00Z", 17.199),
        ("2021-01-14T10:00:00Z", 16.9785),
        ("2021-01-14T09:30:00Z", 16.5375),
        ("2021-01-14T09:00:00Z", 16.758),
        ("2021-01-14T08:30:00Z", 17.115),
        ("2021-01-14T08:00:00Z", 16.863),
        ("2021-01-14T07:30:00Z", 18.081),
        ("2021-01-14T07:00:00Z", 16.9365),
        ("2021-01-14T06:30:00Z", 18.6585),
        ("2021-01-14T06:00:00Z", 15.498),
        ("2021-01-14T05:30:00Z", 13.8915),
        ("2021-01-14T05:00:00Z", 14.994),
        ("2021-01-14T04:30:00Z", 14.3325),
        ("2021-01-14T04:00:00Z", 14.7315),
        ("2021-01-14T03:30:00Z", 13.776),
        ("2021-01-14T03:00:00Z", 12.789),
        ("2021-01-14T02:30:00Z", 12.81),
        ("2021-01-14T02:00:00Z", 12.0435),
        ("2021-01-14T01:30:00Z", 12.705),
        ("2021-01-14T01:00:00Z", 11.907),
        ("2021-01-14T00:30:00Z", 13.671),
        ("2021-01-14T00:00:00Z", 10.605),
    ]
)
