import logging
import ipaddress
from typing import Dict, Tuple, List, Optional
import uuid
from enum import Enum
import re
from zoneinfo import ZoneInfo
from zoneinfo import ZoneInfoNotFoundError
import datetime

from pydantic import BaseModel
from pydantic import BaseSettings
from pydantic import Field
from pydantic import validator
from pydantic import AnyHttpUrl

logger = logging.getLogger("cofybox-config")

valid_ap_regex = re.compile(
    r'^[^!#;+\]\/"\t][^+\]\/"\t]{0,30}[^ +\]\/"\t]$|^[^ !#;+\]\/"\t]$[ \t]+$'  # noqa: W605
)


class CofyBoxInfoNetworkManagerDeviceIPv4config(BaseModel):
    addresses: List[Tuple[ipaddress.IPv4Address, int, ipaddress.IPv4Address]] = None
    routes: List[Tuple[ipaddress.IPv4Address, int, ipaddress.IPv4Address, int]] = None
    nameserver: List[ipaddress.IPv4Address] = None


class CofyBoxInfoNetworkManagerDevice(BaseModel):
    ipv4config: CofyBoxInfoNetworkManagerDeviceIPv4config = None
    interface: str = None
    dev_type: str = None


class CofyBoxInfoNetworkManagerConnectionIPconfig(BaseModel):
    address_data: str = Field(None, alias="address-data")
    method: str = None

    class Config:
        allow_population_by_field_name = True


class CofyBoxInfoNetworkManagerConnectionTypeEnum(str, Enum):
    _802_3_ethernet = "802-3-ethernet"
    _802_11_wireless = "802-11-wireless"


class CofyBoxInfoNetworkManagerConnectionS(BaseModel):
    type: CofyBoxInfoNetworkManagerConnectionTypeEnum
    interface_name: str = Field(None, alias="interface-name")
    id: str = None
    autoconnect: bool = None
    autoconnect_retries: int = Field(None, alias="autoconnect-retries")
    mdns: int = None

    class Config:
        allow_population_by_field_name = True


class CofyBoxInfoNetworkManagerWireless(BaseModel):
    ssid: str
    band: str = None
    mode: str = None
    powersave: int = None


class CofyBoxInfoNetworkManagerWirelessSecurity(BaseModel):
    psk: str
    key_mgmt: str = Field(None, alias="key-mgmt")
    proto: List[str] = None

    class Config:
        allow_population_by_field_name = True


class CofyBoxInfoNetworkManagerConnection(BaseModel):
    wireless: CofyBoxInfoNetworkManagerWireless = Field(None, alias="802-11-wireless")
    wireless_security: CofyBoxInfoNetworkManagerWirelessSecurity = Field(
        None, alias="802-11-wireless-security"
    )
    connection: CofyBoxInfoNetworkManagerConnectionS
    ipv4: CofyBoxInfoNetworkManagerConnectionIPconfig
    ipv6: CofyBoxInfoNetworkManagerConnectionIPconfig
    devices: List[CofyBoxInfoNetworkManagerDevice] = None

    class Config:
        allow_population_by_field_name = True


class CofyBoxInfoNetworkManager(BaseModel):
    active_connections: List[CofyBoxInfoNetworkManagerConnection] = None


def get_network_info() -> Tuple[ipaddress.IPv4Address, ipaddress.IPv4Address, str]:
    """
    Get the network connection/device information from NetworkManager via DBUS.
    """
    import NetworkManager

    c = (
        NetworkManager.const
    )  # this is a library function used to generate strings for constants

    active_connections = []
    for conn in NetworkManager.NetworkManager.ActiveConnections:

        awireless = None
        wireless_security = None
        try:
            settings = conn.Connection.GetSettings()
        except KeyError:
            continue

        conn_type = settings["connection"]["type"]

        if conn_type == "802-3-ethernet" or conn_type == "802-11-wireless":

            # Build connection section
            try:
                iname = settings["connection"]["interface-name"]
            except KeyError:
                iname = None

            connection_section = CofyBoxInfoNetworkManagerConnectionS(
                type=conn_type,
                interface_name=iname,
            )

            # Build ipv4 section
            ipv4 = CofyBoxInfoNetworkManagerConnectionIPconfig(
                address_data=str(settings["ipv4"]["address-data"]),
            )

            # Build ipv6 section
            ipv6 = CofyBoxInfoNetworkManagerConnectionIPconfig(
                address_data=str(settings["ipv6"]["address-data"]),
            )

            if (
                conn_type
                == CofyBoxInfoNetworkManagerConnectionTypeEnum._802_11_wireless
            ):
                try:
                    awireless = CofyBoxInfoNetworkManagerWireless(
                        ssid=str(settings["802-11-wireless"]["ssid"]),
                    )

                    wireless_security = CofyBoxInfoNetworkManagerWirelessSecurity(
                        psk=str(settings["802-11-wireless"]["ssid"]),
                    )
                except KeyError:
                    pass
            # Build device list associated with connection (usually only 1 device per connection)
            devices = [
                CofyBoxInfoNetworkManagerDevice(
                    ipv4config=CofyBoxInfoNetworkManagerDeviceIPv4config(
                        addresses=dev.Ip4Config.Addresses,
                        routes=dev.Ip4Config.Routes,
                        nameserver=dev.Ip4Config.Nameservers,
                    ),
                    interface=str(dev.Interface),
                    dev_type=str(c("device_type", dev.DeviceType)),
                )
                for dev in conn.Devices
            ]

            # Create connection object
            connection = CofyBoxInfoNetworkManagerConnection(
                wireless=awireless,
                wireless_security=wireless_security,
                connection=connection_section,
                ipv4=ipv4,
                ipv6=ipv6,
                devices=devices,
            )
            if len(devices) > 0:
                active_connections.append(connection)

    nminfo = CofyBoxInfoNetworkManager(active_connections=active_connections)

    logger.info(f"RAW network info: {nminfo}")

    # Now extract the specific things required for CoFyConfig (atm)
    # In future could just return the whole nminfo object etc.
    wifi_client_ip = None
    wifi_client_ap = None
    wired_ip = None

    for conn in nminfo.active_connections:
        match conn.connection.type:
            case CofyBoxInfoNetworkManagerConnectionTypeEnum._802_11_wireless:
                if conn.wireless is not None:
                    wifi_client_ap = conn.wireless.ssid
                if conn.devices is not None:
                    wifi_client_ip = conn.devices[0].ipv4config.addresses[0][0]
            case CofyBoxInfoNetworkManagerConnectionTypeEnum._802_3_ethernet:
                if conn.devices is not None:
                    wired_ip = conn.devices[0].ipv4config.addresses[0][0]

    return wired_ip, wifi_client_ip, wifi_client_ap


def set_wifi_hotspot_ap(wifi_ap_ssid: str, wifi_ap_psk: str):
    """
    This function configures the wifi network on the host by sending a message to the host DBUS.

    The most useful resource for inferring settings is probably nm-settings(5)
    e.g. http://manpages.ubuntu.com/manpages/trusty/man5/nm-settings.5.html

    python-networkmanager otherwise handles the conversion to DBUS types.

    You don't want to run this locally on your laptop as it will likely screw up your network config (if you use Network Manager)!
    """
    import NetworkManager

    uuid_ = uuid.uuid4()  # we need this to uniquely identify previous connections

    s_con = CofyBoxInfoNetworkManagerConnectionS(
        id="cofybox-wifi-hotspot",
        type="802-11-wireless",
        autoconnect=True,
        autoconnect_retries=0,
        mdns=1,
        uuid=uuid_,
    )

    s_wifi = CofyBoxInfoNetworkManagerWireless(
        band="bg",
        mode="ap",
        ssid=wifi_ap_ssid,
        powersave=2,
    )

    s_wsec = CofyBoxInfoNetworkManagerWirelessSecurity(
        key_mgmt="wpa-psk",
        proto=["rsn"],
        psk=wifi_ap_psk,
    )

    s_ip4 = CofyBoxInfoNetworkManagerConnectionIPconfig(method="shared")

    s_ip6 = CofyBoxInfoNetworkManagerConnectionIPconfig(method="ignore")

    con = CofyBoxInfoNetworkManagerConnection(
        connection=s_con,
        wireless=s_wifi,
        wireless_security=s_wsec,
        ipv4=s_ip4,
        ipv6=s_ip6,
    )

    # Deactivate and remove any old wifi hotspot:
    current_conns = NetworkManager.Settings.ListConnections()

    for conn in filter(
        lambda c: c.GetSettings()["connection"]["id"] == "cofybox-wifi-hotspot",
        NetworkManager.Settings.ListConnections(),
    ):
        conn.Delete()

    logger.info(
        f"Trying to add wifi hotspot connection... {con.dict(by_alias=True,exclude_unset=True)}"
    )
    wifi_ap_conn = NetworkManager.Settings.AddConnection(
        con.dict(by_alias=True, exclude_unset=True)
    )

    # Now we need to find the wifi hardware device
    devices = NetworkManager.NetworkManager.GetDevices()

    for dev in devices:
        if (
            dev.DeviceType == NetworkManager.NM_DEVICE_TYPE_WIFI
        ):  # and dev.State == NetworkManager.NM_DEVICE_STATE_DISCONNECTED:
            logger.info(f"Trying to add wifi hotspot to device... {dev}")
            NetworkManager.NetworkManager.ActivateConnection(wifi_ap_conn, dev, "/")

            # NetworkManager.NetworkManager.ActivateConnection(conn, dev, "/")
            break
    else:
        logger.error("Could not find a wifi device to activate hotspot on!")
