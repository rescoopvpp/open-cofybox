#!/bin/bash
if test "$COFYBOX_ENABLE_SERVICE" = TRUE ; then
    mkdir -p /config/.storage/user
    mkdir -p /config/.storage/packages
    touch /config/.storage/packages/secrets.yaml
    touch /config/automations.yaml
    touch /config/.storage/automations.yaml
    touch /config/.storage/user/automations.yaml
    touch /config/.storage/user/custom-automations.yaml
    touch /config/.storage/user/sensor.yaml
    touch /config/.storage/user/mqtt.yaml
    touch /config/.storage/user/emoncms.yaml
    touch /config/.storage/user/template.yaml
    # We use a docker volume for blueprints to keep it persistent
    # This means we have to copy across the automations to keep them up to date
    mkdir -p /config/blueprints/automation/cofycloud/
    rm -rf /config/blueprints/automation/cofycloud/*
    cp -r /config/blueprints-cofycloud/* /config/blueprints/automation/cofycloud
    python3 -m homeassistant --config /config --debug
else
    echo "HomeAssistant Block Disabled by environment variable"
    break
fi
