"""The cofybox_packages integration."""
import logging

from homeassistant.config_entries import ConfigEntry
from homeassistant.const import Platform
from homeassistant.core import HomeAssistant

from .const import DOMAIN

_LOGGER = logging.getLogger(__name__)

# async def async_setup(hass: HomeAssistant, config: ConfigEntry) -> bool:
#     _LOGGER.info("hi!")

#     # hass.data[const.DOMAIN] = {}
#     # # hass.data[const.DOMAIN][const.DATA_CLIENT] = {}

#     # if const.DOMAIN not in config:
#     #     return True

#     # # Store config for use during entry setup:
#     # hass.data[const.DOMAIN][const.DATA_CONFIG] = config
#     return True

async def async_setup_entry(hass: HomeAssistant, entry: ConfigEntry) -> bool:
    """Set up cofybox_packages from a config entry."""
    _LOGGER.info("hello")
    # TODO Optionally store an object for your platforms to access
    # hass.data[DOMAIN][entry.entry_id] = ...

    # TODO Optionally validate config entry options before setting up platform

    # await hass.config_entries.async_forward_entry_setups(entry, (Platform.SENSOR,))


    return True



# async def async_unload_entry(hass: HomeAssistant, entry: ConfigEntry) -> bool:
#     """Unload a config entry."""
#     if unload_ok := await hass.config_entries.async_unload_platforms(
#         entry, (Platform.SENSOR,)
#     ):
#         hass.data[DOMAIN].pop(entry.entry_id)

#     return unload_ok
