import os
import shutil
import yaml


def move_package(device: str):
    dest_path = "/config/.storage/packages/"
    src_paths = {
        "emoncms": ("/config/library/emoncms.yaml",),
        "victron": (
            "/config/library/battery_victron_ess.yaml",
            "/config/library/generic_battery_dispatch.yaml",
            "/config/library/generic_flexibility.yaml",
        ),
        "viessmann": (
            "/config/library/heatpump_viessmann.yaml",
            "/config/library/generic_heatpump_dispatch.yaml",
            "/config/library/generic_flexibility.yaml",
        ),
        "nibe": (
            "/config/library/heatpump_nibe_f_series.yaml",
            "/config/library/generic_heatpump_dispatch.yaml",
            "/config/library/generic_flexibility.yaml",
        ),
        "sgready": (
            "/config/library/heatpump_sgready_cookie.yaml",
            "/config/library/generic_heatpump_dispatch.yaml",
            "/config/library/generic_flexibility.yaml",
        ),
        "sunnyboy_battery_man": (
            "/config/library/battery_sma_sunnyboy_storage_manual.yaml",
            "/config/library/generic_battery_dispatch.yaml",
            "/config/library/generic_flexibility.yaml",
        ),
        "sunnyboy_battery_auto": (
            "/config/library/battery_sma_sunnyboy_storage.yaml",
            "/config/library/generic_battery_dispatch.yaml",
            "/config/library/generic_flexibility.yaml",
        ),
        "sunnyboy_pv_man": (
            "/config/library/pv_sma_sunnyboy_manual.yaml",
            "/config/library/generic_pv_dispatch.yaml",
            "/config/library/generic_flexibility.yaml",
        ),
        "sunnyboy_pv_auto": (
            "/config/library/pv_sma_sunnyboy.yaml",
            "/config/library/generic_pv_dispatch.yaml",
            "/config/library/generic_flexibility.yaml",
        ),
        "shelly_utility": [
            "/config/library/shelly.yaml"
        ]
    }
    if len(src_paths[device]) > 1:
        for path in src_paths[device]:
            shutil.copy(path, dest_path)
    else:
        shutil.copy(src_paths[device][0], dest_path)


def append_secret(user_input: dict):
    if "add_another" in user_input:
        secrets = user_input.copy()
        del secrets["add_another"]
    with open("/config/.storage/packages/secrets.yaml", 'a') as file:
        document = yaml.dump(secrets, file)

def find_and_replace_shelly(filename: str, id: str):
    with open(filename, 'r') as file:
        filedata = file.read()

    filedata = filedata.replace("{SHELLY_ID}", id)

    with open(filename, 'w') as file:
        file.write(filedata)