"""Config flow for cofybox_packages integration."""
import logging
import re

from typing import Any, Optional, Dict

import voluptuous as vol

from homeassistant import config_entries
from homeassistant.helpers import selector
import homeassistant.helpers.config_validation as cv

from . import const
from .services import move_package, append_secret, find_and_replace_shelly

_LOGGER = logging.getLogger(__name__)

CONFIG_SCHEMA = vol.Schema(
    {
        vol.Required("name"): selector.TextSelector(),
    }
)

DEVICES = [
    const.SUNNYBOY_PV,
    const.SUNNYBOY_BATTERY,
    const.EMONCMS,
    const.SONNEN,
    const.VIESSMANN,
    const.VICTRON,
    const.NIBE,
    const.SGREADY,
    const.SHELLY_UTILITY,
    const.SHELLY_RELAY
]

DEVICES_SCHEMA = vol.Schema({vol.Required("selected_device"): vol.In(DEVICES)})

SECRETS_SCHEMA_SUNNYBOY_BATTERY = vol.Schema(
    {vol.Required("sbs_host"): cv.string, vol.Optional("add_another"): cv.boolean}
)

SECRETS_SCHEMA_SUNNYBOY_PV = vol.Schema(
    {vol.Required("sb_host"): cv.string, vol.Optional("add_another"): cv.boolean}
)

SECRETS_SCHEMA_EMON = vol.Schema(
    {
        vol.Required("emoncms_api"): cv.string,
        vol.Required("emoncms_url"): cv.string,
        vol.Optional("add_another"): cv.boolean,
    }
)

SECRETS_SCHEMA_VICTRON = vol.Schema(
    {vol.Required("victron_host"): cv.string, vol.Optional("add_another"): cv.boolean}
)

SB_AUTO_REQUIRED_SCHEMA = vol.Schema(
    {vol.Required("login_known"): vol.In(("Yes", "No"))}
)


class ConfigFlowHandler(config_entries.ConfigFlow, domain=const.DOMAIN):
    """Handle a config or options flow for cofybox_packages."""

    data: Optional[Dict[str, Any]]

    async def async_step_get_shelly_id(self):
        shelly_id = None

        def _get_shelly_id():
            pattern = r'^sensor\.shelly_em_([a-zA-Z0-9]+)_'
            for id in self.hass.states.async_entity_ids():
                match = re.match(pattern, id)
                if match:
                        shelly_id = match.group(1)
                        self.data["shelly_id"] = shelly_id
                        _LOGGER.info(f"Shelly ID found: {shelly_id}")
                        break
            else:
                _LOGGER.info("Never found a Shelly ID")

        return await self.hass.async_add_executor_job(_get_shelly_id)

    async def async_step_user(self, user_input: Optional[Dict[str, Any]] = None):
        """First step in in the config flow - get the devices."""
        errors: Optional[Dict[str, Any]] = {}
        if user_input is not None:
            if not errors:
                self.data = user_input
                if user_input.get("selected_device"):
                    match user_input["selected_device"]:
                        case const.SHELLY_UTILITY:
                            await self.async_step_get_shelly_id()
                            find_and_replace_shelly("/config/library/shelly.yaml", self.data["shelly_id"])
                            move_package("shelly_utility")
                            return self.async_create_entry(
                                title="shelly_utility", data={"config_worked": "yes"}
                            )
                        case const.SHELLY_RELAY:
                            await self.async_step_get_shelly_id()
                            append_secret({"shelly_id": f"{self.data['shelly_id']}"})
                            return self.async_create_entry(
                                title="shelly_utility", data={"config_worked": "yes"}
                            )
                        case const.VICTRON:
                            return await self.async_step_secrets_victron()
                        case const.EMONCMS:
                            return await self.async_step_secrets_emon()
                        case const.SUNNYBOY_BATTERY:
                            return await self.async_step_sb_auto_required()
                        case const.SUNNYBOY_PV:
                            return await self.async_step_sb_auto_required()
                        case const.VIESSMANN:
                            move_package(const.VIESSMANN)
                            return self.async_create_entry(
                                title="viessmann", data={"config_worked": "yes"}
                            )
                        case const.NIBE:
                            move_package(const.NIBE)
                            return self.async_create_entry(
                                title="nibe", data={"config_worked": "yes"}
                            )
                        case const.SGREADY:
                            move_package(const.SGREADY)
                            return self.async_create_entry(
                                title="sgready", data={"config_worked": "yes"}
                            )

        return self.async_show_form(
            step_id="user", data_schema=DEVICES_SCHEMA, errors=errors
        )

    async def async_step_secrets_victron(
        self, user_input: Optional[Dict[str, Any]] = None
    ):
        """Second step in config flow to add secrets for the victron."""
        errors: Dict[str, str] = {}
        if user_input is not None:
            if not errors:
                append_secret(user_input)
                move_package(const.VICTRON)
                if user_input.get("add_another", False):
                    return await self.async_step_user()
                else:
                    return self.async_create_entry(
                        title="secrets_victron", data=self.data
                    )

        return self.async_show_form(
            step_id="secrets_victron", data_schema=SECRETS_SCHEMA_VICTRON, errors=errors
        )

    async def async_step_secrets_emon(
        self, user_input: Optional[Dict[str, Any]] = None
    ):
        """Second step in config flow to add secrets for the emon."""
        errors: Dict[str, str] = {}
        if user_input is not None:
            if not errors:
                append_secret(user_input)
                move_package(const.EMONCMS)
                if user_input.get("add_another", False):
                    return await self.async_step_user()
                else:
                    return self.async_create_entry(title="emoncms", data=self.data)

        return self.async_show_form(
            step_id="secrets_emon", data_schema=SECRETS_SCHEMA_EMON, errors=errors
        )

    async def async_step_sb_auto_required(
        self, user_input: Optional[Dict[str, Any]] = None
    ):
        """
        Find out whether we need to use the auto/manual version of the SB package.
        Login known means we use the automatic variation. For the auto PV, we don't need secret.
        """
        errors: Dict[str, str] = {}
        if user_input is not None:
            if not errors:
                self.data.update(user_input)
                if self.data["selected_device"] == const.SUNNYBOY_BATTERY:
                    return await self.async_step_secrets_sunnyboy_battery()
                if self.data["selected_device"] == const.SUNNYBOY_PV:
                    return await self.async_step_secrets_sunnyboy_pv()

        return self.async_show_form(
            step_id="sb_auto_required",
            data_schema=SB_AUTO_REQUIRED_SCHEMA,
            errors=errors,
        )

    async def async_step_secrets_sunnyboy_battery(
        self, user_input: Optional[Dict[str, Any]] = None
    ):
        """Second step in config flow to add secrets for the sunnyboy battery."""
        errors: Dict[str, str] = {}
        if user_input is not None:
            if not errors:
                if self.data["login_known"] == "Yes":
                    move_package("sunnyboy_battery_auto")
                    append_secret(user_input)
                elif self.data["login_known"] == "No":
                    move_package("sunnyboy_battery_man")
                    append_secret(user_input)
                if user_input.get("add_another", False):
                    return await self.async_step_user()
                else:
                    return self.async_create_entry(
                        title="sunnyboy_battery", data=self.data
                    )

        return self.async_show_form(
            step_id="secrets_sunnyboy_battery",
            data_schema=SECRETS_SCHEMA_SUNNYBOY_BATTERY,
            errors=errors,
        )

    async def async_step_secrets_sunnyboy_pv(
        self, user_input: Optional[Dict[str, Any]] = None
    ):
        """Second step in config flow to add secrets for the sunnyboy PV."""
        errors: Dict[str, str] = {}
        if user_input is not None:
            if not errors:
                if self.data["login_known"] == "Yes":
                    move_package("sunnyboy_pv_auto")
                    append_secret(user_input)
                elif self.data["login_known"] == "No":
                    move_package("sunnyboy_pv_man")
                    append_secret(user_input)
                if user_input.get("add_another", False):
                    return await self.async_step_user()
                else:
                    return self.async_create_entry(title="sunnyboy_pv", data=self.data)

        return self.async_show_form(
            step_id="secrets_sunnyboy_pv",
            data_schema=SECRETS_SCHEMA_SUNNYBOY_PV,
            errors=errors,
        )
