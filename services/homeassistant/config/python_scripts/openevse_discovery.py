"""
* emonEVSE Discovery Script
** Intro
This script adds MQTT discovery support for the OpenEVSE. It is pending changes to the MQTT API due in a future version 
of the wifi controller.

It is based on something similar for Shelly wifi relays:
https://github.com/bieniu/ha-shellies-discovery

As well as the OpenEVSE HA configuration by 
https://github.com/TonyApuzzo/home-assistant-config/blob/master/packages/openevse.yaml


Arguments:
 - discovery_prefix:    - discovery prefix in HA, default 'homeassistant',
                          optional
 - id                   - OpenEVSE ESP32 hardware ID full

** Default configuration
# emonEVSE discovery trigger
- id: 'openevse_discovery'
  alias: 'OpenEVSE Discovery'
  trigger:
    - platform: mqtt
      topic: openevse/announce/#
  action:
    service: python_script.openevse_discovery
    data_template:
      id: '{{ trigger.payload_json.id }}'
      name: '{{ trigger.payload_json.name }}'
      http: '{{ trigger.payload_json.http }}'

** EVSE status updates look like
openevse-XXXX/amp 0
openevse-XXXX/wh 0
openevse-XXXX/temp1 260
openevse-XXXX/temp2 -2560
openevse-XXXX/temp3 -2560
openevse-XXXX/pilot 32
openevse-XXXX/state 1
openevse-XXXX/freeram 27352
openevse-XXXX/divertmode 1

** State codes
#define OPENEVSE_STATE_STARTING               0
#define OPENEVSE_STATE_NOT_CONNECTED          1
#define OPENEVSE_STATE_CONNECTED              2
#define OPENEVSE_STATE_CHARGING               3
#define OPENEVSE_STATE_VENT_REQUIRED          4
#define OPENEVSE_STATE_DIODE_CHECK_FAILED     5
#define OPENEVSE_STATE_GFI_FAULT              6
#define OPENEVSE_STATE_NO_EARTH_GROUND        7
#define OPENEVSE_STATE_STUCK_RELAY            8
#define OPENEVSE_STATE_GFI_SELF_TEST_FAILED   9
#define OPENEVSE_STATE_OVER_TEMPERATURE      10
#define OPENEVSE_STATE_SLEEPING             254
#define OPENEVSE_STATE_DISABLED             255

NOTE: HA defines two special  

"""

VERSION = "0.1.0"

ATTR_DEVELOP = "develop"

ATTR_ID = "id"
ATTR_MAC = "mac"
ATTR_FW_VER = "fw_ver"
ATTR_NAME = "name"
ATTR_HTTP = "http"

ATTR_DISCOVERY_PREFIX = "discovery_prefix"
ATTR_TEMP_UNIT = "temp_unit"
ATTR_QOS = "qos"
ATTR_TRIGGER = "trigger"

ATTR_TEMPLATE_TEMPERATURE = "{{ (value | float / 10) | round(1) }}"
ATTR_TEMPLATE_HUMIDITY = "{{ value | float | round(1) }}"
ATTR_TEMPLATE_LUX = "{{ value | float | round }}"
ATTR_TEMPLATE_POWER = "{{ value | float | round(1) }}"
ATTR_TEMPLATE_REACTIVE_POWER = "{{ value | float | round(1) }}"
ATTR_TEMPLATE_VOLTAGE = "{{ value | float | round(1) }}"
ATTR_TEMPLATE_CURRENT_PILOT = "{{ value | float | round(1) }}"
ATTR_TEMPLATE_CURRENT = "{{ (value | float /1000) | round(1) }}"
ATTR_TEMPLATE_ENERGY = "{{ (value | float / 60 / 1000) | round(2) }}"
ATTR_TEMPLATE_BATTERY = "{{ value | float | round }}"
#ATTR_TEMPLATE_STATE = "{{ value | int | round }}"
ATTR_TEMPLATE_STATE = "{% if value|int==0 -%}Ready{%- elif value|int==1 -%}Not Connected{%- elif value|int==2 -%}Connected{%- elif value|int==3 -%}Charging{%- elif value|int==4 -%}Vent Required{%- elif value|int==5 -%}Diode Check Failed{%- elif value|int==6 -%}GFI Fault{%- elif value|int==7 -%}No Earth Ground{%- elif value|int==8 -%}Stuck Relay{%- elif value|int==9 -%}GFI Self Test Failed{%- elif value|int==10 -%}Over Temperature{%- elif value|int==254 -%}Sleeping{%- elif value|int==255 -%}Disabled{%- else -%}Unknown status code{%- endif %}"
ATTR_TEMPLATE_OVERPOWER = "{{ value_json.overpower }}"
ATTR_TEMPLATE_DIVERT_MODE = "{% if value|int==1 -%}Normal{%- elif value|int==2 -%}Eco{%- else -%}Unknown divert mode code{%- endif %}"

ATTR_MANUFACTURER = "OpenEVSE"
ATTR_MODEL_OPENEVSE_1 = "OpenEVSE1"
ATTR_MODEL_EMONEVSE_1 = "openevse"
ATTR_OPENEVSE = "OpenEVSE"
ATTR_EMONEVSE = "emonEVSE"
ATTR_STATE = "state"
ATTR_TEMPERATURE = "temp1"
ATTR_HUMIDITY = "humidity"
ATTR_BATTERY = "battery"
ATTR_POWER = "power"
ATTR_REACTIVE_POWER = "reactive_power"
ATTR_VOLTAGE = "voltage"
ATTR_ENERGY = "wh"
ATTR_CURRENT = "amp"
ATTR_CURRENT_PILOT = "pilot"
ATTR_SWITCH = "switch"
ATTR_CHARGER = "charger"
ATTR_INPUT = "input"
ATTR_LONGPUSH = "longpush"
ATTR_OVERTEMPERATURE = "overtemperature"
ATTR_OVERPOWER = "overpower"
ATTR_HEAT = "heat"
ATTR_COVER = "cover"
ATTR_UNIT_W = "W"
ATTR_UNIT_KWH = "kWh"
ATTR_UNIT_WH = "Wh"
ATTR_UNIT_V = "V"
ATTR_UNIT_A = "A"
ATTR_UNIT_VAR = "VAR"
ATTR_UNIT_PERCENT = "%"
ATTR_UNIT_CELSIUS = "°C"
ATTR_UNIT_FARENHEIT = "°F"
ATTR_ON = "on"
ATTR_OFF = "off"
ATTR_TRUE_FALSE_PAYLOAD = {ATTR_ON: "true", ATTR_OFF: "false"}
ATTR_1_0_PAYLOAD = {ATTR_ON: "1", ATTR_OFF: "0"}
ATTR_EXPIRE_AFTER = "7200"


def mqtt_publish(topic, payload, retain, qos):
    KEY_TOPIC = "topic"
    KEY_PAYLOAD = "payload"
    KEY_RETAIN = "retain"
    KEY_QOS = "qos"
    service_data = {
        KEY_TOPIC: topic,
        KEY_PAYLOAD: payload,
        KEY_RETAIN: retain,
        KEY_QOS: qos,
    }
    logger.debug("Sending to MQTT broker: %s %s", topic, payload)
    hass.services.call("mqtt", "publish", service_data, False)


develop = False
retain = False
qos = 0
roller_mode = False

logger.info(data)

attr_id = data.get(ATTR_ID)
name = data.get(ATTR_NAME)
http = data.get(ATTR_HTTP)

# state = data.get(ATTR_STATE)
# name = data.get(ATTR_NAME)
# http = data.get(ATTR_HTTP)
# mac = ''
# fw_ver=''

RELAY_GROUP = "dsr"
ESA_GROUP = "ev"
add_relay_to_groups = [RELAY_GROUP, ESA_GROUP]

try:
    if data.get(ATTR_QOS):
        if int(data.get(ATTR_QOS)) in [0, 1, 2]:
            qos = int(data.get(ATTR_QOS))
        else:
            raise ValueError
except ValueError:
    logger.error(
        "Wrong qos argument! Should be 0, 1 or 2. The default \
                        value 0 was used."
    )
temp_unit = ATTR_UNIT_CELSIUS
if data.get(ATTR_TEMP_UNIT) is not None:
    if data.get(ATTR_TEMP_UNIT) == "F":
        temp_unit = ATTR_UNIT_FARENHEIT
disc_prefix = "homeassistant"
if data.get(ATTR_DISCOVERY_PREFIX) is not None:
    disc_prefix = data.get(ATTR_DISCOVERY_PREFIX)

if data.get(ATTR_DEVELOP) is not None:
    develop = data.get(ATTR_DEVELOP)
if develop:
    disc_prefix = "develop"
    retain = False
    logger.error("DEVELOP MODE !!!")

if attr_id == "":
    logger.error("Expected non-empty id.")
else:

    # Initialise
    relays = 0
    meters = 0
    relay_components = [ATTR_SWITCH]
    config_component = ATTR_SWITCH
    relays_sensors = []
    relays_sensors_units = []
    relays_sensors_templates = []
    relays_sensors_classes = []
    relays_bin_sensors = []
    relays_bin_sensors_payload = []
    sensors = []
    sensors_units = []
    sensors_templates = []
    sensors_classes = []
    bin_sensors = []
    bin_sensors_classes = []
    battery_powered = False

    model = ATTR_MODEL_EMONEVSE_1

    # Relay configuration
    relays = 1
    relays_sensors = []  # [ATTR_CURRENT, ATTR_ENERGY]
    relays_sensors_units = [ATTR_UNIT_A, ATTR_UNIT_WH]
    relays_sensors_classes = [ATTR_POWER, ATTR_POWER]
    relays_sensors_templates = [ATTR_TEMPLATE_CURRENT, ATTR_TEMPLATE_ENERGY]
    relays_bin_sensors = []
    relays_bin_sensors_payload = []

    # Sensor configuration
    sensors = [
        ATTR_CURRENT,
        ATTR_CURRENT_PILOT,
        ATTR_ENERGY,
        ATTR_STATE,
        ATTR_TEMPERATURE,
    ]
    sensors_classes = ["power", "power", "power", None, "temperature"]
    sensors_units = [ATTR_UNIT_A, ATTR_UNIT_A, ATTR_UNIT_WH, None, temp_unit]
    sensors_templates = [
        ATTR_TEMPLATE_CURRENT,
        ATTR_TEMPLATE_CURRENT_PILOT,
        ATTR_TEMPLATE_ENERGY,
        ATTR_TEMPLATE_STATE,
        ATTR_TEMPLATE_TEMPERATURE,
    ]

    # Meters configuration
    meters = 0
    meters_sensors = [ATTR_POWER, ATTR_REACTIVE_POWER, ATTR_VOLTAGE]
    meters_sensors_units = [ATTR_UNIT_W, ATTR_UNIT_VAR, ATTR_UNIT_V]
    meters_sensors_classes = [ATTR_POWER, "", ""]
    meters_sensors_templates = [
        ATTR_TEMPLATE_POWER,
        ATTR_TEMPLATE_REACTIVE_POWER,
        ATTR_TEMPLATE_VOLTAGE,
    ]

    for relay_id in range(0, relays):
        # Add main EVSE relay
        device_name = "{}".format("openevse-" + attr_id[-4:])
        relay_name = "{} Relay {}".format(device_name, relay_id)
        default_topic = "openevse/"
        unique_id = "{}-relay-{}".format("openevse-" + attr_id[-4:], relay_id)
        if data.get(unique_id):
            config_component = data.get(unique_id)
        elif data.get(unique_id.lower()):
            config_component = data.get(unique_id.lower())
        for component in relay_components:
            config_topic = "{}/{}/{}-relay-{}/config".format(
                disc_prefix, component, "openevse-" + attr_id[-4:], relay_id
            )
            if component == config_component:
                payload = {}

                payload["name"] = relay_name
                payload["stat_t"] = "openevse-" + attr_id[-4:] + "/state"
                payload["cmd_t"] = "openevse/command/" + attr_id[-4:] + "/set"
                payload["uniq_id"] = unique_id
                payload["qos"] = str(qos)
                payload["~"] = default_topic
                payload["opt"] = "true"
                payload["pl_off"] = "OFF"
                payload["pl_on"] = "ON"
                payload["dev"] = {
                    "ids": [attr_id[-4:]],
                    "name": device_name,
                    "mdl": model,
                    "mf": ATTR_MANUFACTURER,
                }
                payload_str = str(payload)

            else:
                payload_str = ""

            mqtt_publish(config_topic, str(payload).replace("'", '"'), retain, qos)

        # Divert mode toggle
        device_name = "{}".format("openevse-" + attr_id[-4:])
        relay_name = "{} Divert {}".format(device_name, relay_id)
        default_topic = "openevse/"
        unique_id = "{}-divert-{}".format("openevse-" + attr_id[-4:], relay_id)

        if data.get(unique_id):
            config_component = data.get(unique_id)
        elif data.get(unique_id.lower()):
            config_component = data.get(unique_id.lower())
        for component in relay_components:
            config_topic = "{}/{}/{}-divert-{}/config".format(
                disc_prefix, component, "openevse-" + attr_id[-4:], relay_id
            )
            if component == config_component:

                payload = {
                    "name": relay_name,
                    "stat_t": "openevse-" + attr_id[-4:] + "/divertmode",
                    "cmd_t": "openevse-" + attr_id[-4:] + "/divertmode/set",
                    "val_tpl" : ATTR_TEMPLATE_DIVERT_MODE,
                    "uniq_id" : unique_id,
                    "qos": str(qos),
                    "~" : default_topic,
                    "opt": "true",
                    "pl_off" : "1",
                    "pl_on" : "2",
                    "dev": {
                        "ids": [attr_id[-4:]],
                        "name": device_name,
                        "mdl": model,
                        "mf": ATTR_MANUFACTURER,
                    },
                }
                payload_str = str(payload)

            else:
                payload_str = ""

            mqtt_publish(config_topic, str(payload).replace("'", '"'), retain, qos)

    # Add non-relay sensors
    for sensor_id in range(0, len(sensors)):
        device_name = "{}".format("openevse-" + attr_id[-4:])
        unique_id = "{}-{}".format("openevse-" + attr_id[-4:], sensors[sensor_id])
        config_topic = "{}/sensor/{}-{}/config".format(
            disc_prefix, "openevse-" + attr_id[-4:], sensors[sensor_id]
        )
        default_topic = "openevse-" + attr_id[-4:] + "/"
        sensor_name = "{} {}".format(device_name, sensors[sensor_id].capitalize())

        state_topic = "~{}".format(sensors[sensor_id])

        payload = {}
        payload["name"] = sensor_name
        payload["stat_t"] = state_topic
        if sensors_units[sensor_id] is not None:
            payload["unit_of_meas"] = sensors_units[sensor_id]
        if sensors_classes[sensor_id] is not None:
            payload["dev_cla"] = sensors_classes[sensor_id]
        payload["val_tpl"] = sensors_templates[sensor_id]
        payload["uniq_id"] = unique_id
        payload["qos"] = str(qos)
        payload["~"] = default_topic
        payload["dev"] = {
            "ids": [attr_id[-4:]],
            "name": device_name,
            "mdl": model,
            "mf": ATTR_MANUFACTURER,
        }

        mqtt_publish(config_topic, str(payload).replace("'", '"'), retain, qos)
