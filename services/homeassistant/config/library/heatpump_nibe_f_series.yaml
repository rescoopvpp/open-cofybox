template:
  - sensor:
      - name: "dhw tank temperature"
        unit_of_measurement: "°C"
        device_class: power
        state: '{{ states.sensor.bt7_hw_top_40013.state | float }}'
      - name: "indoor temperature"
        unit_of_measurement: "°C"
        device_class: power
        state: '{{ states.climate.climate_system_s1.attributes.current_temperature | float }}'
        
automation:
- id: 'nibe_mqtt_outdoor_temperature'
  alias: "Nibe heat pump: mqtt outdoor temperature"
  description: ""
  trigger:
  - platform: state
    entity_id: sensor.bt1_outdoor_temperature_40004
  condition:
  action:
    - service: mqtt.publish
      data:
        topic: data/devices/heatpump/outdoor_temperature
        payload: >
          {"entity":"heat_pump",
          "metric":"OutdoorTemperature", "metricKind":"gauge",
          "unit":"�°C", "friendly_name":"Heat pump Outdoor air temp.",
          "value": {{ states.sensor.bt1_outdoor_temperature_40004.state | float | string }},
          "timestamp": {{  now().timestamp() | int | string }},
          "sensorId":"heat_pump.outdoor_temperature"}
  mode: single
- id: 'nibe_mqtt_dhwtank_temperature'
  alias: "Nibe heat pump: mqtt dhwtank temperature"
  description: ""
  trigger:
  - platform: state
    entity_id: sensor.bt7_hw_top_40013
  condition:
  action:
    - service: mqtt.publish
      data:
        topic: data/devices/heatpump/dhwtank_temperature
        payload: >
          {"entity":"heat_pump", "metric":"DHWTankTemperature", "metricKind":"gauge",
          "unit":"°C", "friendly_name":"Heat pump DHW tank temperature",
          "value": {{ states.sensor.bt7_hw_top_40013.state | float | string }},
          "timestamp": {{ now().timestamp() | int | string }},
          "sensorId":"heat_pump.dhwtank_temperature"}
  mode: single
- id: 'nibe_mqtt_indoor_temperature'
  alias: "Nibe heat pump: mqtt indoor temperature"
  description: ""
  trigger:
  - platform: state
    entity_id:
      - climate.climate_system_s1
    attribute: current_temperature
  condition:
  action:
    - service: mqtt.publish
      data:
        topic: data/devices/heatpump/indoor_temperature
        payload: >
          {"entity":"heat_pump",
          "metric":"IndoorTemperature", "metricKind":"gauge",
          "unit":"°C", "friendly_name":"Heat pump Indoor ambient temperature",
          "value": {{ states.climate.climate_system_s1.attributes.current_temperature | float | string }},
          "timestamp": {{ now().timestamp() | int | string }},
          "sensorId":"heat_pump.indoor_temperature"}
  mode: single
- id: 'nibe_mqtt_indoor_temperature_setpoint'
  alias: "Nibe heat pump: mqtt indoor temperature setpoint"
  description: ""
  trigger:
  - platform: state
    entity_id:
      - climate.climate_system_s1
    attribute: temperature
  condition:
  action:
    - service: mqtt.publish
      data:
        topic: data/devices/heatpump/indoor_temperature_setpoint
        payload: >
          {"entity":"heat_pump",
          "metric":"IndoorTemperatureSetpoint", "metricKind":"gauge",
          "unit":"°C", "friendly_name":"Heat pump Room temperature setpoint",
          "value": {{ states.climate.climate_system_s1.attributes.temperature | float | string }},
          "timestamp": {{ now().timestamp() | int | string }},
          "sensorId":"heat_pump.indoor_temperature_setpoint"}
  mode: single
#SGR contacts
- id: 'sgr_heat_pump_normal_operation'
  alias: "SGR heat pump: normal operation"
  description: ""
  trigger:
  - platform: state
    entity_id:
        - input_select.flexibility_level_heating
  condition:
    - condition: state
      entity_id: input_select.flexibility_level_heating
      state: Normal operation
  action:
    - service: mqtt.publish
      data:
        topic: set/devices/heatpump/sgr_mode
        payload: |
          {% set mydict = {"value": 0 | int} %} {{ mydict }}
    - service: mqtt.publish
      data:
        topic: set/devices/heatpump/sgr_chan_1
        payload: |
          {% set mydict = {"value": "off"} %} {{ mydict }}
    - service: mqtt.publish
      data:
        topic: set/devices/heatpump/sgr_chan_2
        payload: |
          {% set mydict = {"value": "off"} %} {{ mydict }}
  mode: single
- id: 'sgr_heat_pump_force_off'
  alias: "SGR heat pump: force off"
  description: ""
  trigger:
  - platform: state
    entity_id: input_select.flexibility_level_heating
  - platform: state
    entity_id: input_boolean.manual_override
    from: "on"
    to: "off"
  condition:
    - condition: state
      entity_id: input_select.flexibility_level_heating
      state: Force off
    - condition: template
      value_template: >-
        {{ states('sensor.indoor_temperature') | float >
        states('input_number.minimum_indoor_temperature') | float }}
    - condition: template
      value_template: >-
        {{ states('sensor.dhw_tank_temperature') | float >
        states('input_number.minimum_dhw_tank_temperature') | float }}
    - condition: state
      entity_id: input_boolean.manual_override
      state: "off"
    - condition: state
      entity_id: input_boolean.opt_out
      state: "off"
  action:
    - service: mqtt.publish
      data:
        topic: set/devices/heatpump/sgr_mode
        payload: |
          {% set mydict = {"value": 2 | int} %} {{ mydict }}
    - service: mqtt.publish
      data:
        topic: set/devices/heatpump/sgr_chan_1
        payload: |
          {% set mydict = {"value": "off"} %} {{ mydict }}
    - service: mqtt.publish
      data:
        topic: set/devices/heatpump/sgr_chan_2
        payload: |
          {% set mydict = {"value": "on"} %} {{ mydict }}
  mode: single
- id: 'sgr_heat_pump_force_on'
  alias: "SGR heat pump: force on"
  description: ""
  trigger:
    - platform: state
      entity_id: input_select.flexibility_level_heating
    - platform: state
      entity_id: input_boolean.manual_override
      from: "on"
      to: "off"
  condition:
    - condition: state
      entity_id: input_select.flexibility_level_heating
      state: Force on
    - condition: state
      entity_id: input_boolean.manual_override
      state: "off"
    - condition: state
      entity_id: input_boolean.opt_out
      state: "off"
  action:
    - service: mqtt.publish
      data:
        topic: set/devices/heatpump/sgr_mode
        payload: |
          {% set mydict = {"value": 3 | int} %} {{ mydict }}
    - service: mqtt.publish
      data:
      topic: set/devices/heatpump/sgr_chan_1
      payload: |
        {% set mydict = {"value": "on"} %} {{ mydict }}
    - service: mqtt.publish
      data:
        topic: set/devices/heatpump/sgr_chan_2
        payload: |
          {% set mydict = {"value": "on"} %} {{ mydict }}
  mode: single
- id: 'sgr_heat_pump_manual_override'
  alias: "SGR heat pump: manual override"
  description: Disengage flexibility controls if manual override is activated
  trigger:
    - platform: state
      entity_id: input_boolean.manual_override
      from: "off"
      to: "on"
  condition: []
  action:
    - service: mqtt.publish
      data:
        topic: set/devices/heatpump/sgr_mode
        payload: |
          {% set mydict = {"value": 0 | int} %} {{ mydict }}
    - service: mqtt.publish
      data:
        topic: set/devices/heatpump/sgr_chan_1
        payload: |
          {% set mydict = {"value": "off"} %} {{ mydict }}
    - service: mqtt.publish
      data:
        topic: set/devices/heatpump/sgr_chan_2
        payload: |
          {% set mydict = {"value": "off"} %} {{ mydict }}
  mode: single
- id: 'sgr_heat_pump_automatic_override'
  alias: "SGR heat pump: automatic override"
  description: >-
    Disengage flexibility controls if one of the temperature boundary conditions
    has been reached
  trigger:
    - platform: state
      entity_id:
        - sensor.indoor_temperature
        - sensor.dhw_tank_temperature
  condition:
    - condition: or
      conditions:
        - condition: template
          value_template: >-
            {{ states('sensor.indoor_temperature') | float <
            states('input_number.minimum_indoor_temperature') | float }}
        - condition: template
          value_template: >-
            {{ states('sensor.dhw_tank_temperature') | float <
            states('input_number.minimum_dhw_tank_temperature') | float }}
  action:
    - service: mqtt.publish
      data:
        topic: set/devices/heatpump/sgr_mode
        payload: |
          {% set mydict = {"value": 0 | int} %} {{ mydict }}
    - service: mqtt.publish
      data:
        topic: set/devices/heatpump/sgr_chan_1
        payload: |
          {% set mydict = {"value": "off"} %} {{ mydict }}
    - service: mqtt.publish
      data:
        topic: set/devices/heatpump/sgr_chan_2
        payload: |
          {% set mydict = {"value": "off"} %} {{ mydict }}
  mode: single