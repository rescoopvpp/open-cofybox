## Upgrading homeassistant

We should follow the [homeassistant release cycle](https://www.home-assistant.io/faq/release/) fairly closely. They release on the first Wednesday of every month at time of writing. There is usually a lot of good stuff, so it would be good to also update once a month.

We can only update when docker images have been uploaded to both the [raspberrypi3 repository](https://hub.docker.com/r/homeassistant/raspberrypi3-homeassistant/)(for the boxes) and the [amd64 repository](https://hub.docker.com/r/homeassistant/amd64-homeassistant/)(for testing locally).

We should check the [release notes](https://www.home-assistant.io/blog/categories/release-notes/) to see if anything is being deprecated or removed entirely, and act accordingly.

To do the upgrade, you just change the `ha_version` in the docker-compose file. It is also necessary to update the `ha_compatible_pip_version` to the latest pip version which that version of homeassistant is compatible with.

We should also check our custom integrations and scripts. These don't automatically update, so we have to do it manually.

To update custom integrations, you have to:
- clone the latest version of the repository from GitHub or similar (or the most recent stable version, if they have such a thing)
- remove the folder `services/homeassistant/custom_components/COMPONENT_NAME`
- copy the folder (and files within) `custom_components/COMPONENT_NAME` from the cloned repo, e.g. `cp -r ha-myenergi/custom_components/myenergi cofybox-balena/services/homeassistant/custom_components`
- make sure to test these with the new version of homeassistant.

The other things to check are the files under `services/homeassistant/config/automations` and `services/homeassistant/config/python_scripts`.

Once we've updated all of the above, we should test
- locally
- on a local mode box
- on staging
- on prod

Take note of any deprecation warnings when homeassistant starts up!
