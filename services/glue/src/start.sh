#!/bin/bash
if test "$COFYBOX_ENABLE_SERVICE" = TRUE ; then
    cp -n config/default_glue_keys.json /cofybox-config/glue_keys.json

    python glue.py
else
    echo "Glue Block not enabled by environment variable"
fi