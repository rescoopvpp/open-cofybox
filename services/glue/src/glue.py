# -*- coding: utf-8 -*-
"""
@author: Joannes //Epyon// Laveyne
"""
import datetime
import json
import logging
import os
import signal
import time
from enum import Enum
from typing import Callable
from typing import Dict
from typing import List
from typing import Optional
from typing import Tuple
from typing import Union

import paho.mqtt.client as mqtt
from jinja2 import Environment
from jinja2 import meta
from jinja2 import StrictUndefined
from jinja2 import Template
from jinja2.exceptions import TemplateSyntaxError
from jinja2.exceptions import UndefinedError
from pydantic import BaseModel
from pydantic import validator


# Logging setup
logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class metricEnum(str, Enum):
    GridElectricityImport = "GridElectricityImport"
    GridElectricityExport = "GridElectricityExport"
    GridElectricityPower = "GridElectricityPower"
    GridElectricityVoltage = "GridElectricityVoltage"
    EvElectricityVoltage = "EvElectricityVoltage"
    EvElectricityCurrent = "EvElectricityCurrent"
    EvElectricityImport = "EvElectricityImport"
    EvElectricityPower = "EvElectricityPower"
    HpElectricityVoltage = "HpElectricityVoltage"
    HpElectricityCurrent = "HpElectricityCurrent"
    HpElectricityConsumption = "HpElectricityConsumption"
    HpElectricityPower = "HpElectricityPower"
    PvElectricityVoltage = "PvElectricityVoltage"
    PvElectricityCurrent = "PvElectricityCurrent"
    PvElectricityProduction = "PvElectricityProduction"
    PvElectricityPower = "PvElectricityPower"
    HeaterElectricityVoltage = "HeaterElectricityVoltage"
    HeaterElectricityCurrent = "HeaterElectricityCurrent"
    HeaterElectricityConsumption = "HeaterElectricityConsumption"
    HeaterElectricityPower = "HeaterElectricityPower"
    BatteryElectricityImport = "BatteryElectricityImport"
    BatteryElectricityExport = "BatteryElectricityExport"
    BatteryElectricitySoc = "BatteryElectricitySoc"
    BatteryElectricityPower = "BatteryElectricityPower"
    BatteryElectricityVoltage = "BatteryElectricityVoltage"
    BatteryElectricityCharge = "BatteryElectricityCharge"
    BatteryElectricityDecharge = "BatteryElectricityDecharge"
    BatteryElectricityCharging = "BatteryElectricityCharging"
    IndoorTemperatureSetpoint = "IndoorTemperatureSetpoint"
    DHWTankTemperature = "DHWTankTemperature"
    electricityImport = "electricityImport"  # The electricity import to house measured at the grid connection point.
    naturalGasImport = (
        " naturalGasImport"  # The gas import to house at the grid connection point.
    )
    districtHeatingImport = (
        "districtHeatingImport"  # The heat import to house at the grid point.
    )
    districtCoolingImport = (
        "districtCoolingImport"  # The cooling import to house at the grid point.
    )
    fuelOilStockDraw = "fuelOilStockDraw"
    solarPhotovoltaicProduction = "solarPhotovoltaicProduction"
    solarThermalProduction = "solarThermalProduction"
    windPowerProduction = "windPowerProduction"
    cogenerationPowerProduction = "cogenerationPowerProduction"
    electricityExport = "electricityExport"  # The electricity export from house measured at the grid connection point.
    electricVehicleCharging = "electricVehicleCharging"
    # The electricity used by a final (end) appliance/circuit.
    finalElectricityConsumption = "finalElectricityConsumption"
    finalHeatConsumption = (
        "finalHeatConsumption"  # The heat used by a final (end) appliance/circuit.
    )
    finalCoolingConsumption = "finalCoolingConsumption"  # The cooling used by a final (end) appliance/circuit.
    drinkingWaterImport = "drinkingWaterImport"
    rainwaterStockDraw = "rainwaterStockDraw"
    groundwaterImport = "groundwaterImport"
    IndoorTemperature = (
        "IndoorTemperature"  # A temperature measurement from inside a building.
    )
    OutdoorTemperature = (
        "OutdoorTemperature"  # A temperature measurement from outside a building.
    )
    relativeIndoorHumidity = (
        "relativeIndoorHumidity"  # A RH measurement from inside a building.
    )
    relativeOutdoorHumidity = (
        "relativeOutdoorHumidity"  # A RH measurement from outside a building.
    )


class metricKindEnum(str, Enum):
    # - The value constantly increases over
    # . The value can periodically restart from zero when the maximum possible value of the counter is exceeded.
    cumulative = "cumulative"
    delta = "delta"  # - The value measures the change since it was last recorded.
    gauge = "gauge"  # - The value measures a specific instant in time. For example, metrics measuring temperature.
    actuator = "actuator"  # - The value indicates a state. Can be 0 or 1.


class unitEnum(str, Enum):
    """
    Enum for use in GlueRecipeConfig Pydantic model.
    """

    # W, kW, kWh, Wh, V, A, l, m³, kg, km, °C, %, count, none
    W = "W"
    kW = "kW"
    kWh = "kWh"
    Wh = "Wh"
    V = "V"
    A = "A"
    l = "l"  # noqa: E741
    m3 = "m3"
    kg = "kg"
    km = "km"
    C = "C"
    percent = "%"
    count = "count"
    none = "none"


def is_valid_jinja2_template(template: str) -> bool:
    env = Environment()
    try:
        env.parse(template)
        return True
    except TemplateSyntaxError:
        return False


class GlueIngredientConfig(BaseModel):
    friendly_name: Optional[str]
    topic_in: str
    json_template: Optional[str]

    @validator("topic_in")
    def is_a_valid_mqtt_topic(cls, v):
        if not mqtt.topic_matches_sub("#", v):
            raise ValueError(f"Not a valid MQTT topic! ({v})")
        return v

    @validator("json_template")
    def is_a_valid_jinja2_template(cls, v):
        if is_valid_jinja2_template(v):
            return v
        else:
            raise ValueError(f"json_template is not valid jinja2 template!:{v}")


class GlueRecipeConfig(BaseModel):
    friendly_name: Optional[str]
    topic_out: str
    recipe: str
    entity: str
    channel: str
    unit: unitEnum
    metricKind: metricKindEnum
    metric: metricEnum
    ignore_after: Optional[int] = 300
    ignore: Optional[bool] = False

    @validator("topic_out")
    def is_a_valid_mqtt_topic(cls, v):
        if not mqtt.topic_matches_sub("#", v):
            raise ValueError(f"Not a valid MQTT topic! ({v})")
        return v

    @validator("recipe")
    def is_a_valid_jinja2_template(cls, v):
        if is_valid_jinja2_template(v):
            return v
        else:
            raise ValueError(f"json_template is not valid jinja2 template!:{v}")


class HADiscoveryPayloadDeviceClassEnum(str, Enum):
    """
    We only support a subset of the HA device classes.

    See: https://developers.home-assistant.io/docs/core/entity/sensor/#available-device-classes
    """

    battery = "battery"
    current = "current"
    energy = "energy"
    frequency = "frequency"
    gas = "gas"


class HADiscoveryPayloadStateClassEnum(str, Enum):
    """
    See: https://developers.home-assistant.io/docs/core/entity/sensor/#available-state-classes
    """

    measurement = "measurement"
    total = "total"
    total_increasing = "total_increasing"


class HADiscoveryPayloadDevice(BaseModel):
    identifiers: List[str]
    name: str
    model: str
    manufacturer: str


class HADiscoveryPayload(BaseModel):
    name: str
    unit_of_measurement: Optional[unitEnum]
    device_class: HADiscoveryPayloadDeviceClassEnum
    state_class: Optional[HADiscoveryPayloadStateClassEnum]
    state_topic: str
    value_template: Optional[str]
    unique_id: Optional[str]
    last_reset: Optional[datetime.datetime]
    last_reset_value_template: Optional[datetime.datetime]
    device: Optional[HADiscoveryPayloadDevice]


class GlueConfig(BaseModel):
    version: str
    ingredients: Dict[str, GlueIngredientConfig]
    recipes: Dict[str, GlueRecipeConfig]

    def to_ha(self):
        """
        Generates a set of HA discovery payloads from config file.
        """
        return [
            HADiscoveryPayload(
                name=r.channel,
                unit_of_measurement=r.unit,
                device_class="energy",
                state_class="total",
                state_topic="data/devices/" + r.entity + "/" + r.channel,
                value_template="{{ value_json.value }}",
                # TODO:
                # implement once JSON templating supported in ingredient ID specifier
                # unique_id=r.entity + "_" + r.channel,
                # TODO: Some of below maybe possible at some point.
                #   Without device field HA device will not be initialised.
                # device=HADiscoveryPayloadDevice(
                #    identifiers=[r.entity.replace(" ", "_")],
                #    name="",
                #    model="",
                #    manufacturer="",
                # ),
            )
            for _, r in self.recipes.items()
        ]


class GlueOutputPayload(BaseModel):
    entity: str
    channel: str
    value: Union[int, float]
    unit: unitEnum
    metric: metricEnum
    metricKind: metricKindEnum
    timestamp: datetime.datetime


def to_number_if_possible(value: Union[str, float, int]) -> Union[int, float, str]:
    """
    Returns input string as an int if possible, else a float if possible, else makes no change.
    """
    try:
        return int(value)
    except ValueError:
        try:
            return float(value)
        except ValueError:
            return value


class Glue(mqtt.Client):

    """
    Main Glue client class extending the paho MQTT client.
    """

    ingredients: dict
    recipes: dict

    def __init__(
        self, config: GlueConfig, client_id: str = "Glue", broker: str = "localhost"
    ):
        mqtt.Client.__init__(self, client_id=client_id)

        self.ingredients, self.recipes = self._parse_config(config)

        # Build the list of topics to subscribe to - this is mainly to support debugging by connecting a
        # logger to a callback on all topics.
        sublist = []
        for _, i in self.ingredients.items():
            sublist.append(i["topic_in"])
        self.subs = [(s, 0) for s in list(set(sublist))]

        self.connect(broker)

        self.subscribe(self.subs)

        # Create recipe message callbacks for defined ingredients
        for i_name, i in self.ingredients.items():
            topic_in = i["topic_in"]
            logger.info(f"Adding recipes for {topic_in}")
            for recipe_name in i["recipes"]:

                self.message_callback_add(
                    topic_in, self.__create_recipe_callback(recipe_name, i_name)
                )

        # Create HA discovery for recipes

        discovery_payloads = config.to_ha()

        for dp in discovery_payloads:
            discovery_topic = "homeassistant/sensor/" + dp.name + "/config"
            self.publish(discovery_topic, "")
            self.publish(discovery_topic, dp.json(exclude_none=True), retain=True)

    def __create_recipe_callback(
        self, recipe_name: str, ingredient_name: str
    ) -> Callable:
        """
        Factory for recipe callback function. Returns a closure with appropriate recipe/ingredient names in scope.
        """
        logger.info(f"Creating recipe callback for {recipe_name} : {ingredient_name}")

        recipe = self.recipes[recipe_name]
        json_template = self.ingredients[ingredient_name]["json_template"]

        def recipe_callback(client, userdata, message):
            payload = str(message.payload.decode("utf-8"))
            timestamp = int(time.time())
            try:
                value, timestamp = self.load_value_from_payload(
                    payload, timestamp, json_template
                )
                logger.info(
                    "Loaded payload for "
                    + ingredient_name
                    + ":"
                    + str(timestamp)
                    + ":"
                    + str(payload)
                )
            except UndefinedError:
                logger.info("UndefinedError?")
                return

            self.update_ingredient(ingredient_name, value, timestamp)

            logger.info("Executing recipe - " + recipe_name)
            try:
                output_value = self.execute_recipe(recipe_name)

                if output_value is not None:
                    glue_output_payload = self.create_glue_output_payload(
                        recipe_name, value, timestamp
                    ).json()
                    logger.info(f"Publishing to {recipe['topic_out']} : {output_value}")
                    self.publish(recipe["topic_out"], glue_output_payload)

            except Exception:
                logger.exception(f"Messed up the recipe execution: {recipe_name}")

        return recipe_callback

    def on_message(self, client, userdata, message):
        """
        Debug default callback which will log all messages received.
        """
        logger.info(
            f"Message received : {message.payload.decode('utf-8')} on {message.topic}"
        )

    def load_value_from_payload(
        self, payload: str, timestamp: int, json_template: Optional[str] = None,
    ) -> Tuple[str, int]:
        """
        Checks the received message, extracts the value (template)
        If the message contains a timestamp, returns that, otherwise returns the current
        time that has been passed in.
        """

        value = None

        # If a template was not supplied for payload parsing then just return value and timestamp
        if not json_template:
            value = payload  # <-check required here as this is straight off bus?
            return value, timestamp

        payload_dict = json.loads(payload)

        # If payload was not JSON convert to dummy format so templating can be used (or do not provide template)
        if type(payload_dict) is not dict:
            payload_dict = {"value": payload_dict}

        jinja_template = Template(json_template, undefined=StrictUndefined)

        value = jinja_template.render(payload_dict)

        try:
            timestamp = payload_dict["timestamp"]
        except KeyError:
            pass

        return value, timestamp

    def update_ingredient(
        self, ingredient_name: str, value: Union[int, float], timestamp: int
    ):
        self.ingredients[ingredient_name]["last_value"] = value
        self.ingredients[ingredient_name]["last_timestamp"] = timestamp

    def execute_recipe(self, recipe_name: str,) -> Union[str, int, float, None]:

        # Evaluate greatest difference in timestamps between ingredients

        t = [
            self.ingredients[ingredient]["last_timestamp"]
            for ingredient in self.recipes[recipe_name]["ingredients"]
        ]
        max_dt = max(t) - min(t)

        # if greater than ignore_after in recipe then ignore (unless ignore flag)
        fresh = True
        if max_dt > self.recipes[recipe_name]["ignore_after"]:
            fresh = False

        if fresh or bool(self.recipes[recipe_name]["ignore"]):

            # Evaluate recipe template using relevant ingredients
            value = Template(self.recipes[recipe_name]["recipe"]).render(
                **{
                    i: float(self.ingredients[i]["last_value"])
                    for i in self.recipes[recipe_name]["ingredients"]
                }
            )

            logger.info("Recipe " + recipe_name + " evaluated to " + str(value))

            return to_number_if_possible(value)

    def create_glue_output_payload(
        self, recipe_name: str, value: Union[int, float, str], timestamp: int
    ) -> GlueOutputPayload:

        return GlueOutputPayload(
            entity=self.recipes[recipe_name]["entity"],
            channel=self.recipes[recipe_name]["channel"],
            value=to_number_if_possible(value),
            unit=self.recipes[recipe_name]["unit"],
            metric=self.recipes[recipe_name]["metric"],
            metricKind=self.recipes[recipe_name]["metricKind"],
            timestamp=timestamp,
        )

    @staticmethod
    def _parse_config(config: GlueConfig) -> Tuple[dict, dict]:

        config_ = config.dict()

        raw_ingredients = config_["ingredients"]
        ingredients = {}

        all_recipes = config_["recipes"]

        # Build a list of ingredients referenced in recipe and a map from an ingredient to a list of recipes
        i_r: Dict[str, List[str]] = {}
        for recipe_name, r in all_recipes.items():
            parsed_ingredients_ = meta.find_undeclared_variables(
                Environment().parse(r["recipe"])
            )
            all_recipes[recipe_name]["ingredients"] = parsed_ingredients_
            for i in parsed_ingredients_:
                try:
                    i_r[i].append(recipe_name)
                except KeyError:
                    i_r[i] = [recipe_name]

        # Build the ingredients internal data structure. Add fields for the last value and last timestamp.
        for ingredient in raw_ingredients:

            try:
                recipes = i_r[ingredient]
            except KeyError:
                logger.warning(
                    f"Ingredient {ingredient} is not referenced in any recipe!"
                )
                i_r[ingredient] = []
                recipes = []

            ingredients[ingredient] = {
                "friendly_name": raw_ingredients[ingredient]["friendly_name"],
                "topic_in": raw_ingredients[ingredient]["topic_in"],
                "json_template": raw_ingredients[ingredient]["json_template"],
                "recipes": recipes,
                "last_value": None,
                "last_timestamp": None,
            }
            del i_r[ingredient]

        if i_r:
            logger.error(
                "There are ingredients referenced in recipes which are not defined in glue configuration!"
                + str(i_r)
            )
            raise RuntimeError

        return ingredients, config_["recipes"]


class GracefulDeath:
    """
    When the Grim Sysadmin comes to reap with his scythe,
    let this venerable daemon process die a Graceful Death
    """

    kill_now = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, signum, frame):
        self.kill_now = True


"""
The main loop which checks for subcribed topics and calls the message handler
when a message arrives
"""
if __name__ == "__main__":
    """
    Read in the config file containing the glue configuration and initialise the script
    """

    import argparse
    import pathlib

    parser = argparse.ArgumentParser(description="Run Glue.")
    parser.add_argument(
        "--config",
        type=pathlib.Path,
        help="path to config file",
        default="/cofybox-config/glue_keys.json",
    )
    args = parser.parse_args()

    GLUE_CONFIG_PATH = args.config

    MQTT_BROKER = os.getenv("MQTT_BROKER", "localhost")
    logger.info(f"MQTT Broker is {MQTT_BROKER}")

    glue_config_timestamp = os.stat(GLUE_CONFIG_PATH).st_mtime
    glue_config = GlueConfig.parse_file(GLUE_CONFIG_PATH)
    glue_client = Glue(glue_config, "Glue", MQTT_BROKER)

    killer = GracefulDeath()

    while not killer.kill_now:
        try:
            glue_client.loop_start()
            glue_client.loop_stop()

            # Check for config changes and reload on change
            if glue_config_timestamp != os.stat(GLUE_CONFIG_PATH).st_mtime:
                logger.info(
                    "Glue config has changed, reloading and restarting client..."
                )
                glue_config_timestamp = os.stat(GLUE_CONFIG_PATH).st_mtime

                glue_client.loop_stop()
                glue_client.disconnect()

                glue_config = GlueConfig.parse_file(GLUE_CONFIG_PATH)
                glue_client = Glue(glue_config, "Glue", MQTT_BROKER)

        except KeyboardInterrupt:
            glue_client.loop_stop()
            logger.info("Unsubscribing from topics...")
            for sub_topics in glue_client.subs:
                glue_client.unsubscribe(sub_topics[0])
            logger.info("done")
            logger.info("Disconnecting from broker... ", end="")
            glue_client.disconnect()
            logger.info("Press Ctrl-C to terminate while statement")

    # Make a graceful exit when this daemon gets terminated
    logger.info("Termination signal received")
    logger.info("Unsubscribing from topics... ", end="")
    for topic in glue_client.subs:
        glue_client.unsubscribe(topic[0])
    logger.info("done")
    logger.info("Disconnecting from broker... ", end="")
    glue_client.disconnect()
    logger.info("done")
