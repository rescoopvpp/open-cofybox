import math
import random
import unittest
from unittest.mock import patch

from jinja2.exceptions import UndefinedError

from glue import Glue
from glue import GlueConfig

random.seed()


class AbstractTestCase(unittest.TestCase):
    pass


class ParsingValidConfigTestCase(AbstractTestCase):
    def setUp(self) -> None:
        self.config_dict = {
            "version": "202111120",
            "ingredients": {
                "total_consumption": {
                    "friendly_name": "Total energy consumed",
                    "topic_in": "shellies/shellyem-B9F2CA/emeter/1/total",
                },
                "total_injection": {
                    "friendly_name": "Total energy injected",
                    "topic_in": "shellies/shellyem-B9F2CA/emeter/1/total_returned",
                },
                "total_power": {
                    "friendly_name": "Total energy injected",
                    "topic_in": "shellies/shellyem-B9F2CA/emeter/1/power",
                },
                "voltage": {
                    "friendly_name": "Grid voltage",
                    "topic_in": "shellies/shellyem-B9F2CA/emeter/1/voltage",
                    "value_template": "value_json.foo",
                },
            },
            "recipes": {
                "total_consumed": {
                    "friendly_name": "Total consumption",
                    "topic_out": "data/devices/utility_meter/total_energy_consumed",
                    "recipe": "{{total_consumption}}",
                    "entity": "utility_meter",
                    "channel": "total_energy_consumed",
                    "unit": "Wh",
                    "metricKind": "cumulative",
                    "metric": "electricityImport",
                },
                "total_consumedpo": {
                    "friendly_name": "Total consumption",
                    "topic_out": "data/devices/utility_meter/total_energy_consumed",
                    "recipe": "{{total_consumption}}",
                    "entity": "utility_meter",
                    "channel": "total_energy_consumed",
                    "unit": "Wh",
                    "metricKind": "cumulative",
                    "metric": "electricityImport",
                },
            },
        }

    def test_parse_config(self) -> None:
        ingredients, recipes = Glue._parse_config(
            GlueConfig.parse_obj(self.config_dict)
        )

        self.assertIsInstance(ingredients, dict)
        self.assertIsInstance(recipes, dict)
        self.assertEqual(4, len(ingredients), "Should have 4 ingredients")
        self.assertEqual(2, len(recipes), "Should have 2 recipes")
        self.assertEqual(
            0,
            len(ingredients["total_injection"]["recipes"]),
            "Should not have any associated recipes",
        )

        self.assertEqual(
            2,
            len(ingredients["total_consumption"]["recipes"]),
            "Should have 2 associated recipes",
        )
        self.assertEqual(
            "total_consumed",
            ingredients["total_consumption"]["recipes"][0],
            "The associated recipe should be total_energy_consumed",
        )


@patch("glue.Glue.connect")
class LoadValuesFromTemplateTestCase(AbstractTestCase):
    def test_load_value_from_simple_json_payload(self, mock_connect):
        config_json = {
            "version": "202111120",
            "ingredients": {
                "total_consumption": {
                    "friendly_name": "Total energy consumed",
                    "topic_in": "dummy-device/emeter/1/total",
                    "json_template": "{{energy_consumed}}",
                },
            },
            "recipes": {
                "total_consumed": {
                    "friendly_name": "Total consumption",
                    "topic_out": "data/devices/utility_meter/total_energy_consumed",
                    "recipe": "{{total_consumption}}",
                    "entity": "utility_meter",
                    "channel": "total_energy_consumed",
                    "unit": "Wh",
                    "metricKind": "cumulative",
                    "metric": "electricityImport",
                }
            },
        }
        glue_client = Glue(GlueConfig.parse_obj(config_json), "test_glue")

        payload = """
        {"energy_consumed": 123}
        """
        trigger = "dummy-device/emeter/1/total"  # noqa: F841

        value, timestamp = glue_client.load_value_from_payload(
            payload,
            4000,
            config_json["ingredients"]["total_consumption"]["json_template"],
        )

        self.assertEqual("123", value)
        self.assertEqual(4000, timestamp)

    def test_load_value_with_calculated_value(self, mock_connect):
        config_json = {
            "version": "202111120",
            "ingredients": {
                "total_consumption": {
                    "friendly_name": "Device Status",
                    "topic_in": "dummy-device/jinja",
                    "json_template": "{{test1 - test2 |int}}",
                },
            },
            "recipes": {
                "total_consumed": {
                    "friendly_name": "Total consumption",
                    "topic_out": "data/devices/utility_meter/total_energy_consumed",
                    "recipe": "{{total_consumption}}",
                    "entity": "utility_meter",
                    "channel": "total_energy_consumed",
                    "unit": "Wh",
                    "metricKind": "cumulative",
                    "metric": "electricityImport",
                }
            },
        }
        glue_client = Glue(GlueConfig.parse_obj(config_json), "test_glue")

        payload = """{"test1": 1, "test2": 3, "test3": 7, "year": 1964}"""
        trigger = "dummy-device/jinja"  # noqa: F841

        value, timestamp = glue_client.load_value_from_payload(
            payload,
            4000,
            config_json["ingredients"]["total_consumption"]["json_template"],
        )

        self.assertEqual("-2", value)
        self.assertEqual(4000, timestamp)

    def test_load_value_when_no_template(self, mock_connect):
        config_json = {
            "version": "202111120",
            "ingredients": {
                "total_consumption": {
                    "friendly_name": "Device Status",
                    "topic_in": "dummy-device/state",
                },
            },
            "recipes": {
                "total_consumed": {
                    "friendly_name": "Total consumption",
                    "topic_out": "data/devices/utility_meter/total_energy_consumed",
                    "recipe": "{{total_consumption}}",
                    "entity": "utility_meter",
                    "channel": "total_energy_consumed",
                    "unit": "Wh",
                    "metricKind": "cumulative",
                    "metric": "electricityImport",
                }
            },
        }
        glue_client = Glue(GlueConfig.parse_obj(config_json), "test_glue")

        payload = "on"
        trigger = "dummy-device/state"  # noqa: F841

        value, timestamp = glue_client.load_value_from_payload(payload, 4000)

        self.assertEqual("on", value)
        self.assertEqual(4000, timestamp)

    def test_load_value_when_blank_template(self, mock_connect):
        config_json = {
            "version": "202111120",
            "ingredients": {
                "total_consumption": {
                    "friendly_name": "Device Status",
                    "topic_in": "dummy-device/state",
                    "json_template": "",
                },
            },
            "recipes": {
                "total_consumed": {
                    "friendly_name": "Total consumption",
                    "topic_out": "data/devices/utility_meter/total_energy_consumed",
                    "recipe": "{{total_consumption}}",
                    "entity": "utility_meter",
                    "channel": "total_energy_consumed",
                    "unit": "Wh",
                    "metricKind": "cumulative",
                    "metric": "electricityImport",
                }
            },
        }
        glue_client = Glue(GlueConfig.parse_obj(config_json), "test_glue")

        payload = "on"
        trigger = "dummy-device/state"  # noqa: F841

        value, timestamp = glue_client.load_value_from_payload(
            payload,
            4000,
            config_json["ingredients"]["total_consumption"]["json_template"],
        )

        self.assertEqual("on", value)
        self.assertEqual(4000, timestamp)

    def test_load_value_parses_timestamp_from_json_payload(self, mock_connect):
        config_json = {
            "version": "202111120",
            "ingredients": {
                "total_consumption": {
                    "friendly_name": "Device Status",
                    "topic_in": "dummy-device/jinja",
                    "json_template": "{{test1 - test2 |int}}",
                },
            },
            "recipes": {
                "total_consumed": {
                    "friendly_name": "Total consumption",
                    "topic_out": "data/devices/utility_meter/total_energy_consumed",
                    "recipe": "{{total_consumption}}",
                    "entity": "utility_meter",
                    "channel": "total_energy_consumed",
                    "unit": "Wh",
                    "metricKind": "cumulative",
                    "metric": "electricityImport",
                }
            },
        }
        glue_client = Glue(GlueConfig.parse_obj(config_json), "test_glue")

        payload = (
            """{"test1": 1, "test2": 3, "test3": 7, "year": 1964, "timestamp": 3000}"""
        )
        trigger = "dummy-device/jinja"  # noqa: F841

        value, timestamp = glue_client.load_value_from_payload(
            payload,
            4000,
            config_json["ingredients"]["total_consumption"]["json_template"],
        )

        self.assertEqual("-2", value)
        self.assertEqual(3000, timestamp)


@patch("glue.Glue.connect")
class ValueNotInPayloadTestCase(AbstractTestCase):
    def test_load_value_fails_gracefully_if_key_not_in_payload(self, mock_connect):
        config_json = {
            "version": "202111120",
            "ingredients": {
                "total_consumption": {
                    "friendly_name": "Device Status",
                    "topic_in": "dummy-device/jinja",
                    "json_template": "{{foo}}",
                },
            },
            "recipes": {
                "total_consumed": {
                    "friendly_name": "Total consumption",
                    "topic_out": "data/devices/utility_meter/total_energy_consumed",
                    "recipe": "{{total_consumption}}",
                    "entity": "utility_meter",
                    "channel": "total_energy_consumed",
                    "unit": "Wh",
                    "metricKind": "cumulative",
                    "metric": "electricityImport",
                }
            },
        }
        glue_client = Glue(GlueConfig.parse_obj(config_json), "test_glue")

        payload = """{"bar": 3000}"""
        trigger = "dummy-device/jinja"  # noqa: F841

        with self.assertRaises(UndefinedError):
            glue_client.load_value_from_payload(
                payload,
                4000,
                config_json["ingredients"]["total_consumption"]["json_template"],
            )


@patch("glue.Glue.connect")
class MultipleIngredientsInGlue(AbstractTestCase):
    def test_execute_recipe_with_multiple_present_ingredients_which_are_fresh(
        self, mock_connect
    ):
        config_json = {
            "version": "202111120",
            "ingredients": {
                "total_consumption_1": {
                    "friendly_name": "Device 1 Power Usage",
                    "topic_in": "dummy-device-1/jinja",
                    "json_template": "{{bar}}",
                },
                "total_consumption_2": {
                    "friendly_name": "Device Status",
                    "topic_in": "dummy-device-2/jinja",
                    "json_template": "{{bar/1000}}",
                },
            },
            "recipes": {
                "total_consumed": {
                    "friendly_name": "Total consumption",
                    "topic_out": "data/devices/utility_meter/total_energy_consumed",
                    "recipe": "{{total_consumption_1+total_consumption_2}}",
                    "entity": "utility_meter",
                    "channel": "total_energy_consumed",
                    "unit": "Wh",
                    "metricKind": "cumulative",
                    "metric": "electricityImport",
                }
            },
        }
        glue_client = Glue(GlueConfig.parse_obj(config_json), "test_glue")

        value_1 = random.random() * 3
        value_2 = random.random() * 3000

        payload1 = '{"bar": ' + str(value_1) + "}"
        trigger1 = "dummy-device-1/jinja"  # noqa: F841

        payload2 = '{"bar": ' + str(value_2) + "}"
        trigger2 = "dummy-device-2/jinja"  # noqa: F841

        ex_value_1, ts_1 = glue_client.load_value_from_payload(
            payload1,
            4000,
            config_json["ingredients"]["total_consumption_1"]["json_template"],
        )

        glue_client.update_ingredient("total_consumption_1", ex_value_1, ts_1)

        ex_value_2, ts_2 = glue_client.load_value_from_payload(
            payload2,
            4030,
            config_json["ingredients"]["total_consumption_2"]["json_template"],
        )

        glue_client.update_ingredient("total_consumption_2", ex_value_2, ts_2)
        output_value = glue_client.execute_recipe("total_consumed")

        assert math.isclose(float(ex_value_1), value_1)
        assert math.isclose(float(ex_value_2), value_2 / 1000)
        assert math.isclose(
            float(ex_value_1) + float(ex_value_2), value_1 + value_2 / 1000
        )
        assert math.isclose(value_1 + value_2 / 1000, float(output_value))

    def test_execute_recipe_with_multiple_present_ingredients_which_are_stale(
        self, mock_connect
    ):
        config_json = {
            "version": "202111120",
            "ingredients": {
                "total_consumption_1": {
                    "friendly_name": "Device 1 Power Usage",
                    "topic_in": "dummy-device-1/jinja",
                    "json_template": "{{bar}}",
                },
                "total_consumption_2": {
                    "friendly_name": "Device Status",
                    "topic_in": "dummy-device-2/jinja",
                    "json_template": "{{bar/1000}}",
                },
            },
            "recipes": {
                "total_consumed": {
                    "friendly_name": "Total consumption",
                    "topic_out": "data/devices/utility_meter/total_energy_consumed",
                    "recipe": "{{total_consumption_1+total_consumption_2}}",
                    "entity": "utility_meter",
                    "channel": "total_energy_consumed",
                    "unit": "Wh",
                    "metricKind": "cumulative",
                    "metric": "electricityImport",
                    "ignore_after": 1,
                }
            },
        }

        glue_client = Glue(GlueConfig.parse_obj(config_json), "test_glue")

        value_1 = random.random() * 3
        value_2 = random.random() * 3000

        payload1 = '{"bar": ' + str(value_1) + "}"
        trigger1 = "dummy-device-1/jinja"  # noqa: F841

        payload2 = '{"bar": ' + str(value_2) + "}"
        trigger2 = "dummy-device-2/jinja"  # noqa: F841

        ex_value_1, ts_1 = glue_client.load_value_from_payload(
            payload1,
            4000,
            config_json["ingredients"]["total_consumption_1"]["json_template"],
        )

        glue_client.update_ingredient("total_consumption_1", ex_value_1, ts_1)

        ex_value_2, ts_2 = glue_client.load_value_from_payload(
            payload2,
            4003,
            config_json["ingredients"]["total_consumption_2"]["json_template"],
        )

        glue_client.update_ingredient("total_consumption_2", ex_value_2, ts_2)
        output_value = glue_client.execute_recipe("total_consumed")

        assert output_value is None

    def test_execute_recipe_with_multiple_present_ingredients_which_are_stale_but_ignore(
        self, mock_connect
    ):
        config_json = {
            "version": "202111120",
            "ingredients": {
                "total_consumption_1": {
                    "friendly_name": "Device 1 Power Usage",
                    "topic_in": "dummy-device-1/jinja",
                    "json_template": "{{bar}}",
                },
                "total_consumption_2": {
                    "friendly_name": "Device Status",
                    "topic_in": "dummy-device-2/jinja",
                    "json_template": "{{bar/1000}}",
                },
            },
            "recipes": {
                "total_consumed": {
                    "friendly_name": "Total consumption",
                    "topic_out": "data/devices/utility_meter/total_energy_consumed",
                    "recipe": "{{total_consumption_1+total_consumption_2}}",
                    "entity": "utility_meter",
                    "channel": "total_energy_consumed",
                    "unit": "Wh",
                    "metricKind": "cumulative",
                    "metric": "electricityImport",
                    "ignore_after": 1,
                    "ignore": True,
                }
            },
        }

        glue_client = Glue(GlueConfig.parse_obj(config_json), "test_glue")

        value_1 = random.random() * 3
        value_2 = random.random() * 3000

        payload1 = '{"bar": ' + str(value_1) + "}"
        trigger1 = "dummy-device-1/jinja"  # noqa: F841

        payload2 = '{"bar": ' + str(value_2) + "}"
        trigger2 = "dummy-device-2/jinja"  # noqa: F841

        ex_value_1, ts_1 = glue_client.load_value_from_payload(
            payload1,
            4000,
            config_json["ingredients"]["total_consumption_1"]["json_template"],
        )

        glue_client.update_ingredient("total_consumption_1", ex_value_1, ts_1)

        ex_value_2, ts_2 = glue_client.load_value_from_payload(
            payload2,
            4003,
            config_json["ingredients"]["total_consumption_2"]["json_template"],
        )

        glue_client.update_ingredient("total_consumption_2", ex_value_2, ts_2)
        output_value = glue_client.execute_recipe("total_consumed")

        assert math.isclose(value_1 + value_2 / 1000, float(output_value))

    def test_load_recipe_with_missing_ingredients(self, mock_connect):
        config_json = {
            "version": "202111120",
            "ingredients": {
                "total_consumption_1": {
                    "friendly_name": "Device 1 Power Usage",
                    "topic_in": "dummy-device-1/jinja",
                    "json_template": "{{bar}}",
                },
                "total_consumption_2": {
                    "friendly_name": "Device Status",
                    "topic_in": "dummy-device-2/jinja",
                    "json_template": "{{bar}}/1000",
                },
            },
            "recipes": {
                "total_consumed": {
                    "friendly_name": "Total consumption",
                    "topic_out": "data/devices/utility_meter/total_energy_consumed",
                    "recipe": "{{total_consumption_1}}+{{total_consumption_3}}",
                    "entity": "utility_meter",
                    "channel": "total_energy_consumed",
                    "unit": "Wh",
                    "metricKind": "cumulative",
                    "metric": "electricityImport",
                }
            },
        }

        with self.assertRaises(RuntimeError):
            Glue(GlueConfig.parse_obj(config_json), "test_glue")


@patch("glue.Glue.connect")
class PayloadValues(AbstractTestCase):
    def test_handle_payload_that_is_only_a_float(
        self, mock_connect
    ):
        config_json = {
            "version": "202111120",
            "ingredients": {
                "total_consumption_1": {
                    "friendly_name": "Device 1 Power Usage",
                    "topic_in": "dummy-device-1/jinja",
                    "json_template": "{{value}}",
                },
            },
            "recipes": {
                "total_consumed": {
                    "friendly_name": "Total consumption",
                    "topic_out": "data/devices/utility_meter/total_energy_consumed",
                    "recipe": "{{total_consumption_1}}",
                    "entity": "utility_meter",
                    "channel": "total_energy_consumed",
                    "unit": "Wh",
                    "metricKind": "cumulative",
                    "metric": "electricityImport",
                }
            },
        }

        glue_client = Glue(GlueConfig.parse_obj(config_json), "test_glue")

        value_1 = random.random() * 3
        value_2 = random.random() * 3000

        payload1 = value_1
        trigger1 = "dummy-device-1/jinja"  # noqa: F841

        ex_value_1, ts_1 = glue_client.load_value_from_payload(
            str(payload1),
            4000,
            config_json["ingredients"]["total_consumption_1"]["json_template"],
        )
