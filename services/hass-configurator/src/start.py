#!/usr/bin/python3
# -*- coding: utf-8 -*-
import re
import subprocess
import sys

from hass_configurator.configurator import main

if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0])
    subprocess.run(["ln", "-s", "/ha-config/packages/secrets.yaml", "/cofybox-config/secrets.yaml"])
    sys.exit(main())
