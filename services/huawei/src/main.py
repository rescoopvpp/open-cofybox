# -*- coding: utf-8 -*-
"""
Created on Wed Mar 23 22:18:32 2022
Interface with Huawei SUN2000 solar inverters and publish their sensor values to MQTT
Detects if inverter is equiped with utility meter and/or battery
Will run continuously every minute. Only requires IP of inverter
@author: Epyon
"""

import json
import logging
import os
import sched
import time

import paho.mqtt.client as mqtt
from pymodbus.client.sync import ModbusTcpClient as ModbusClient

conAttempts = 0
firstRun = True
mclient = ModbusClient()

MQTT_BROKER = os.getenv("MQTT_BROKER", "localhost")
brokers_out = {"broker1": MQTT_BROKER}
client = mqtt.Client("huawei")

scheduler = sched.scheduler(time.time, time.sleep)

_LOGGER = logging.getLogger(__name__)
_LOGGER.info("Starting Huawei parser")

def haAutoDiscover(name, unit):
    """Generate Home Assistant autodiscovery MQTT sensor creation payload"""
    msg = {}
    msg['name'] = name
    msg['unit_of_measurement'] = unit
    msg['device_class'] = "energy"
    msg['value_template'] = "{{ value_json.value }}"   
    msg['unique_id'] = name.lower().replace(" ", "_")
    splitString = name.lower().split()
    if(splitString[0] == "utility"):
        topic = splitString[0] + "_" + splitString[1] + "/" + '_'.join(splitString[2:])
    else:
        topic = splitString[0] + "/" + '_'.join(splitString[1:])
    msg['state_topic'] = "data/devices/" + topic
    if(unit == "Wh" or unit == "kWh"):
        msg["state_class"] = "total_increasing"
    else:    
        msg["state_class"] = "measurement"
        msg["last_reset"] = "1970-01-01T00:00:00+00:00"
        msg["last_reset_value_template"] = "1970-01-01T00:00:00+00:00"
    dev = {}
    dev['identifiers'] = "Huawei inverter system"
    dev['name'] = "Huawei inverter"
    dev['model'] = "SUN2000"
    dev['manufacturer'] = "by CofyCo"
    msg['device'] = dev
    topic = "homeassistant/sensor/" + name.lower().replace(" ", "_") + "/config"
    resp = {}
    #Purge existing sensors, avoiding duplicates
    resp = json.dumps(resp, ensure_ascii=False)
    client.publish(topic, resp)
    time.sleep(1)
    resp = json.dumps(msg, ensure_ascii=False)
    client.publish(topic, resp)


def mqtttPublish(name, unit, value, metric):
    splitString = name.lower().split()
    if(splitString[0] == "utility"):
        sensorId = splitString[0] + "_" + splitString[1] + "." + '_'.join(splitString[2:])
        entity = splitString[0] + "_" + splitString[1]
    else:
        sensorId = splitString[0] + "." + '_'.join(splitString[1:])
        entity = splitString[0]
    if(unit == "Wh" or unit == "kWh"):
        metricKind = "cumulative"
    else:    
        metricKind = "gauge"
    if(splitString[0] == "utility"):
        topic = "data/devices/" + splitString[0] + "_" + splitString[1] + "/" + '_'.join(splitString[2:])
    else:
        topic = "data/devices/" + splitString[0] + "/" + '_'.join(splitString[1:])
    try:
        resp = {
            "entity": entity,
            "sensorId": sensorId,
            "value": value,
            "unit": unit,
            "metric": metric,
            "metricKind" : metricKind,
            "timestamp": int(time.time()),
        }
        resp = json.dumps(resp, ensure_ascii=False)
        client.publish(topic, resp)
    except Exception as e: 
        _LOGGER.exception("Unable to publish to MQTT broker")
        _LOGGER.exception(e)
        

def readRegs(name, unit, metric, publish, address, words = 2, multi = 1.0, shift = False):
    global conAttempts 
    if(mclient.connect()):
        readAttemps = 0
        while(readAttemps < 3):
            try:
                result = mclient.read_holding_registers(address, words)
                if(shift):
                    rr = ((result.registers[0] << 16) + result.registers[1])/multi
                else:
                    if(words == 2):
                        rr = result.registers[1]/multi
                    else:
                        rr = result.registers[0]/multi
            except Exception as e:
                _LOGGER.exception("Could not read register" + str(address))
                _LOGGER.exception(e)
                readAttemps = readAttemps + 1
            else:
                if(publish):
                    if(firstRun):
                        haAutoDiscover(name, unit)
                    mqtttPublish(name, unit, rr, metric)
                return rr
        if(readAttemps == 3):
            conAttempts = conAttempts + 1


def fetch_inverter_updates():
    global conAttempts
    global firstRun
    try:
        with open("/cofybox-config/huawei.json") as f:
            configdata = json.load(f)
    except OSError:
        _LOGGER.exception("Could not read config file")
        configdata = ""
    if(configdata != "" or configdata[0]['ipaddr'] !=0):
        try:
            client.connect(brokers_out["broker1"])
        except Exception as e: 
            _LOGGER.exception(e)
        else:
            mclient = ModbusClient(host=configdata[0]['ipaddr'], port=502, unit_id=0)
            while(conAttempts < 10):
                try:
                    mclient.connect()
                    time.sleep(1)
                    inverter = readRegs("Inverter status", "", "", False, 32089, 1)
                    if (inverter is None): raise Exception("No inverter found")
                    if(inverter == 40960 or inverter < 512): _LOGGER.exception("Inverter on standby")
                    else:
                        readRegs("Solar Voltage phase l1", "V", "PvElectricityVoltage",  True,32066, 1, 10.0)
                        readRegs("Solar Current l1", "A", "PvElectricityCurrent", True, 32072, 2, 1000.0)
                        readRegs("Solar Total power", "W", "PvElectricityPower" , True, 32080, 2, 1.0)
                        readRegs("Solar Total energy injected", "kWh", "PvElectricityProduction", True, 32106, 2, 100.0, True)
                    meter = readRegs("Meter status", "", "", False, 37100, 1)
                    if(meter == 0): _LOGGER.exception("No meter connected")
                    else:
                        readRegs("Utility meter Voltage phase l1", "V", "GridElectricityVoltage", True, 37101, 2, 10.0)
                        readRegs("Utility meter Total active power", "W", "GridElectricityPower", True, 37113, 2)
                        readRegs("Utility meter Total energy consumed", "kWh", "GridElectricityImport", True, 37119, 2, 100.0, True)
                        readRegs("Utility meter Total energy injected", "kWh", "GridElectricityExport", True, 37121, 2, 100.0, True)
                    battery = readRegs("Battery status", "", "", False, 37000, 1)
                    if(battery == 0): _LOGGER.exception("No battery connected")
                    else:
                        readRegs("Battery Total energy consumed", "kWh", "BatteryElectricityImport", True, 37066, 2, 100.0, True)
                        readRegs("Battery Total energy injected", "kWh", "BatteryElectricityExport", True, 37068, 2, 100.0, True)
                        readRegs("Battery State of Charge proc", "%", "BatteryElectricitySoc", True, 37006, 1, 10.0)
                        readRegs("Battery Total active power", "W", "BatteryElectricityPower", True, 37001, 2, 1.0)
                    mclient.close()
                    firstRun = False;
                    conAttempts = 10
                except:
                    _LOGGER.exception("Could not connect")
                    conAttempts = conAttempts + 1
    scheduler.enter(60, 1, fetch_inverter_updates)

if __name__ == "__main__":
    scheduler.enter(0, 1, fetch_inverter_updates)  # Zero delay, run immediately
    scheduler.run()
