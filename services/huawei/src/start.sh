#!/bin/bash
if test "$COFYBOX_ENABLE_SERVICE" = TRUE ; then
    cp -n huawei.json /cofybox-config/huawei.json
    python main.py
else
    echo "Huawei block not enabled by environment variable"
fi
